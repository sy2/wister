<?php 
/*请设置“{site_url}”的配置信息*/

return array (
  'DATABASE' => 
  array (
    'tablePrefix' => 'xyrj_',
    'dbHost' => '172.28.138.234',
    //'dbHost' => 'localhost',
    'dbPort' => '3306',
    'dbType' => 'Mysql',
    'dbDriver' => 'mysqli',
    'dbCharset' => 'utf8',
    'dbName' => 'hjz_tuangou',
    'dbUserName' => 'xyrj_remote',
    'dbUserPassword' => 'xyrj123456'
  ),
  'STATIC_URL' => '{site_url}',
  'CDN_URL' => '{site_url}vendor/',
  'SALT' => '{salt}',
  'TIME_ZONE' => 'Asia/Shanghai',
  'RES_DIR' => 'static/uploadfiles/sites/{site}/'
); ?>
