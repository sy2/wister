<?php
return array (
  'STATIC_URL'    => 'http://dev.wister.com/',
  'CDN_URL'       => 'http://dev.wister.com/vendor/',
  'SALT'          => 'a2f17d728b3e44fc9c01d3e79eafe23f',
  'TIME_ZONE'     => 'Asia/Shanghai',
  'RES_DIR'       => 'static/uploadfiles/',
  'MESSAGE_THEME' => 'mobile',
  'DEF_APP'       => 'mobile',
  'DATABASE' => 
  array (
    'tablePrefix'    => '',
    'dbHost'         => '127.0.0.1',
    'dbPort'         => '3306',
    'dbType'         => 'mysql',
    'dbDriver'       => 'PDO',
    'dbCharset'      => 'utf8',
    'dbName'         => 'wister',
    'dbUserName'     => 'root',
    'dbUserPassword' => 'root'
  ),
); ?>
