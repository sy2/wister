<?php 

/**
 * @version			$Id$
 * @create 			2016-03-13 10:03:18 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class AreaPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '市县区';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'area';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'city';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_area';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 
        ),'name' => array(
            'name' => '名称', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '长度50','is_show' => true, 'is_search' => true
        ),'parent_id' => array(
            'name' => '所在城市', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '城市编号','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array(),
            'comment' => '时间信息','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '编号','is_show' => true, 
        ),);

}

?>
