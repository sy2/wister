<?php 

/**
 * @version			$Id$
 * @create 			2016-09-17 14:09:44 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 框架公用配置信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class CommonPopo extends HPopo
{

    /**
     * @var public static $couXiaoTypeMap    类别映射
     */
    public static $couXiaoTypeMap    = array(
        '1' => array('id' => '1', 'name' => '折扣活动'),
        '2' => array('id' => '2', 'name' => '促销活动'),
        '3' => array('id' => '3', 'name' => '拼团活动'),
        '4' => array('id' => '4', 'name' => '秒杀活动')
    );

    /**
     * @var public static $orderStatusMap    状态
     */
    public static $orderStatusMap    = array(
        1 => array('id' => 1, 'name' => '等待支付', 'label' => 'label-info'),
        2 => array('id' => 2, 'name' => '等待支付宝确认', 'label' => 'label-primary'),
        3 => array('id' => 3, 'name' => '等待评论', 'label' => 'label-warning'),
        4 => array('id' => 4, 'name' => '订单完成', 'label' => 'label-success'),
        5 => array('id' => 5, 'name' => '订单取消', 'label' => 'label-danger'),
        6 => array('id' => 6, 'name' => '订单删除', 'label' => 'label-default'),
        7 => array('id' => 7, 'name' => '申请退款', 'label' => 'label-danger'),
        8 => array('id' => 8, 'name' => '退款成功', 'label' => 'label-success'),
        9 => array('id' => 9, 'name' => '订单过期', 'label' => 'label-default'),
        10 => array('id' => 10, 'name' => '等待确认交易', 'label' => 'label-warning'),
        11 => array('id' => 11, 'name' => '金额不对', 'label' => 'label-warning'),
        12 => array('id' => 12, 'name' => '支付错误', 'label' => 'label-warning'),
        13 => array('id' => 13, 'name' => '业务错误', 'label' => 'label-warning'),
    );

}

?>
