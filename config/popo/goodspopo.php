<?php 

/**
 * @version			$Id$
 * @create 			2016-09-11 12:09:15 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class GoodsPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '产品';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'goods';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'goodscategory';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_goods';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $statusMap    商品状态映射
     */
    public static $statusMap    = array(
        '1' => array('id' => '1', 'name' => '下架'),
        '2' => array('id' => '2', 'name' => '上架'),
        '3' => array('id' => '3', 'name' => '删除')
    );

    /**
     * @var public static $isNewMap   是否新品映射
     */
    public static $isShowMap    = array(
        '1' => array('id' => '1', 'name' => '否'),
        '2' => array('id' => '2', 'name' => '是')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序',
            'verify' => array(), 'default' => 99999,
            'comment' => '只能是数字，默认为：99999。','is_show' => true, 'is_order' => 'ASC',
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '标题',
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '标题', 'is_show' => true, 'is_search' => true,
        ),'code' => array(
            'name' => '颜色', 'default' => '0',
            'comment' => '颜色','is_show' => true,
        ),'shop_id' => array(
            'name' => 'PN', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => 'PN','is_show' => false,
        ),'parent_id' => array(
            'name' => '分类', 'default' => 0 ,
            'verify' => array('null' => true, 'numeric' => false,),
            'comment' => '比如：田园时疏。','is_show' => true,
        ),'brand_id' => array(
            'name' => '锁头', 'default' => '',
            'verify' => array( 'null' => true),
            'comment' => '锁头','is_show' => false,
        ),'activity_id' => array(
            'name' => '锁体', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '锁体','is_show' => false,
        ),'parent_path' => array(
            'name' => '内径',
            'verify' => array('null' => true),
            'comment' => '内径','is_show' => false,
        ),'max_price' => array(
            'name' => '厚度', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '厚度','is_show' => false,
        ),'mk_price' => array(
            'name' => '价格', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '价格，请准确输入','is_show' => false,
        ),'content' => array(
            'name' => '中心距', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '中心距',
        ),'attr_data' => array(
            'name' => '孔数', 'default' => '',
            'verify' => array('null' => true),
            'comment' => '孔数','is_show' => false
        ),'hash' => array(
            'name'    => '孔径', 'default' => '0',
            'verify' => array('null' => true ),
            'comment' => '孔径',
        ),'image_path' => array(
            'name' => '封面', 
            'verify' => array( 'len' => 255,),
            'zoom' => array('middel' => array(300, 320), 'small' => array(440, 460)), 
            'type' => array('.png', '.jpg', '.gif', '.bmp'), 'size' => 0.5,
            'comment' => '大小要求：宽480px x 高 480px','is_show' => true, 
        ),'adv_img' => array(
            'name' => '台外径',
            'verify' => array('null' => true),
            'comment' => '台外径','is_show' => false,
        ),'is_new' => array(
            'name' => '台高', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '台高。','is_show' => false,
        ),'is_show' => array(
            'name' => '厚度可允许值', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '厚度可允许值。','is_show' => false,
        ),'buy_type' => array(
            'name' => '内径最小值可做', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '内径最小值可做','is_show' => false,
        ),'score' => array(
            'name' => '外径最大值可做', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '外径最大值可做',
        ),'zhiliang_score' => array(
            'name' => '商品质量', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '质量打分',
        ),'taidou_score' => array(
            'name' => '服务态度', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '态度打分','is_show' => false,
        ),'time_score' => array(
            'name' => '配送准时', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '配送打分',
        ),'tui_rate' => array(
            'name' => '奖励金额', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '奖励金额','is_show' => false,
        ),'total_number' => array(
            'name' => '库存数量', 'default' => '0',
            'verify' => array('null' => false),
            'comment' => '库存数量','is_show' => false,
        ),'total_comments' => array(
            'name' => '总评价数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '评价统计次数','is_show' => false, 
        ),'total_great' => array(
            'name' => '起订数量', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '点赞总数','is_show' => false, 
        ),'total_save' => array(
            'name' => '收藏数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '收藏数统计','is_show' => false, 
        ),'total_orders' => array(
            'name' => '前台咨询', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '前台咨询','is_show' => false
        ),'total_visits' => array(
            'name' => '访问量', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总访问量统计','is_show' => false, 
        ),'status' => array(
            'name' => '是否上架', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1 下架 2 上架','is_show' => true, 
        ),'ext_1' => array(
            'name' => '佣金', 
            'verify' => array( 'len' => 50,),
            'comment' => '佣金','is_show' => false,
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属会员',
        ),'is_pintuan' => array(
            'name' => '是否组团', 'default' => '·',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否组团',
        ),'is_pintuan' => array(
            'name' => '是否组团', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否组团',
        ),'printer_id' => array(
            'name' => '组团人数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '组团人数',
        ),'extend' => array(
            'name' => '房型信息', 'default' => '11',
            'comment' => '房型信息',
        ));
}
?>
