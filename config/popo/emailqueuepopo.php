<?php 

/**
 * @version			$Id$
 * @create 			2016-02-25 16:02:53 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class EmailqueuePopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '消息队列';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'emailqueue';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_email_queue';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 'is_order' => 'DESC'
        ),'shop_id' => array(
            'name' => '商家', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '对应商家','is_show' => false, 
        ),'name' => array(
            'name' => '邮件名称', 'is_search' => true,
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '长度255','is_show' => true, 
        ),'contact' => array(
            'name' => '接收人信息', 
            'verify' => array('null' => false,),
            'comment' => '任务发送内容','is_show' => false, 
        ),'user_name' => array(
            'name' => '用户姓名', 
            'verify' => array('null' => false,),
            'comment' => '长度100字符','is_show' => false, 
        ),'parent_id' => array(
            'name' => '所属用户', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属用户','is_show' => true, 
        ),'content' => array(
            'name' => '内容', 
            'verify' => array('null' => false,),
            'comment' => '详细内容','is_show' => false, 
        ),'type' => array(
            'name' => '类型1电邮、2微信、3短信', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '发送方式','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '消息所属的状态','is_show' => true, 
        ),'hash' => array(
            'name' => '签名', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '签名重复性检测','is_show' => false, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '上一次修改的管理员','is_show' => true, 
        ),);

}

?>
