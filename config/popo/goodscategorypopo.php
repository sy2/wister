<?php 

/**
 * @version			$Id$
 * @create 			2014-10-09 19:10:53 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class GoodscategoryPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '分类';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'goodscategory';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'goodscategory';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_goods_category';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $isRecommendMap    文章状态映射
     */
    public static $isRecommendMap    = array(
        '1' => array('id' => '1', 'name' => '不显示'),
        '2' => array('id' => '2', 'name' => '显示')
    );

    /**
     * @var public static $statusMap    状态映射
     */
    public static $statusMap    = array(
        '1' => array('id' => '1', 'name' => '草稿'),
        '2' => array('id' => '2', 'name' => '发布'),
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序', 
            'verify' => array(), 'default' => 999,
            'comment' => '只能是数字，默认为：当前时间。','is_show' => true, 'is_order' => 'ASC', 
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => null, 
        ),'shop_id' => array(
            'name' => '商家',  'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属的商家ID','is_show' => false, 
        ),'name' => array(
            'name' => '名称', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '长度范围：2~255。','is_show' => true, 'is_search' => true, 
        ),'parent_id' => array(
            'name' => '所属分类', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请正确选取','is_show' => false,
        ),'parent_path' => array(
            'name' => '所属层级', 
            'verify' => array('null' => true, 'len' => 255,),
            'comment' => '格式:：:3:2:1:','is_show' => false, 
        ),'image_path' => array(
            'name' => '缩略图',
            'verify' => array( 'len' => 255,),
            'comment' => '请选择允许的类型。','is_show' => true, 'is_file' => true,
            'zoom' => array('middle' => array(400, 400), 'small' => array(320, 320)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'image_path2' => array(
            'name' => '详细图', 'default' => 'image_path2',
            'verify' => array( 'len' => 255,),
            'comment' => '请选择允许的类型。','is_show' => false, 'is_file' => true,
            'zoom' => array('middle' => array(400, 400), 'small' => array(320, 320)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'is_recommend' => array(
            'name' => '是否前台显示', 'default' => '2',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否让客户选择，如碗、或纸巾为前台添加。','is_show' => true,
        ),'status' => array(
            'name' => '是否发布', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否发布','is_show' => true,
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => false, 
        ),'author' => array(
            'name' => '管理员', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请从下拉里选择','is_show' => true, 
        ),);

}

?>
