<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 服务层基础类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @since 1.0.0
 */
class BaseService extends HObject
{
	
    /**
     * 构造函数
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
	public function __construct()
	{

	}

    /**
     * 得到订单编号
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @return 编号
     */
    protected function _getOrderCode()
    {
        return date('YmdHis') . rand(1000, 9999);
    }
    
    /**
     * [_getWechatCfg 得到当前店铺微信配置]
     * @return [Array] [微信配置信息]
     */
    protected function _getWechatCfg($shopId = null, $exception = false)
    {
        if(!$shopId) {
            return;
        }
        $wxSetting  = HClass::quickLoadModel('wxsetting');
        $info       = $wxSetting->getRecordByWhere('shop_id=' . $shopId);
        if(!$info && $exception) {
            throw new HVerifyException('店铺还没有配置微信，请同负责人联系！');
        }

        return $info;
    }

}