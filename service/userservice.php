<?php

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('service.baseservice');

/**
 * 商品 服务层
 *
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @since 1.0.0
 */
class UserService extends BaseService
{

    /**
     * goods 对象
     * @var
     */
    private $_user;

    /**
     * 构造函数
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->_user     = HClass::quickLoadModel('user');
    }

    public function addTempUser()
    {
        $sessionId      = session_id();
        $name           = 'T-' . $sessionId;
        $data           = array(
            'name' => $name,
            'phone' => '',
            'true_name' => $name,
            'password' => HString::getUUID(),
            'parent_id' => 0,
            'login_time' => $_SERVER['REQUEST_TIME'],
            'ip' => HRequest::getClientIp(),
            'author' => 0
        );

        return $this->_addNewUserInfo($data, 'temp');
    }

    protected function _addNewUserInfo($data, $actor = 'member') 
    {
        $actorInfo      = $this->_getActorByIdentifier($actor);
        if(!$actorInfo) {
            throw new HVerifyException('没有这个用户类别哦，请确认！');
        }
        $salt               = HString::createRandStr(5);
        $data['salt']       = $salt;
        $data['parent_id']  = $actorInfo['id'];
        $data['password']   = HString::encodePassword($data['password'], $salt);
        $userId             = $this->_user->add($data);
        if(!$userId) {
            throw new HVerifyException('非常抱歉，服务器正忙，请你稍后再试！');
        }
        $data['id']     = $userId;
        $name           = str_pad($userId, 6, '0');
        $data['name']   = $name;
        $data['true_name']  = '会员' . $name;
        $this->_user->editByWhere(array('name' => $name, 'true_name' => '会员' . $name), '`id` = ' . $userId);
        $this->_addNewHomersInfo($userId);
        $this->_setUserLoginInfo($data);
        $this->_setUserRights($actorInfo);

        return $data;
    }

    /**
     * 得到会员角色信息
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param  String $identifier 标识 默认为会员
     * @return Array 会员角色信息
     * @throws HVerifyException 验证异常
     */
    protected function _getActorByIdentifier($identifier = 'member')
    {
        $actor      = HClass::quickLoadModel('actor');
        $actorInfo  = $actor->getRecordByIdentifier($identifier);
        if(!$actorInfo) {
            throw new HVerifyException('抱赚，网站还没有开放注册，请您耐心等待～');
        }

        return $actorInfo;
    }

    /**
     * 添加用户扩展信息
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $userId 用户id
     */
    private function _addNewHomersInfo($userId)
    {
        $data       = array(
            'parent_id' => $userId,
            'author' => $userId,
        );
        $userExtend     = HClass::quickLoadModel('userextend');
        $userExtend->add($data);
    }
    
    /**
     * 设置用户登陆信息
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param  Array $userInfo 当前用户的信息
     */
    protected function _setUserLoginInfo($userInfo)
    {
        HSession::setAttributeByDomain($userInfo, 'user');
        HSession::setAttribute('time', (time() + 7200), 'user');
    }

    /**
     * 设置用户权限
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param int $parentId 用户所属的角色
     */
    protected function _setUserRights($parentId)
    {
        $actor      = HClass::quickLoadModel('actor');
        $actorInfo  = is_array($parentId) ? $parentId : $actor->getRecordById($parentId);
        HSession::setAttribute('actor', $actorInfo['identifier'], 'user');
        HSession::setAttribute('actor_name', $actorInfo['name'], 'user');
        HSession::setAttribute('rights', $actorInfo['rights'], 'user');
    }

}