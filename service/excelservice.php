<?php

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('service.baseservice');
HClass::import('vendor.excel.PHPExcel', false);

/**
 * Excel服务层
 *
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @since 1.0.0
 */
class ExcelService extends BaseService
{

    /**
     * @var private $_excel Excel导入实例
     */
    private $_excel;

    /**
     * 构造函数
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->_excel  = new PHPExcel();
    }

    /**
     * 导出Excel文件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function export($titles, $data, $title)
    {
        if(empty($data)) {
            throw new HVerifyException($this->_popo->modelZhName . '数据为空，还不能导出Excel文件！');
        }
        $this->_initExcelProps($title, '导出数据');
        $this->_fillDataToExcelFile($titles, $data);
        $this->_outputExcelFile($title . '导出数据-' . $_SERVER['REQUEST_TIME'] .  '.xls');
    }

    /**
     * 初始化Excel属性
     * 
     * @desc
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  String $type 导出文件类型
     */
    private function _initExcelProps($title, $type)
    {
        $title  = $title . '-Excel' . $type;
        $this->_props  = $this->_excel->getProperties();
        //$this->_props->setCreator('红橘子');
        //$this->_props->setLastModifiedBy('红橘子');
        $this->_props->setTitle($title);
        $this->_props->setSubject($title);  
        $this->_props->setDescription($title);  
        $this->_props->setKeywords($type);
        $this->_props->setCategory($title . '-Excel' . $type);
    }

    /**
     * 初始化Excel表头
     * 
     * @desc
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  Array $data 需要导入的数据
     * @param  int $sheet 需要操作的表
     */
    private function _fillDataToExcelFile($titles, $data, $title = 'sheet1', $sheet = 0)
    {
        $this->_excel->setActiveSheetIndex($sheet);
        $objActSheet = $this->_excel->getActiveSheet(); 
        //设置当前活动sheet的名称  
        $objActSheet->setTitle($title);
        //设置单元格内容
        $col    = 0;
        //设置Excel表头
        foreach($titles as $th) {
            $objActSheet->setCellValueByColumnAndRow($col, '1', $th['name'], 
                    PHPExcel_Cell_DataType::TYPE_STRING);
            $cell   = $objActSheet->getStyleByColumnAndRow($col, '1');
            $cell->getFont()->setBold(true);
            $cell->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            $cell->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objActSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($col)) -> setAutoSize(true);
            $col ++;
        }
        if(empty($data)) {
            return;
        }
        //填充数据
        $row    = 2;
        foreach($data as $item) {
            $col    = 0;
            foreach($titles as $key => $th) {
                $objActSheet->setCellValueByColumnAndRow(
                    $col, $row, HString::decodeHtml($item[$key]),
                    PHPExcel_Cell_DataType::TYPE_STRING
                );
                $col ++;
            }
            $row ++;
        }
    }

    /**
     * 生成对应的模块Excel模板
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _outputExcelFile($fileName)
    {
        HClass::import('vendor.excel.PHPExcel.Writer.Excel5', false);
        $writer     = new PHPExcel_Writer_Excel5($this->_excel);
        header('Content-Type: application/force-download');  
        header('Content-Type: application/octet-stream');  
        header('Content-Type: application/download');
        header('Content-Disposition:inline;filename=' . $fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');  
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');  
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        $writer->save('php://output');  
    }

}