
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindBtnGenCode();
    },
    bindBtnGenCode: function() {
        $('#btn-code').click(function() {
            if(!confirm('确认要生成50个邀请码吗？')) {
                return false;
            }
            HHJsLib.info('正在生成中，请稍等...');
            $.getJSON(
                queryUrl + 'admin/invitation/amakecode',
                {},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    HHJsLib.succeed('生成成功，正在帮您刷新页面中...');
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                }
            );
        });
    }
});


