
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

 HHJsLib.register({
     zTreeObj: null,
     init: function() {
         //运费
         this.bindBtnSubYunFee('a.btn-sub-yun-fee');
         this.bindBtnAddYunFee();
         this.bindBtnAddYunFeeAddress('a.btn-yun-fee-add-address');
         //包邮
         this.bindBtnSubBaoYou('a.btn-sub-bao-you');
         this.bindBtnAddBaoYou();
         this.bindBtnAddBaoYouAddress('a.btn-bao-you-add-address');
         //确认选择区域
         this.bindBtnSelectAddressSure();
         this.bindIsBaoYouChange();
         this.bindWhoPayChange();
     },
     bindWhoPayChange: function() {
        $('#who_pay').change(function() {
            if(1 != $(this).val()) {
                $('#tpl-detail-data-box').hide();
            } else {
                $('#tpl-detail-data-box').show();
            }
        });
     },
     bindIsBaoYouChange: function() {
        $('#is_baoyou').change(function() {
            if(2 != $(this).val()) {
                $('#baoyou-table-box').hide();
            } else {
                $('#baoyou-table-box').show();
            }
        });
     },
     bindBtnAddBaoYou: function() {
         var tpl    = $('#bao-you-item-tpl').html();
         var self   = this;
         $('#btn-add-bao-you').click(function() {
             var id = (new Date()).getTime();
             var trHtml     = tpl.replace(/{id}/g, id);
             $('#bao-you-list-box').append(trHtml);
             self.bindBtnSubBaoYou('#btn-sub-bao-you-' + id);
             self.bindBtnAddBaoYouAddress('#btn-bao-you-add-address-' + id);
         });
     },
     bindBtnAddBaoYouAddress: function(dom) {
        var self   = this;
        $(dom).click(function() {
             var id         = $(this).attr('data-id');
             var isLoaded   = $('#address-data-box').attr('data-loaded');
             $('#btn-select-address-sure')
             .attr('data-target', '#bao-you-address-box-' + id)
             .attr('data-field', '#bao-you-address-path-' + id);
             $('#select-address-modal').modal('show');
             if(1 == isLoaded) {
                 self.loadSelectAddressData();
                 return;
             }
			 this.zTreeObj.checkAllNodes(false);
         });
     },
     bindBtnSubBaoYou: function(dom) {
         $(dom).click(function() {
             var id     = $(this).attr('data-id');
             var itemId = $('#by-add-id-' + id).val();
             $('#bao-you-box-' + id).fadeOut('fast', function() {
                 $(this).remove();
             });
             if(!itemId) {
                 return;
             }
             $.getJSON(
                queryUrl + 'admin/expressbaoyou/adel',
                {id: itemId},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                }
             );
         });
     },
     bindBtnSelectAddressSure: function() {
         var self   = this;
         $('#btn-select-address-sure').click(function() {
             var target = $(this).attr('data-target');
             var field  = $(this).attr('data-field');
			 var nodes  = self.zTreeObj.getCheckedNodes(true);
             var names  = '';
             var paths  = ',';
             if(1 > nodes.length) {
                 target.html('');
                 field.val('');
                 return;
             }
             for(var ele in nodes) {
                 var node = nodes[ele];
                 var path = ':' + node.id + ':';
                 var name = node.name;
                 while(true) {
                     node = node.getParentNode();
                     if(!node) {
                         break;
                     }
                     name   = $.trim(node.name) + '/' + name;
                     path   = ':' + node.id + path;
                 }
                 names      += name + ',';
                 paths      += path + ',';
             }
             $(target).html(names);
             $(field).val(paths);
             $(field + '-name').val(names);
             $('#select-address-modal').modal('hide');
         });
     },
     bindBtnAddYunFeeAddress: function(dom) {
         var self   = this;
         $(dom).click(function() {
             var id         = $(this).attr('data-id');
             var isLoaded   = $('#address-data-box').attr('data-loaded');
             $('#btn-select-address-sure')
             .attr('data-target', '#yun-fee-address-box-' + id)
             .attr('data-field', '#yun-fee-address-path-' + id);
             $('#select-address-modal').modal('show');
             if(1 == isLoaded) {
                 self.loadSelectAddressData();
                 return;
             }
			 this.zTreeObj.checkAllNodes(false);
         });
     },
     loadSelectAddressData: function() {
         var self   = this;
         $.getJSON(
            queryUrl + 'admin/province/alist',
            {},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                $('#address-data-box').attr('data-loaded', 2);
                $('#address-data-box').html('<div id="ztree-address-list" class="ztree"></div>');
                self.bindZTreeData(response.data);
            }
         );
     },
     bindZTreeData: function(data) {
        var self    = this;
        var setting = {
            check: {
                enable: true 
            },
			async: {
				enable: true,
				url: queryUrl + "admin/city/alistbyztree",
				autoParam:["id", "name=n", "level=lv", 'checked=ch'],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: function (treeId, parentNode, childNodes) {
					if (!childNodes) { 
                        return null;
                    }
                    for (var i=0, l=childNodes.length; i<l; i++) {
                        childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
                    }
                    return childNodes;
				}
			},
			view: {
				selectedMulti: false
			},
            callback: {
                onCheck: function() {
                    //self.setParentIds();
                },
				beforeClick: function (treeId, treeNode) {
				}
            }
        };
        this.zTreeObj   = $.fn.zTree.init($('#ztree-address-list'), setting, data);
     },
     bindBtnAddYunFee: function() {
         var tpl    = $('#yun-fee-tpl').html();
         var self   = this;
         $('#btn-add-yun-fee').click(function() {
             var id = (new Date()).getTime();
             var trHtml     = tpl.replace(/{id}/g, id);
             $('#yun-fee-list-box').append(trHtml);
             self.bindBtnSubYunFee('#btn-sub-yun-fee-' + id);
             self.bindBtnAddYunFeeAddress('#btn-yun-fee-add-address-' + id);
         });
     },
     bindBtnSubYunFee: function(dom) {
         $(dom).click(function() {
             var id     = $(this).attr('data-id');
             var itemId = $('#yf-add-id-' + id).val();
             $('#yun-fee-box-' + id).fadeOut('fast', function() {
                 $(this).remove();
             });
             if(!itemId) {
                 return;
             }
             $.getJSON(
                queryUrl + 'admin/expressdetail/adel',
                {id: itemId},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                }
             );
         });
     }
 });
