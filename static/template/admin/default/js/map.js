
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

 HHJsLib.register({
     init: function() {
        var self    = this;
        this.initMapHeight();
        this.initMap();
        this.bindSearchFormSubmit();
        $(window).resize(function() {
            self.initMapHeight();
        });
        this.bindAutoSyncStuffGPSInfo();
     },
     bindAutoSyncStuffGPSInfo: function() {
        var self    = this;
        setInterval(function() {
            self.doSearching($('#search-form').serialize());
        }, 5000);
     },
     isSearching: false,
     bindSearchFormSubmit: function() {
        var self    = this;
        $('#search-form').submit(function() {
            $('#loading-modal').modal('show');
            self.doSearching($(this).serialize());
            return false;
        });
     },
     doSearching: function(data) {
        var self    = this;
        if(self.isSearching) {
            return;
        }
        $.getJSON(
            queryUrl + 'admin/map/astuffsearch',
            data,
            function(response) {
                self.isSearching    = false;
                $('#loading-modal').modal('hide');
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                stuffList   = response.data;
                self.reSetStuffOnMap();
            }
        );
     },
     reSetStuffOnMap: function() {
        this.map.clearOverlays();
        this.setStuffOnMap();
     },
     map: null,
     initMap: function() {
        var self    = this;
        setTimeout(function() {
            self.map = new BMap.Map("container");
            // 创建地图实例  
            var point = new BMap.Point(109.9869588, 27.5574829);
            // 创建点坐标  
            self.map.centerAndZoom(point, 14);
            // 初始化地图，设置中心点坐标和地图级别 
            self.map.enableScrollWheelZoom(true);
            self.map.enableDragging();   //两秒后开启拖拽
            self.map.enableInertialDragging();   //两秒后开启惯性拖拽
            // 添加带有定位的导航控件
            var navigationControl = new BMap.NavigationControl({
            // 靠左上角位置
                anchor: BMAP_ANCHOR_TOP_LEFT,
                // LARGE类型
                type: BMAP_NAVIGATION_CONTROL_LARGE,
                // 启用显示定位
                enableGeolocation: true
            });
            self.map.addControl(navigationControl);
            // 添加定位控件
            var geolocationControl = new BMap.GeolocationControl();
            geolocationControl.addEventListener("locationSuccess", function(e){
                // 定位成功事件
                var address = '';
                address += e.addressComponent.province;
                address += e.addressComponent.city;
                address += e.addressComponent.district;
                address += e.addressComponent.street;
                address += e.addressComponent.streetNumber;
                alert("当前定位地址为：" + address);
            });
            geolocationControl.addEventListener("locationError",function(e){
                // 定位失败事件
                alert(e.message);
            });
            self.map.addControl(geolocationControl);
            self.doSearching($('#search-form').serialize());
        }, 1000);
     },
     labelStyle: {
        'status1': {fontSize: '12px', color: '#fff', borderColor: '#999', background: '#999', padding: '2px 4px', borderRadius: '5px'},
        'status2': {fontSize: '12px', color: '#fff', background: '#f30', padding: '2px 4px', borderRadius: '5px'}
     },
     setStuffOnMap: function() {
        var  self   = this;
        for (var ele in stuffList) {
            var stuff   = stuffList[ele];
            if(!stuff.position || 0 > stuff.position.indexOf(',')) {
                continue;
            }
            var pos     = stuff.position.split(',');
            var point   = new BMap.Point(pos[0], pos[1]);
            var icon    = new BMap.Icon(siteUri + "images/status-" + stuff.status + ".png", new BMap.Size(32, 32));
            var marker  = new BMap.Marker(point, {icon: icon, title: stuff.true_name + '-' + stuff.id});
            var label   = new BMap.Label(stuff.true_name,{offset:new BMap.Size(-5, -22)});
            label.setStyle(this.labelStyle['status' + stuff.status]);
            marker.setLabel(label);
            if(stuffList && stuffList.length == 1) {
                marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
            }
            marker.addEventListener("click", function() {
                self.showStuffInfo(this);
            });
            this.map.addOverlay(marker);;
        }
     },
     showStuffInfo: function(marker) {
        var title   = marker.getTitle();
        var info    = title.split('-');
        var opts = {
            width : 250,     // 信息窗口宽度
            enableMessage:true//设置允许信息窗发送短息
        };
        $.getJSON(
            queryUrl + 'admin/userpeisongyuan/ainfo',
            {id: info[1]}, 
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                self.setUserInfoWindow(response.data.info);
                self.setUserTaskListLines(response.data.tasks);
            }
        );
     },
     setUserTaskListLines: function(tasks) {
        
     },
     setUserInfoWindow: function(userInfo) {
        var tpl         = $('#stuff-info-tpl').html();
        var sContent    = tpl.replace(/{name}/ig, userInfo.name)
        .replace(/{phone}/ig, userInfo.phone)
        .replace(/{loginTime}/ig, userInfo.loginTime)
        .replace(/{status}/ig, userInfo.status)
        .replace(/{address}/ig, userInfo.address)
        .replace(/{verify}/ig, userInfo.verify)
        .replace(/{src}/ig, userInfo.avatar);
        var infoWindow = new BMap.InfoWindow(sContent, opts);  // 创建信息窗口对象
        marker.openInfoWindow(infoWindow);
        setTimeout(function() {
            //图片加载完毕重绘infowindow
            document.getElementById('stuff-avatar').onload = function (){
                infoWindow.redraw();   //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
            };
        }, 1000);
     },
     initMapHeight: function() {
        var height  = $(document).height() - 88 - 200;
        $('#container').css('height', height);
     }
 });
