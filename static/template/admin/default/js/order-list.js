
/**
 * @version $Id$
 * @create Sat 03 Aug 2013 17:50:39 CST By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindStatusChange();
        this.bindBtnShopComment();
        this.bindFormComment();
        this.bindBtnFenPai();
    },
    bindBtnFenPai: function() {
        var self    = this;
        $('a.btn-fenpai').click(function() {
            var id  = $(this).attr('data-id');
            $.getJSON(
                queryUrl + 'admin/userpeisongyuan/alist',
                {},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    self.setPeiSongYuanList(id, response.data);
                    self.bindBtnDoneFenpei();
                }
            );
        });
    },
    setPeiSongYuanList: function(id, list) {
        var html    = '';
        var tpl     = $('#peisong-item-tpl').html();
        for(var ele in list) {
            var item    = list[ele];
            html        += tpl.replace(/{id}/ig, item.id)
            .replace(/{order_id}/ig, id)
            .replace(/{name}/ig, item.name)
            .replace(/{phone}/ig, item.phone);
        }
        $('#peisongyuan-list-box-' + id).html(html);
    },
    bindBtnDoneFenpei: function(){
        $('.btn-done-fenpei').click(function(){            
            try{
                var $that   = $(this);
                var id      = $that.attr('data-id');
                var orderId = $that.attr('data-order-id');
                HHJsLib.isEmpty(id, '编号');
                HHJsLib.isEmpty(orderId, '编号');
                $.getJSON(
                    queryUrl + 'admin/order/afenpei',
                    {
                        id: orderId,
                        user_id: id
                    },
                    function(response){
                        if(response.rs == false){
                            return HHJsLib.warn(response.message);
                        }
                        HHJsLib.succeed('分配成功');
                        setTimeout(function() {
                            window.location.reload();
                        }, 500);
                    }
                );
            }catch(e){
                return HHJsLib.warn(e);
            }
        });
    },
    bindStatusChange: function() {
        $("#status, #process").bind('change', function() {
            if($(this).val()) {
                $("#search-form").submit();
            }
        });
    },
    bindBtnShopComment: function() {
        $("a.btn-shop-comment").click(function() {
            try{
                var id      = $(this).attr('data-id');
                if(!id){
                    return HHJsLib.warn('编号不能为空');
                }
                $.getJSON(
                    queryUrl + 'admin/order/agetshopcomment',
                    {id: id},
                    function(response){
                        if(false === response.rs){
                            $('#myModal').find('.modal-body').html(response.message);
                            $('#myModal').modal('show');
                            return false;
                        }
                        $('#item_id').val(id);
                        $('#shop_comment').val(response.data);
                        $('#myModal').modal('show');
                    }
                );
            }catch(e)
            {
                return HHJsLib.warn(e);
            }
        });
    },
    bindFormComment: function(){
        $('#form-comment').submit(function(){
            try{
                var comment     = $('#shop_comment').val();
                var id          = $('#item_id').val();
                HHJsLib.isEmpty(id, '编号');
                $.getJSON(
                    queryUrl + 'admin/order/apostshopcomment',
                    {id: id, comment: comment},
                    function(response){
                        if(response.rs == false){
                            return HHJsLib.warn(response.message);
                        }
                        HHJsLib.info('保存成功');
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }
                );

                return false;
            }catch(e){
                return HHJsLib.warn(e);
            }
        });
    }
});
