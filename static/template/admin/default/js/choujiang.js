
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
var open    = false;
var timer   = '';
HHJsLib.register({
    effectTimer: null,
    init: function() {
        this.bindStartBtn();
    },
    setWinner: function(id) {
        $('span.label-winner').removeClass('winner').addClass('no-winner');
        $('#winner-' + id).removeClass('no-winner').addClass('winner');
    },
    bindStartBtn: function() {
        var _self   = this;
        $('#start-btn').click(function() {
            var num     = orderList.length -1;
            if($(this).attr('is-success') > 1) {
                HHJsLib.warn('对不起已抽中');
                return false;
            }
            if($(this).attr('is-choujiang') < 1) {
                HHJsLib.warn('暂时不能抽奖');
                return false;
            }
            if(!open) {
                timer   = setInterval(function() {
                    var randomVal = Math.round(Math.random() * num);
                    var prizeName = orderList[randomVal];
                    $('#show').attr('data-code', prizeName).text(prizeName.substring(0, 12) + '******');
                }, 20);
                $('#start-btn').removeClass('start').addClass('stop').text('停止');
                open = true;
            }else{
                clearInterval(timer);
                console.log($('#show').text());
                HHJsLib.info($('#show').text() + '被抽中了！');
                _self._aRunChouJiang($('#show').attr('data-code'));
                $('#start-btn').removeClass('stop').addClass('start').text('开始抽奖');
                open = false;
            }
        });
    },
    _aRunChouJiang: function(name) {
        var _self   = this;
        $.getJSON(
            queryUrl + 'admin/choujiang/arunchoujiang',
            {
                name: name,
                id: $('#id').val()
            },
            function(response) {
                clearInterval(_self.effectTimer);
                if(false == response.rs) {
                    return HHJsLib.warn(response.message);
                }
                _self.setWinner(response.data.id);
                $('td.status-label').text('未中奖');
                $('#status-' + response.data.id).html('<strong class="red fs-22">恭喜您，中奖啦！</strong>');
                $('#start-btn').attr('is-success', 2).text('已抽中');
                $('#delete-btn-' + response.data.orderchoujiang_id).attr(
                    'href', queryUrl + 'admin/choujiang/cancle?id=' + response.data.orderchoujiang_id
                ).text('取消中奖');
                $('#chouzhong-span').text('中奖名单：订单号：' + response.data.name);
                $('#choujiang-show').html('<label>活动抽奖完成！</label><button type="button" class="btn btn-purple btn-small">抽奖已完成</button>');
            }
        );
    }
});