
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function() {
        this.bindTouser();
        this.bindBtnTuwenAdd();
        this.bindBtnSave();
    },
    bindTouser: function() {
        var self = this;
        $('#to-ids').click(function(){
            var body = $('#search-goods-body-template').html();
            $('#modal-body').html(body);
            $('#myModalLabel').html('选择发送用户');
            $('#modal-footer').html($('#search-footer-template').html());
            $('#myModal').modal('show');
            self.bindBtnGoodsSearchFormSubmit();
        });
    },
    bindBtnTuwenAdd: function() {
        var self = this;
        $('#btn-tuwen-add').click(function(){
            var body = $('#search-tuwen-body-template').html();
            $('#modal-body').html(body);
            $('#myModalLabel').html('选择图文素材');
            $('#modal-footer').html($('#search-tuwen-footer-template').html());
            $('#myModal').modal('show');
            self.bindBtnTuwenSearchFormSubmit();
        });
    },
    bindBtnGoodsSearchFormSubmit: function() {
        var self    = this;
        $('#goods-search-form').submit(function() {
            self.doSearchQueList(1);
            return false;
        });
    },
    doSearchQueList: function(page) {
        var self            = this;
        try {
            var keywords    = $('#goo-keywords').val();
            var type        = $('#goo-type').val();
            $.getJSON(
                queryUrl + 'admin/wxarticle/auser',
                {keywords: keywords, type: type, page: page},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    $('#search-page').val(page + 1);
                    var tableTpl    = $('#search-result-template').html();
                    var itemHtml    = self.getSearchGoodsListHtml(response.data);
                    tableTpl        = tableTpl.replace(/{content}/g, itemHtml);
                    $('#goods-table-box').html(tableTpl);
                    self.bindCheckAll();
                    self.initSearchTablePages(page, response);
                    self.bindBtnBatch();
                    self.bindBtnAddSearchUser();
                    self.bindBtnPreNextSearchPages(function(page) {
                        self.doSearchQueList(page);
                    });
                },
                'JSON'
            );
            return false;
        } catch(e) {
            return HHJsLib.warn(e);
        }
    },
    initSearchTablePages: function(page, response) {
        var prePage     = page - 1 > 0 ? page - 1 : 1;
        var nextPage    = page + 1 > response.totalPages ? response.totalPages : page + 1;
        $('#btn-search-pre-page').attr('data-page', prePage);
        $('#btn-search-next-page').attr('data-page', nextPage);
        $('#cur-page-info').html(page + ' / ' + response.totalPages);
        $('#btn-search-next-page,#btn-search-pre-page').attr('disabled', null);
        if(1 == response.totalPages) {
            $('#btn-search-next-page,#btn-search-pre-page').attr('disabled', 'disabled');
        } else if(1 >= page) {
            $('#btn-search-pre-page').attr('disabled', 'disabled');
        } else if (page == response.totalPages) {
            $('#btn-search-next-page').attr('disabled', 'disabled');
        }
    },
    bindBtnPreNextSearchPages: function(callback) {
        $('#btn-search-next-page,#btn-search-pre-page').click(function() {
            if(null != $(this).attr('disabled')) {
                return false;
            }
            var page    = $(this).attr('data-page');
            if('undefined' !== typeof(callback)) {
                return callback(page);
            }
        });
    },
    getSearchGoodsListHtml: function(list) {
        if(!list || 1 > list.length) {
            return '<tr><td colspan="4"><div  class="text-center">没有搜索到相关数据哦～</div></td></tr>';
        }
        var itemTpl = $('#result-item-template').html();
        var tdHtml  = '';
        for(var ele in list) {
            var item    = list[ele];
            tdHtml      += itemTpl.replace(/{id}/g, item.id)
                .replace(/{name}/g, item.name)
                .replace(/{status}/g, item.is_subscribe);
        }

        return tdHtml;
    },
    bindCheckAll: function() {
        $('table th input:checkbox').on('click' , function(){
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function(){
                    this.checked = that.checked;
                    $(this).closest('tr').toggleClass('selected');
                });
        });
    },
    bindBtnBatch: function() {
        var self    = this;
        $('#myModal a.btn-add-batch').on('click', function(){
            try{
                var items       = $("#search-goo-list input:checked");
                if(typeof items.length == 'undefined' || items.length < 1 ) {
                    return HHJsLib.warn("请选择要添加的用户！");
                }
                items.each(function(){
                    var id  = $(this).val();
                    $('#search-goo-item-' + id).fadeOut('fast', function() {
                        $(this).remove();
                    });
                    var parent = $(this).parents('tr');
                    self.appentToUser(parent);
                });
            } catch(e) {
                return HHJsLib.warn(e);
            }
        });
    },
    appentToUser: function(dom){
        var userid = dom.find('td:eq(1)').text();
        var username = dom.find('td:eq(2)').text();
        var ids     = $('#ids').val();
        var names   = $('#to-ids').val();
        ids         += userid + ',';
        names       += username + ',';
        $('#ids').val(ids);
        $('#to-ids').val(names);
    },
    bindBtnAddSearchUser: function() {
        var self    = this;
        $('#goods-table-box a.btn-add-search-goo').click(function() {
            var id  = $(this).attr('data-id');
            $('#search-goo-item-' + id).fadeOut('fast', function() {
                $(this).remove();
            });
            var parent = $(this).parents('tr');
            self.appentToUser(parent);
        });
    },
    bindBtnTuwenSearchFormSubmit: function() {
        var self    = this;
        $('#tuwen-search-form').submit(function() {
            self.doTuwenSearchQueList(1);
            return false;
        });
    },
    doTuwenSearchQueList: function(page) {
        var self            = this;
        try {
            var keywords    = $('#goo-keywords').val();
            $.getJSON(
                queryUrl + 'admin/wxarticle/aarticlewx',
                {keywords: keywords, page: page},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    $('#search-page').val(page + 1);
                    var tableTpl    = $('#tuwen-search-result-template').html();
                    var itemHtml    = self.getSearchTuwenListHtml(response.data);
                    tableTpl        = tableTpl.replace(/{content}/g, itemHtml);
                    $('#goods-table-box').html(tableTpl);
                    self.initSearchTablePages(page, response);
                    self.bindBtnAddSearchTuwen();
                    self.bindBtnPreNextSearchPages(function(page) {
                        self.doTuwenSearchQueList(page);
                    });
                },
                'JSON'
            );
            return false;
        } catch(e) {
            return HHJsLib.warn(e);
        }
    },
    getSearchTuwenListHtml: function(list) {
        if(!list || 1 > list.length) {
            return '<tr><td colspan="4"><div  class="text-center">没有搜索到相关数据哦～</div></td></tr>';
        }
        var itemTpl = $('#tuwen-result-item-template').html();
        var tdHtml  = '';
        for(var ele in list) {
            var item    = list[ele];
            tdHtml      += itemTpl.replace(/{id}/g, item.id)
                .replace(/{name}/g, item.name)
                .replace(/{author-name}/g, item.author_name)
                .replace(/{media-id}/g, item.media_id)
                .replace(/{content}/g, item.content);
        }

        return tdHtml;
    },
    bindBtnAddSearchTuwen: function() {
        var self    = this;
        $('#goods-table-box a.btn-add-search-goo').click(function() {
            var id  = $(this).attr('data-id');
            $('#search-goo-item-' + id).fadeOut('fast', function() {
                $(this).remove();
            });
            var media = $(this).attr('data-media-id');
            var content = $(this).parent('td').find('#content-' + id).html();
            $('#media-id').val(media);
            $('#content-msg').html(content);
        });
    },
    bindBtnSave: function(){
        $('#btn-save').click(function(){
            try{
                var ids     = $('#ids').val();
                var media   = $('#media-id').val();
                HHJsLib.isEmpty(ids, '要发送的用户');
                HHJsLib.isEmpty(media, '图文素材');
                $.getJSON(
                    queryUrl + 'admin/wxarticle/apost',
                    {ids: ids, media: media},
                    function(response){
                        if(response.rs == false){
                            return HHJsLib.warn(response.message);
                        }
                        HHJsLib.info('群发成功');
                        window.location.reload();
                    }
                );
            }catch(e){
                return HHJsLib.warn(e);
            }
        });
    }
});
