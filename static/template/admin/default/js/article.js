
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    len: 0,
    isFenci: false,
    init: function() {
        this.bindAutoFenci();
        this.bindBtnIsBold();
        this.bindTitleColorChange();
        this.bindProvinceListChange();
        this.bindCityListChange();
        this.initCityList();
        this.initDisrictList();
        $('#title-color').ace_colorpicker();
    },
    initCityList: function() {
        var provinceId  = $('#province-list').attr('data-cur');
        provinceId      = !provinceId ? $('#province-list').val() : provinceId;
        if(!provinceId) {
            return;
        }
        this.loadCityList(provinceId);
    },
    initDisrictList: function() {
        var cityId      = $('#city-list').attr('data-cur');
        cityId          = !cityId ? $('#city-list').val() : cityId;
        if(!cityId) {
            return;
        }
        this.loadDistrictList(cityId);
    },
    bindProvinceListChange: function() {
        var self        = this;
        $('#province-list').bind('change', function() {
            var id  = $(this).val();
            if(!id) {
                $('#city-list').html('<option value="">请选择</option>');
                $('#district-list').html('<option value="">请选择</option>');
                return;
            }
            self.loadCityList(id);
        });
    },
    loadCityList: function(pId) {
        $.getJSON(
            queryUrl + 'admin/city/alist',
            {pid: pId},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#city-list').html(optHtml);
                HHJsLib.autoSelect('#city-list');
            }
        );
    },
    bindCityListChange: function() {
        var self    = this;
        $('#city-list').bind('change', function() {
            var id  = $(this).val();
            if(!id) {
                $('#district-list').html('<option value="">请选择</option>');
                return;
            }
            self.loadDistrictList(id);
        });
    },
    loadDistrictList: function(pId) {
        $.getJSON(
            queryUrl + 'admin/area/alist',
            {pid: pId},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#district-list').html(optHtml);
                HHJsLib.autoSelect('#district-list');
            }
        );
    },
    bindTitleColorChange: function() {
        $('#title-color').bind('change', function() {
            $('#name').css({'color': $('#title-color').val()});
        });
    },
    bindBtnIsBold: function() {
        $('#btn-bold').click(function() {
            if($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $('#is_bold').val(1);
                $('#name').css({'font-weight': 400});
                return;
            }
            $(this).addClass('btn-danger');
            $('#is_bold').val(2);
            $('#name').css({'font-weight': 700});
        });
    },
    bindAutoFenci: function() {
        var self    = this;
        var process = false;
        setInterval(function() {
            if(true === process) {
                return;
            }
            if('undefined' === typeof(HHJsLib.editor['content'])) {
                return;
            }
            var content     = HHJsLib.editor['content'].getContentTxt();
            if(!content) {
                return;
            }
            var contentLen  = content.length;
            if(self.len == contentLen && self.isFenci == true) {
                return;
            }
            $('#description').val(content.substring(0, 200));
            self.len    = contentLen;
            process     = true;
            $.post(
                queryUrl + 'public/tstring/afenci',
                {data: content},
                function (response) {
                    process     = false;
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    var tagTopHtml  = '建议：';
                    for(var ele in response.data) {
                        tagTopHtml  += '<a href="###">' + response.data[ele].word + '</a>';
                    }
                    self.isFenci    = true;
                    $('#tags-top-list').html(tagTopHtml);
                    self.bindAddTag();
                },
                'json'
            );
        }, 3000);
    },
    bindAddTag: function() {
        var _root       = this;
        $('#tags-top-list a').click(function() {
            var tag         = $(this).html();
            var tagsName    = $('#tags_name').val();
            if(tagsName && 0 <= tagsName.indexOf(',' + tag + ',')) {
                $("#tags").removeTag(tag);
                return;
            }
            $("#tags").addTag(tag);
        });
    }
});
