
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
var HJZInfoUploadFile   = {
    bindBtnFile: function(target) {
        var self        = this;
        var content     ='<div class="tabbable">'
        + ' <ul class="nav nav-tabs" id="myTab">'
        + ' <li class="active">'
        + ' <a data-toggle="tab" href="#local-file">'
        + ' <i class="green icon-home bigger-110"></i>'
        + ' 本地上传资源'
        + ' </a>'
        + ' </li>'
        + ' <li>'
        + ' <a data-toggle="tab" href="#insert-remote">'
        + ' 插入远程资源'
        + ' </a>'
        + ' </li>'
        + ' </ul>'
        + ' <div class="tab-content">'
        + ' <div id="local-file" class="tab-pane in active">'
        + ' <div id="dialog-upload-file-uploader" class="wu-example">'
        + ' <div id="dialog-upload-file-thelist" class="uploader-list">'
        + '</div>'
        + ' <div class="btns">'
        + ' <div id="dialog-upload-file-btn">选择文件</div>'
        + ' </div>'
        + ' </div>'
        + ' </div>'
        + ' <div id="insert-remote" class="tab-pane row-fluid">'
        + ' <div class="form-group">'
        + ' <label class="control-label no-padding-right" for="form-url"> 地址： </label>'
        + ' <div>'
        + ' <input type="text" id="from-remote-url" placeholder="请输入资源地址..." class="span12">'
        + ' </div>'
        + ' </div>'
        + ' </div>'
        + ' <div id="demo-file-box"></div>'
        + ' </div>'
        + '</div>';
        $(target).click(function() {
            var field   = $(this).attr('data-field');
            var target  = !$(this).attr('data-target') ? field : $(this).attr('data-target');
            var $dialog = HHJsLib.Modal.dialog(
                '选择资源文件',
                content,
                null,
                function() { }
            );
            self.bindFromRemoteUrl(target);
            self.bindUploadBtn($(this).attr('data-type'), target, $dialog);
            $dialog.on('hidden.bs.modal', function (e) {
                //取消当前上传文件进度
                if(self.uploader && self.uploader.isInProgress()) {
                    self.uploader.cancelFile(self.file);
                }
            });
        });
        this.initTargetByType(target);
    },
    initTargetByType: function(target) {
        var self     = this;
        $(target).each(function() {
            var type    = $(this).attr('data-type');
            var field   = $(this).attr('data-field');
            switch(type) {
                case 'video':
                    if($('#video-' + field + '-box').attr('data-src')) {
                        self.initVideoByTarget(field, $('#video-' + field + '-box').attr('data-src'));
                    }
                break;
                case 'audio':
                    if($('#audio-' + field + '-box').attr('data-src')) {
                        self.initAudioByTarget(field, $('#audio-' + field + '-box').attr('data-src'));
                    }
                break;
                default:
                break;
            }
        });
    },
    bindUploadBtn: function(type, target, t) {
        switch(type) {
            case 'image': {
                this.uploadImage(target, t);
                break;
            }
            case 'video': 
                this.uploadVideo(target, t);
                break;
            case 'audio': 
                this.uploadAudio(target, t);
                break;
            case 'file': 
            default: {
                this.uploadFile(target, t);
            }
        }
    },
    file: null,
    uploader: null,
    uploadImage: function(target, t) {
        var self    = this;
        this.uploader = HJZUploader.createImage('#dialog-upload-file-btn', {
            formData: formData[target],
            auto: true,
            fileVal: 'path'
        }, function(file, response) {
            if(false == response.rs) {
                self.uploader.reset();
                return HHJsLib.warn(response.message);
            }
            var data    = response.data;
            $('#' + target).val(data.src).attr(
                'data-url', 
                siteUrl + data.src
            );
            $('#demo-file-box').html('<img src="' + siteUrl + data.src + '"/>');
            $('#' + target + '-lightbox')
            .html('<img src="' + siteUrl + data.small + '"/>')
            .attr('href', siteUrl + data.src);
            HHJsLib.succeed('恭喜，上传成功！');
            t.modal('hide');
        }); 
        this.setUploaderEvents();
    },
    uploadAudio: function(target, t) {
        var self    = this;
        this.uploader = HJZUploader.createAudio('#dialog-upload-file-btn', {
            formData: formData[target],
            auto: true,
            fileVal: 'path'
        }, function(file, response) {
            if(false == response.rs) {
                self.uploader.reset();
                return HHJsLib.warn(response.message);
            }
            var data    = response.data;
            $('#' + target).val(data.src).attr(
                'data-url', 
                siteUrl + data.src
            );
            $('#demo-file-box').html(
                '上传音频：<a href="' + siteUrl + data.src + '" title="点击下载">' + data.name + '</a>'
            );
            self.uploader.reset();
            HHJsLib.succeed('恭喜，上传成功！');
            t.modal('hide');
            self.initAudioByTarget(target, data.src);
        }); 
        this.setUploaderEvents();
    },
    initAudioByTarget: function(target, src) {
        var html    = '<audio controls="true"><source src="' + siteUrl + src + '" type="audio/mp3"></audio>';
        $('#audio-' + target + '-box').html(html);
    },
    uploadVideo: function(target, t) {
        var self    = this;
        this.uploader = HJZUploader.createVideo('#dialog-upload-file-btn', {
            formData: formData[target],
            auto: true,
            fileVal: 'path'
        }, function(file, response) {
            if(false == response.rs) {
                self.uploader.reset();
                return HHJsLib.warn(response.message);
            }
            var data    = response.data;
            $('#' + target).val(data.src).attr(
                'data-url', 
                siteUrl + data.src
            );
            $('#demo-file-box').html(
                '上传视频：<a href="' + siteUrl + data.src + '" title="点击下载">' + data.name + '</a>'
            );
            self.uploader.reset();
            HHJsLib.succeed('恭喜，上传成功！');
            t.modal('hide');
            self.initVideoByTarget(target, data.src);
        }); 
        this.setUploaderEvents();
    },
    initVideoByTarget: function(target, src) {
        var videoObject = {
            container: '#video-' + target + '-box',//“#”代表容器的ID，“.”或“”代表容器的class
            variable: 'player',//该属性必需设置，值等于下面的new chplayer()的对象
            poster: !imagePath ? siteUri + 'images/video.jpg' : imagePath, //封面图片
            video: siteUrl + src //视频地址
        };
        var player=new ckplayer(videoObject);
    },
    uploadFile: function(target, t) {
        var self    = this;
        this.uploader = HJZUploader.createFile('#dialog-upload-file-btn', {
            formData: formData[target],
            auto: true,
            fileVal: 'path'
        }, function(file, response) {
            if(false == response.rs) {
                self.uploader.reset();
                return HHJsLib.warn(response.message);
            }
            var data    = response.data;
            $('#' + target).val(data.src).attr(
                'data-url', 
                siteUrl + data.src
            );
            $('#demo-file-box').html(
                '上传文件：<a href="' + siteUrl + data.src + '" title="点击下载">' + data.name + '</a>'
            );
            self.uploader.reset();
            HHJsLib.succeed('恭喜，上传成功！');
            t.modal('hide');
        });
        this.setUploaderEvents();
    },
    setUploaderEvents: function() {
        var self    = this;
         this.uploader.on('uploadStart', function( file ) {
            self.file   = file;
            $('#dialog-upload-file-btn').append('<div class="modal-progress-box" id="modal-progress-box"><div id="progress-bg-box" class="progress-bg-box text-center" style="width: 0%;"><span class="progress-value-box text-right" id="progress-value-box">0%</span></div></div>');
         });
         //上传完成后重设
         this.uploader.on('uploadFinished', function( file ) {
            $('#progress-value-box').html('100%');
         });
         //上传完成后重设
         this.uploader.on('uploadProgress', function( file, percentage ) {
            var percent     =  parseInt(percentage * 100);
            $('#progress-bg-box').css('width', percent + '%');
            $('#progress-value-box').html(percent + '%');
         });
         //上传出错
         this.uploader.on('uploadError', function( file ) {
            self.uploader.reset();
            HHJsLib.warn("您的网络繁忙，上传“" + file.name + "”文件失败，请调整文件大小或是稍后再来上传！");
         });
    },
    bindFromRemoteUrl: function(target) {
        $('#from-remote-url').bind('change', function() {
            try {
                HHJsLib.isUrlByDom($(this), '图片地址');
                var url     = $(this).val();
                $('#demo-file-box').html('<img src="' + url + '"/>');
                $('#' + target).val(url).attr('data-url', url);
                $('#' + target + '-lightbox')
                .html('<img src="' + url + '"/>')
                .attr('href', url);
            } catch(e) {
                $(this).val('');
                return HHJsLib.warn(e);
            }
        });
    }
};
HHJsLib.register({
    init: function() {
        HJZInfoUploadFile.bindBtnFile("button.btn-file");
    }
});
