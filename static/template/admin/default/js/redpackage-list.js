
/**
 * @version $Id$
 * @create Sat 03 Aug 2013 17:50:39 CST By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindBtnLookUrl();
        this.bindBtnUrl();
    },
    bindBtnLookUrl: function() {
        $("a.btn-look-url").click(function() {
            try{
                var url     = $(this).attr('data-url');
                var title   = $(this).attr('title');                 
                $('#myModal').find('.modal-body').html(url);
                $('#myModalLabel').html(title);
                $('#myModal').modal('show');
            }catch(e) {
                return HHJsLib.warn(e);
            }
        });
    },
    bindBtnUrl: function() {
        $("a.btn-url").click(function() {
            try{
                var url     = $(this).attr('data-url');
                var title   = $(this).attr('title');                 
                $('#myModal').find('.modal-body').html(url);
                $('#myModalLabel').html(title);
                $('#myModal').modal('show');
            }catch(e) {
                return HHJsLib.warn(e);
            }
        });
    }
});
