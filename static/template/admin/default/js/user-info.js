
/**
 * @version $Id$
 * @create 2013/10/3 18:48:19 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function () {
        HJZTMapSelector.init({
            dom: '#area',
            position: '#position',
            full: '#area'
        });
    }
});
