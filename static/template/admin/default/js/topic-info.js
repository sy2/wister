
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function() {
        this.bindProvinceListChange();
        this.bindCityListChange();
        this.initCityList();
        this.initDisrictList();
    },
    initCityList: function() {
        var provinceId  = $('#province-list').attr('data-cur');
        provinceId      = !provinceId ? $('#province-list').val() : provinceId;
        if(!provinceId) {
            return;
        }
        this.loadCityList(provinceId);
    },
    initDisrictList: function() {
        var cityId      = $('#city-list').attr('data-cur');
        cityId          = !cityId ? $('#city-list').val() : cityId;
        if(!cityId) {
            return;
        }
        this.loadDistrictList(cityId);
    },
    bindProvinceListChange: function() {
        var self        = this;
        $('#province-list').bind('change', function() {
            var id  = $(this).val();
            if(!id) {
                $('#city-list').html('<option value="">请选择</option>');
                $('#district-list').html('<option value="">请选择</option>');
                return;
            }
            self.loadCityList(id);
        });
    },
    loadCityList: function(pId) {
        $.getJSON(
            queryUrl + 'admin/city/alist',
            {pid: pId},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#city-list').html(optHtml);
                HHJsLib.autoSelect('#city-list');
            }
        );
    },
    bindCityListChange: function() {
        var self    = this;
        $('#city-list').bind('change', function() {
            var id  = $(this).val();
            if(!id) {
                $('#district-list').html('<option value="">请选择</option>');
                return;
            }
            self.loadDistrictList(id);
        });
    },
    loadDistrictList: function(pId) {
        $.getJSON(
            queryUrl + 'admin/area/alist',
            {pid: pId},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#district-list').html(optHtml);
                HHJsLib.autoSelect('#district-list');
            }
        );
    }
});
