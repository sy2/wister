HHJsLib.register({
    init: function() {
        this.bindItemBoxHover();
        this.bindTimer('div.item-box.status-2 strong.timer');
        this.bindLock('a.btn-lock');
        this.bindUnLock('a.btn-unlock');
        this.bindBtnToDiancan();
    },
    bindLock: function(dom) {
        var self    = this;
        $(dom).click(function() {
            if(!confirm('您确认要锁定此桌号吗？')) {
                return;
            }
            var $this   = $(this);
            var id      = $this.attr('data-id');
            $.getJSON(
                queryUrl + 'admin/zhuohao/alock',
                {id: id},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    HHJsLib.succeed('桌号上锁成功！');
                    $this.addClass('btn-unlock').removeClass('btn-lock').unbind();
                    $('#item-' + id).addClass('status-2');
                    $('#time-box-' + id + ' strong.timer').attr('data-time', response.data);
                    $this.html('<i class="icon-unlock"></i> 解锁');
                    self.bindUnLock('#tool-box-' + id + ' a.btn-unlock');
                }
            );
        });
    },
    bindUnLock: function(dom) {
        var self    = this;
        $(dom).click(function() {
            if(!confirm('您确认要解除此桌号的锁定吗？')) {
                return;
            }
            var $this   = $(this);
            var id      = $this.attr('data-id');
            $.getJSON(
                queryUrl + 'admin/zhuohao/aunlock',
                {id: id},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    HHJsLib.succeed('桌号解锁成功！');
                    $this.addClass('btn-lock').removeClass('btn-unlock').unbind();
                    $('#item-' + id).removeClass('status-2');
                    $this.html('<i class="icon-lock"></i> 上锁');
                    self.bindLock('#tool-box-' + id + ' a.btn-lock');
                }
            );
        });
    },
    bindItemBoxHover: function() {
        $('#data-grid-box div.item-box').hover(function() {
            $(this).addClass('active');
        }, function() {
            $(this).removeClass('active');
        });
    },
    bindTimer: function(dom) {
        dom     = typeof(dom) == 'undefined' ? 'strong.time-box' : dom;
        var $dom        = $(dom);
        var time        = Math.floor((new Date()).getTime() / 1000);
        var timer       = setInterval(function() {
            $dom.each(function() {
                var $that       = $(this);
                var startTime   = $that.attr('data-time');
                var lastTime    = time - startTime;
                var min         = Math.floor(lastTime / 60);
                var sec         = lastTime - min * 60;
                var minStr      = min >= 10 ? min : (min <= 0 ? '00' : '0' + min);
                var secStr      = sec >= 10 ? sec : (sec <= 0 ? '00' : '0' + sec);
                $that.html(minStr + ':' + secStr);
            });
            time ++;
        }, 1000);
    },
    bindBtnToDiancan: function(){
        $('a.btn-to-diancan').click(function(){
            var that    = $(this);
            var id      = that.attr('data-id');
            $.getJSON(
                queryUrl + 'admin/zhuohao/agetorder',
                {id: id},
                function(response){
                    if(false == response.rs){
                        return false;
                    }
                    window.location.href = queryUrl + 'admin/diancan?type=1&id=' + response.data.id;
                }
            );
        });
    }
});