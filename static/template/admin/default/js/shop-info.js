
/**
 * @version $Id$
 * @create 2013/10/3 18:48:19 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function () {
        HJZBMapSelector.init({
            dom: '#address',
            position: '#position',
            full: '#address'
        });
        this.btnAddSubItem();
        this.btnDelSubItem('.btn-del-item');
    },
    btnAddSubItem: function() {
        var that = this;
        $('#btn-add-new-category').click(function() {
            var time = (new Date()).getTime();
            var tpl  = $('#new-item-tpl').html();
            var cont = tpl.replace(/{key}/g, time);
            $('#sku-fang-list').append(cont);
            that.btnDelSubItem('#item-' + time + ' a.btn-del-item');
            that.initBtnUploadImg(time);
        });
    },
    btnDelSubItem: function(dom){
        var that = this;
        $(dom).click(function(){
            var id = $(this).attr('data-id');
            $('#item-'+id).remove();
        });
    },
    initBtnUploadImg: function(key) {
        formData['sku-img-' + key]   = {
            'timestamp' : key,
            'token'     : key,
            'model'     : 'goods',
            'field'     : 'image_path',
            'nolinked'  : 1
        };
        HJZInfoUploadFile.bindBtnFile('#btn-sku-img-' + key);
    }
});
