
/**
 * @version $Id$
 * @create Sat 03 Aug 2013 21:49:02 CST By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

//日钟工具
function Clock() {
    var date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth() + 1;
    this.date = date.getDate();
    this.day = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")[date.getDay()];
    this.hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    this.minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    this.second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

    this.toString = function() {
        return (this.year) + "年" + this.month + "月" + this.date + "日 " + this.day + " " + this.hour + ":" + this.minute + ":" + this.second + " ";
    };

    this.toSimpleDate = function() {
        return this.year + "-" + this.month + "-" + this.date;
    };

    this.toDetailDate = function() {
        return this.year + "-" + this.month + "-" + this.date + " " + this.hour + ":" + this.minute + ":" + this.second;
    };
}

//公用的脚本
HHJsLib.register({
    init: function() {
        this.bindDeleteHint();
        $('[data-rel="tooltip"]').tooltip();
        HHJsLib.bindLightBox("a.lightbox", cdnUrl + '/jquery/plugins/lightbox');
        this.highLightNavmenuBox();
        this.bindTimeInfo();
        this.bindSwitchWebsite();
        this.bindNewByCopyBtn();
        this.bindDateList();
        this.bindDateTimeList();
        HHJsLib.fieldHint('input.search-query');
        HHJsLib.autoSelect('select.auto-select');
        this.bindSetIsMiniStyle();
        //this.bindDialogMessage();
        //this.bindVoiceMessage();
    },
    bindSetIsMiniStyle: function() {
        $('#sidebar-collapse').click(function() {
            var isMiniStyle     = 1;
            var $sidebar        = $('#sidebar');
            if(!$sidebar.hasClass('menu-min')) {
                isMiniStyle     = 2;
            }
            $.getJSON(
                queryUrl + 'admin/index/setsidebarmini',
                {flag: isMiniStyle },
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    console.info('Mini sidebar style 设置成功！');
                }
            );
        });
    },
    bindDateList: function() {
        HHJsLib.importCss([siteUri + "/css/datepicker.css"]); 
        HHJsLib.importJs(
            [siteUri + "/js/bootstrap-datepicker.min.js"],
            function() {
                for(var ele in dateList) {
                    $(dateList[ele]).datepicker({
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        todayBtn: true,
                        minuteStep: 2,
                        todayHighlight: 1,
                        language: 'zh-CN'
                    }); 
                }
            }
        );
    },
    bindDateTimeList: function() {
        HHJsLib.importCss([cdnUrl + "/bootstrap/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css"]); 
        HHJsLib.importJs(
            [cdnUrl + "/bootstrap/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"],
            function() {
                for(var ele in datetimeList) {
                    $(datetimeList[ele]).datetimepicker({
                        format: 'yyyy-mm-dd hh:ii:ss',
                        autoclose: true,
                        todayBtn: true,
                        minuteStep: 2,
                        todayHighlight: 1,
                        language: 'zh-CN'
                    }); 
                }
            }
        );
    },
    bindNewByCopyBtn: function() {
        var self    = this;
        $('#btn-new-copy').click(function() {
            $.getJSON(
                queryUrl + 'admin/' + modelEnName + '/aloadlist',
                {page: 1},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(e);
                    }
                    self.showTableGridDialog(response.data);
                }
            );
        });
    },
    showTableGridDialog: function(data) {
        var theadHtml   = '';
        var tbodyHtml   = '';
        for(var field in data.fields) {
            theadHtml   += '<th data-column-id="' + field 
            + '" data-align="center" data-header-align="center">' 
            + data.fields[field].name + '</th>';
        }
        for(var ele in data.list) {
            tbodyHtml   += '<tr>';
            for(var field in data.fields) {
                tbodyHtml   += '<td>' + data.list[ele][field] +'</td>';
            }
            tbodyHtml       += '</tr>';
        }
        var t           = new Date().valueOf();
        var tableHtml   = TplMap.TableGrid.replace(/{thead}/g, theadHtml);
        tableHtml       = tableHtml.replace(/{tbody}/g, tbodyHtml);
        tableHtml       = tableHtml.replace(/{t}/g, t);
        var $dialog     = HHJsLib.Modal.confirm(
            '请选择基于的文章', 
            tableHtml, 
            function($dialog) { 
                HHJsLib.Modal.info($dialog.attr('data-id')); 
            }
        ).css('width', '1002px').css('left', '36%');
        this.initGridTable('#grid-' + t);
    },
    initGridTable: function(dom) {
        HHJsLib.importCss([
            cdnUrl + 'jquery/plugins/jquery.bootgrid/jquery.bootgrid.min.css'
        ]);
        HHJsLib.importJs([
            cdnUrl + 'jquery/plugins/jquery.bootgrid/jquery.bootgrid.min.js'
        ], function() {
            $(dom).bootgrid({
                formatters: {
                    "link": function(column, row) {
                        return "<a href=\"#\">" + column.id + ": " + row.id + "</a>";
                    }
                }
            });
        });
    },
    highLightNavmenuBox: function() {
        HHJsLib.highLightElement('ul.nav-list li', 'active', 1, function(targetDom, target) {
            if(!$(target).hasClass('single')) {
                $(target).parent().parent().parent().addClass('active');
            }
        });
    },
    bindDeleteHint: function() {
        $("a.delete").click(function() {
            if(confirm("您确认要删除这条记录吗？删除后，就不能找回了！")) {
                return true;
            }
            return false;
        });
    },
    bindTimeInfo: function() {
        setInterval(function() {
            $('#time-info').html(new Clock().toString());
        }, 1000);
    },
    bindSwitchWebsite: function() {
        $("#website-list").change(function() {
            var $this   = $(this);
            var href    = window.location.href;
            if(/editview|addview/i.test(href)) {
                if(1 ===  $('#website_id').length) {
                    $('#website_id').val($this.val());
                    HHJsLib.info('当前信息已经移动到“' + $this.find('option:selected').text() + '”，请提交！');
                }
                return;
            }
            HHJsLib.info('正在切换中...');
            $.getJSON(
                siteUrl + "/index.php/public/website/aswitch",
                {id: $this.val()},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    HHJsLib.confirm('切换成功！是否现在刷新页面？', function() {
                        window.location.reload();
                    });
                }
            );
        });
    },
    isLoading: false,
    bindDialogMessage: function(){
        var self = this;
        if(false === isLatestOrder) {
            return;
        }
        self.getLastestMessage();
        setInterval(function(){
            if(false === self.isLoading) {
                self.getLastestMessage();
            }
        }, 5000);
    },
    getLastestMessage: function(){
        var self = this;
        var remind = Cookies.get('never-remind');
        if(remind == '1'){
            this.isLoading = true;
            return;
        }
        var id  = Cookies.get('lastest-notice-id');
        if(typeof id == 'unfined' || id == null || id == ''){
            id = 0;
        }
        this.isLoading = true;
        $.getJSON(
            queryUrl + 'admin/index/getlatestmessage',
            {id: id},
            function(response){
                self.isLoading = false;
                if(response.rs == false){
                    return false;
                }
                var tpl     = $('#message-dialog-tpl').html();
                var style   = 'alert-danger';
                var result  = tpl.replace(/{id}/ig, response.id).replace(/{content}/ig, response.data.content);
                if(response.data.type == 2){
                    style   = 'alert-danger';
                }else if(response.data.type == 6){
                    style   = 'alert-warning';
                }else if(response.data.type == 7){
                    style   = 'alert-success';
                }else if(response.data.type == 8){
                    style   = 'alert-info';
                }
                result      = result.replace(/{style}/ig, style);
                $('.message-dialog-box').append(result);
                setTimeout(function(){
                    $('#message-dialog-id-' + response.id).fadeOut('fast', function(){
                        $(this).parent().remove();
                    });
                }, 1000 * 60 * 5);
                var audioEle = $("#audio")[0];
                audioEle.play();    //播放
                Cookies.set('lastest-notice-id', response.id);
            }
        );
    },
    bindVoiceMessage: function(){
        var self = this;
        var remind = Cookies.get('never-remind');
        if(remind == '1'){
            $('#IconVolume').attr("class","icon-volume-off voice-icon fs-30");
            $("#VoiceMessage").attr("class","btn btn-mini voice-message-box pd-8-9");
        }else{
            $('#IconVolume').attr("class","icon-volume-up voice-icon");
            $("#VoiceMessage").attr("class","btn btn-info voice-message-box");
        }
        $('#VoiceMessage').click(function(){
            if($('#IconVolume').hasClass('icon-volume-up')){
                $('#IconVolume').attr("class","icon-volume-off voice-icon fs-30");
                $("#VoiceMessage").attr("class","btn btn-mini voice-message-box pd-8-9");
                Cookies.set('never-remind', '1', {expires: 7});
            }else if($('#IconVolume').hasClass('icon-volume-off')){
                $('#IconVolume').attr("class","icon-volume-up voice-icon");
                $("#VoiceMessage").attr("class","btn btn-info voice-message-box");
                Cookies.remove('never-remind');
                self.isLoading = false;
            }
        });
    }
});
