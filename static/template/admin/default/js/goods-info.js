
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function() {
        this.bindBtnAddSuoti();
        this.bindBtnDelSuoti();
    },
    bindBtnAddSuoti: function() {
        var self = this;
        $('#btn-add').on('click', function() {
            var tpl     = $('#suoti-item-tpl').html();
            var color   = $('#code').val();
            var t       = (new Date()).getTime();
            tpl         = tpl.replace(/{id}/g, t);
            $('#suoti-box').append(tpl);
            $('#color-' + t).val(color);
            self.bindBtnDelSuoti();
            self.createUploadImage(t);
        });
    },
    bindBtnDelSuoti: function() {
        $('.del-item').on('click', function() {
            var id = $(this).attr('data-id')
            $('#item-' + id).remove();
        });
    },
    createUploadImage: function(t) {
        formData['sku-img-' + t]   = {
            'timestamp':t,
            'token':hex_md5(t),
            'model':'goods',
            'field':'image_path',
            'nolinked':1
        };
        HHJsLib.bindLightBox('#suoti-box a.lightbox', cdnUrl + 'jquery/plugins/lightbox');
        HJZInfoUploadFile.bindBtnFile("#item-" + t + " button.btn-file");
    }
});
