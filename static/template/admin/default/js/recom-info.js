HHJsLib.register({
    init: function () {
        this.bindRoomChange();
    },

    bindRoomChange: function() {
        $('#roomid').change(function() {
            var roomid = $(this).val();
            $.getJSON(
                 queryUrl + 'admin/recom/ahuxing',
                 {roomid:roomid},
                 function(response) {
                    var tpl   = '<option value="{name}">{name}</option>';
                    var list  = response.data;
                    var html  = '';
                    $.each(list,function(i,item) {
                        html += tpl.replace(/{name}/g,item['name']);
                    });
                    $('#loupan').html(html);
                    $('#loupan').trigger("chosen:updated");
                 }
            );
        });
    }
});
