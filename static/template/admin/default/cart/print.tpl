﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
    </head>
    <body style="font-size:18px; font-family: '黑体';">
        <div class="position-relative">
            <h1 class="text-center">
                <a href="javascript:void(0);" style="display:none;" class="btn btn-mini  btn-info" id="btn-print">
                    <i class="icon icon-print"></i> 开始打印
                </a>
            </h1>
            <div class="clearfix"></div>
        </div>
        <?php
            $kefu       = HResponse::getAttribute('kefu');
            $list       = HResponse::getAttribute('list');
            $goodsMap   = HResponse::getAttribute('goodsMap');
            $record     = $list[0];
        ?>
        <div style=" margin: 0 auto; text-align:center; padding: 20px 0;">
            <div style="line-height:34px;">
                <p style="font-size:24px; font-weight:bold; letter-spacing:2px;"><?php echo $siteCfg['name']; ?></p>
                <p>电话：<?php echo $kefu['content']; ?></p>
                <p style="font-size:20px; border-bottom:1px solid #000; padding-bottom:10px;">客人点餐</p>
            </div>
            <div style="line-height:30px;width:260px; margin:0 auto; font-size:16px;">
                <p style="margin-bottom:0; width:260px;">
                    <span style="text-align:left; display:inline-block; width:46%;">台号：<?php echo $record['code']; ?></span>
                    <span style="text-align:left; display:inline-block; width:48%;">人数：<?php echo $record['total_number']; ?></span>
                </p>
                <p style="margin-bottom:0; width:260px;">
                    <span style="text-align:left; display:inline-block; width:100%;">时间：<?php echo date('H:i:s', time()); ?></span>
                </p>
            </div>
            <div style="margin-top:10px; border-bottom: 1px dashed #000; width:240px; margin:0 auto;">
                <p style="border-bottom: 1px dashed #000; padding-bottom:5px; width:240px;">
                    <span style="text-align:left; display:inline-block; width:75%;">菜品名称</span>
                    <span style="text-align:center; display:inline-block; width:20%;">数量</span>
                </p>
                <?php
                    $total  = 0;
                    foreach($list as $item){
                    $price  = $item['number'] * $item['price'];
                    $total += $item['number'];
                    $goods  = $goodsMap[$item['goods_id']];
                ?>
                <p style="margin-bottom:5px; width:240px;">
                    <span style="text-align:left; display:inline-block; width:80%; font-size:18px;"><?php echo HString::cutString($goods['name'], 14) . '  ' . $item['group_name']; ?></span>
                    <span style="text-align:center; display:inline-block; width:10%;"><?php echo $item['number']; ?></span>
                </p>
                <?php } ?>
            </div>
            <div style="width:240px; margin: 0 auto; margin-top:10px; text-align: left; border-bottom: 1px dashed #000; padding-bottom: 10px; ">
                <p>备注:</p>
                <p>&nbsp;</p>
            </div>
            <div style="margin-top:20px; width:240px; margin:0 auto;">
                <div style="float: left;">
                    <p style="margin-top:10px;">总计</p>
                </div>
                <div style="float: right; text-align: left; margin-right:11px; margin-top:10px;">
                    <p>
                        <span style="font-size:18px;"><?php echo $total; ?></span>
                    </p>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div style="margin-top: 10px; font-size: 18px; line-height:25px;">
                <p style="margin-bottom:0;">多谢惠顾</p>
                <p style="margin-bottom:0;">欢迎再次光临</p>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.print();
                    $('#btn-print').show();
                }, 1000);
                $('#btn-print').click(function() { 
                    $(this).hide();
                    setTimeout(function() {
                        window.print();
                        $('#btn-print').show();
                    }, 500);
                });
            });
        </script>
    </body>
</html>
