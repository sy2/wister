<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
	</head>
	<body>
        <?php
            $list       = HResponse::getAttribute('list');
            $listGroup  = HResponse::getAttribute('listGroup');
            $userMap    = HResponse::getAttribute('userMap');
        ?>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/cart/cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <?php require_once(HResponse::path() . '/cart/list-tool-bar.tpl'); ?>
                            <div class="row-fluid border-none pd-0">
                                <div class="tabbable tabs-left pd-15">
                                    <ul class="nav nav-tabs" id="diancan-tab">
                                        <?php 
                                            foreach($listGroup as $key => $item){
                                            $user   = $userMap[$item['parent_id']];
                                        ?>
                                        <li class="<?php echo $key == 0 ? 'active' : ''; ?>">
                                            <a href="#user-diancan-<?php echo $item['parent_id']; ?>" data-toggle="tab">
                                                <i class="blue icon-user bigger-110"></i>
                                                <?php echo '餐桌号:#' . $item['code']; ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content bg-white">
                                        <?php
                                            foreach($listGroup as $key => $item){
                                        ?>
                                        <div class="tab-pane <?php echo $key == 0 ? 'active' : ''; ?>" id="user-diancan-<?php echo $item['parent_id']; ?>">
                                            <form class="mr-0" action="<?php echo HResponse::url('' . $modelEnName . '/quick'); ?>" method="post" id="list-form-<?php echo $item['id']; ?>" target="_blank">
                                                <table id="data-grid-box" class="table table-striped table-bordered table-hover" >
                                                    <thead>
                                                        <tr>
                                                            <th class="center">
                                                                <label><input type="checkbox"/><span class="lbl"></span></label>
                                                            </th>
                                                            <?php
                                                                $columns        = 2;
                                                                $showFields     = HResponse::getAttribute('show_fields');
                                                                foreach($showFields as $key => $cfg ) {
                                                                    echo '<th class="field-' . $key . '" title="' . $cfg['comment'] . '">' . HTranslate::__($cfg['name']) . '</th>';
                                                                    $columns ++;
                                                                }
                                                            ?>
                                                            <th>操作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        if(HVerify::isEmpty(HResponse::getAttribute('list'))) {
                                                            echo '<tr><td colspan="' . $columns . '" class="center">暂无客人点餐</td></tr>';
                                                        }
                                                        foreach(HResponse::getAttribute('list') as $key => $record) {
                                                            if($record['parent_id'] != $item['parent_id']){
                                                                continue;
                                                            }
                                                            echo $key % 2 == 0 ? '<tr class="odd"' . '" id="' . $record['id'] .'">' : '<tr ' . '" id="' . $record['id'] .'">';
                                                            echo '<td class="center"><label><input type="checkbox" name="id[]" value="' .  $record['id'] . '" class="chk-me"/><span class="lbl"></span></label>';
                                                            echo '</td>';
                                                            foreach($showFields as $field => $cfg) {
                                                                echo '<td class="field field-' .  $field . '" field="'
                                                                     . $field . '" data-old="' . $record[$field]
                                                                     . '" id="' .  $field . '-' . $record['id']
                                                                . '" data-id="' .  $record['id'] . '">' ;
                                                                echo HResponse::formatText($field, $record);
                                                                echo '</td>' ;
                                                            }
                                                    ?>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <?php if($record['status'] == 7){ ?>
                                                                    <a href="javascript:void(0);" data-id="<?php echo $record['id']; ?>" title="确认" class='btn btn-mini btn-info btn-queren'><i class="icon-check"></i>确认</a>
                                                                    <?php }else if(in_array($record['status'], array(2,5))){ ?>
                                                                    <a href="javascript:void(0);" data-id="<?php echo $record['id']; ?>" title="退菜" class="btn btn-mini btn-danger btn-tui"><i class="icon-share-alt"></i>退菜</a>
                                                                    <?php } ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                                <div class="row-fluid border-grey">
                                                    <div class="span9">
                                                        
                                                    </div>
                                                    <div class="span3 txt-right">
                                                        <div class="quick-operation">
                                                            <label class="mr-5">批量操作:</label>
                                                            <select name="operation" class="span7 operation" data-id="<?php echo $item['id']; ?>">
                                                                <option value="">选择操作</option>
                                                                <option value="print">打印菜单</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <?php } ?>
                                        <?php
                                            if(!$listGroup){
                                                echo '暂无客人点餐';
                                            }
                                        ?>
                                    </div>                         
                                </div>                                
                            </div>
                        <!-- PAGE CONTENT ENDS HERE -->
                         </div><!--/row-->
                        </div><!--/#page-content-->
                    </div><!-- #main-content -->
                </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>  
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/diancan-list.js"></script>        
	</body>
</html>
