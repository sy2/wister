                                <div class="row-fluid">
                                    <form id="search-form" action="<?php echo HResponse::url('' . $modelEnName . '/search'); ?>" method="get">
                                        <div class="span2">
                                            <div id="table_report_length" class="dataTables_length">
                                                    <label>每页:
                                                        <select size="1" class="auto-select" name="perpage" id="perpage" aria-controls="table_report" data-cur="<?php echo HRequest::getParameter('perpage'); ?>">
                                                            <option value="15" selected="selected">15</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                        条
                                                </label>
                                            </div>
                                        </div>
                                        <div class="span10 txt-right">
                                            <div class="pr-15">
                                                <?php if(HResponse::getAttribute('parent_id_list')) { ?>
                                                <label><?php echo str_replace('分类', '', $modelZhName); ?>分类（<=100）: 
                                                    <select name="type" id="category" class="input-medium" data-cur="<?php echo HRequest::getParameter('type'); ?>">
                                                        <option value="">全部</option>
                                                        <?php 
                                                            HClass::import('hongjuzi.utils.HTree');
                                                            $hTree  = new HTree(
                                                                HResponse::getAttribute('parent_id_list'),
                                                                'id',
                                                                'parent_id',
                                                                'name',
                                                                'id',
                                                                '<option value="{id}">' .
                                                                '{name}' .
                                                                '</option>'
                                                            );
                                                            echo $hTree->getTree();
                                                        ?>
                                                    </select>
                                                </label>
                                                <?php } ?>
                                                <label>搜索关键词: 
                                                    <input type="text" class="input-medium search-query" name="keywords" id="keywords" data-def="<?php echo !HRequest::getParameter('keywords') ? '关键字...' : HRequest::getParameter('keywords'); ?>">
                                                    <button type="submit" class="btn btn-purple btn-small">搜索<i class="icon-search icon-on-right"></i></button>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
