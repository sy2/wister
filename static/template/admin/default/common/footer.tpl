        <div id="message-dialog-box" class="message-dialog-box"></div>
        <?php if(true !== $isNoMessage) { ?>
        <!-- <a id="VoiceMessage" class="btn btn-info voice-message-box"><i id="IconVolume" class="icon-volume-up voice-icon">&nbsp;</i></a> -->
        <?php } ?>
        <div class="center footer pb-15" >
            <hr>
            CopyRight&copy;2012 - <?php echo date('Y');?> 技术支持：<a href="javascript:void();" target="_blank">富泽法兰</a>
        </div>
        <a href="#" id="btn-scroll-up" class="btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only"></i>
		</a>
        <div>
            <audio id="audio">
                <source src="http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=3&text=您有新消息，请尽快处理" type="audio/mpeg">
                <embed height="0" width="0" src="http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=3&text=您有新消息，请尽快处理">
            </audio>
        </div>
        <!-- Modal -->
        <div id="loading-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-body">
            <p class="text-center fs-24 red"><i class="icon-smile"></i> 正在提交中，请稍等…</p>
          </div>
        </div>
        <div id="qr-pay-modal-common" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="modal-title">请您打开微信扫一扫此二维码</h3>
              </div>
              <div class="modal-body">
                  <div class="text-center"  id="qr-modal-body">
                      
                  </div>
                  <p class="text-center">谢谢您的光临，欢迎下次再来！</p>
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary btn-small" data-dismiss="modal" aria-hidden="true">完成</button>                    
            </div>
        </div>
        <script type="javascript/template" id="message-dialog-tpl">            
            <div class="alert {style}">
                <button type="button" class="close" data-dismiss="alert" id="message-dialog-id-{id}">
                    <i class="icon-remove"></i>
                </button>
                {content}
            </div>
        </script>
        <script type='text/javascript' src="<?php echo HResponse::uri();?>js/hjz-extend.js?v=<?php echo $v; ?>"></script>
        <script type='text/javascript' src="<?php echo HResponse::uri();?>js/hjz-tags.js?v=<?php echo $v; ?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/hjz-uploader.js?v=<?php echo $v; ?>"></script>
        <script type='text/javascript' src="<?php echo HResponse::uri();?>js/tpl-map.js?v=<?php echo $v; ?>"></script>
        <script type='text/javascript' src="<?php echo HResponse::uri();?>js/common.js?v=<?php echo $v; ?>"></script>
        <script type='text/javascript' src="<?php echo HResponse::uri();?>js/upload-file.js?v=<?php echo $v; ?>"></script>
        <script type="text/javascript"> var cat = "<?php echo HRequest::getParameter('cat'); ?>"; </script>
        <div class="hide">
            <?php //echo HString::decodeHtml($siteCfg['tongji_code']); ?>
        </div>
