                    <div id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
                                <i class="icon-home"></i> <a href="<?php echo HResponse::url('', '', 'admin'); ?>">后台桌面</a>
                                <span class="divider"><i class="icon-angle-right"></i></span>
                            </li>
                            <li>
                                <a href="<?php echo HResponse::url('apps'); ?>">更多</a>
                                <span class="divider"><i class="icon-angle-right"></i></span>
                            </li>
							<li><?php echo $modelZhName; ?></li>
						</ul><!--.breadcrumb-->
						<div id="nav-search">
                            <span id="time-info">正在加载时钟...</span>
						</div><!-- #nav-search -->
					</div><!-- #breadcrumbs -->
                    <div id="page-content" class="clearfix">
						<div class="page-header position-relative">
							<h1>
                                <a href="javascript:history.go(-1);" class="btn btn-mini pull-right btn-info">
                                    <i class="icon icon-arrow-left"></i> 返回
                                </a>
                                <?php
                                    $title  = HResponse::getAttribute('title');
                                    if(!$title) {
                                        HTranslate::_($modelZhName);
                                    } else {
                                        echo $title;
                                    }
                                ?>
                            </h1>
						</div><!--/page-header-->                   
