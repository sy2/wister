        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
           <div class="container-fluid">
              <a class="brand" title="<?php echo $siteCfg['name'];?>" href="<?php echo HResponse::url('', '', 'admin'); ?>">
                  <img src="<?php echo HResponse::touri($siteCfg['image_path']); ?>" style="height: 25px;" class="avatar"/>
                  <small><?php echo HString::cutString($siteCfg['name'], 10, '...'); ?>管理后台</small>
              </a>
              <ul class="nav ace-nav pull-right">
                    <li class="light-blue user-profile">
                        <a class="user-menu dropdown-toggle" href="#" data-toggle="dropdown">
                            <img src="<?php echo !HSession::getAttribute('image_path', 'user') ? HResponse::uri('admin') . '/avatars/user.jpg' :
                            HResponse::url() . HSession::getAttribute('image_path', 'user'); ?>" class="nav-user-photo" />
                            <span id="user_info">
                                <small><?php HTranslate::_('您好'); ?>,</small>
                                <?php echo HSession::getAttribute('name', 'user'); ?>
                            </span>
                            <i class="icon-caret-down"></i>
                        </a>
                        <ul id="user_menu" class="pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                            <li>
                                <a href="<?php echo HResponse::url('user/editview', 'id=' . HSession::getAttribute('id', 'user')); ?>">
                                    <i class="icon-user"></i><?php HTranslate::_('个人设置'); ?>
                                </a>
                            </li>
                            <li class="divider"></li> 
                            <li>
                                <a href="<?php echo HResponse::url('auser/logout', '', 'oauth'); ?>">
                                    <i class="icon-off"></i><?php HTranslate::_('安全退出'); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
              </ul><!--/.ace-nav-->
           </div><!--/.container-fluid-->
          </div><!--/.navbar-inner-->
        </div><!--/.navbar-->
