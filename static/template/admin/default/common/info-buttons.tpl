                                                    <hr/>
                                                    <div class="control-group text-right btn-form-box" data-spy="affix" data-offset-top="360" >
                                                        <button type="reset" class="btn btn-small">重置</button>
                                                        <?php if($record && $record['id']) { ?>
                                                        <button type="submit" class="btn btn-success btn-small" id="btn-form-save-goback">
                                                            <i class="icon-save"></i>
                                                            更新并返回
                                                        </button>
                                                        <?php } else { ?>
                                                        <button type="button" class="btn btn-info btn-small" id="btn-save-continue">
                                                            <i class="icon-save"></i>
                                                            保存并继续添加
                                                        </button>
                                                        <button type="submit" id="btn-form-save-goback" class="btn btn-success btn-small">
                                                            <i class="icon-save"></i>
                                                            保存并返回
                                                        </button>
                                                        <?php } ?>
                                                    </div>
