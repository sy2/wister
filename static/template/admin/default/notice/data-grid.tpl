                                <form action="<?php echo HResponse::url('' . $modelEnName . '/quick'); ?>" method="post" id="list-form">
                                    <div class="dialogs">
                                        <?php
                                            if(HVerify::isEmpty(HResponse::getAttribute('list'))) {
                                                echo '<p class="text-center">暂无相关记录</p>';
                                            }
                                            $authorMap  = HResponse::getAttribute('authorMap');
                                            foreach(HResponse::getAttribute('list') as $key => $record) {
                                                $author = $authorMap[$record['author']];
                                        ?>
                                        <div class="itemdiv dialogdiv">
                                            <div class="user">
                                                <img alt="<?php echo $record['parent_id'] ?> Avatar" src="<?php echo BaseAction::getAvatar($record['parent_id']); ?>">
                                            </div>
                                            <div class="body">
                                                <div class="time">
                                                    <i class="icon-eye"></i>
                                                    <?php echo HResponse::formatText('status', $record); ?>
                                                    <i class="icon-time"></i>
                                                    <span class="green"><?php echo HString::formatTime($record['create_time']);?></span>
                                                </div>
                                                <div class="name">
                                                    <strong>
                                                     <?php if(!$record['parent_id']) { ?>
                                                     系统
                                                    <?php } else { ?>
                                                    <a href="<?php echo HResponse::url('user', 'id=' . $record['author']);?>"><?php echo $author['name'];?></a>
                                                    <?php } ?>
                                                    </strong>
                                                     <em>发给</em>
                                                    <strong>
                                                     <?php if(!$record['parent_id']) { ?>
                                                     系统
                                                     <?php } else { ?>
                                                    <a href="<?php echo HResponse::url('user', 'id=' . $record['parent_id']);?>"><?php echo HResponse::formatText('parent_id', $record);?></a>
                                                     <?php } ?>
                                                     </strong>
                                                </div>
                                                <div class="text">
                                                    <?php echo HString::decodeHtml($record['content']);?>
                                                </div>
                                                <div class="tools">
                                                    <?php 
                                                        require(HResponse::path() . '/common/single-lang-actions.tpl');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <div class="dataTables_info" id="table_report_info">
                                                共 <?php echo HResponse::getAttribute('totalRows');?> 条
                                                当前: <?php echo HResponse::getAttribute('curPage') . '/' . HResponse::getAttribute('totalPages')?></strong>页
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="dataTables_paginate paging_bootstrap pagination">
                                                <ul><?php echo HResponse::getAttribute('pageHtml');?></ul>
                                            </div>
                                        </div>
                                        <div class="span3 txt-right">
                                            <div class="quick-operation">
                                                <label>批量操作:</label>
                                                <select name="operation" class="span7" id="operation">
                                                    <option value="">选择操作</option>
                                                    <option value="delete">删除</option>
                                                    <?php
                                                        foreach(HPopoHelper::getMoreFields($popo) as $field => $cfg) {
                                                            echo '<option value="' . $field . '">'. $cfg['name'] . '</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
