<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/category/cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                        <div class="span4">
                            <h4 class="page-header">
                                <i class="icon-plus"></i> 添加新分类
                            </h4>
                            <div class="content-box">
                                <form action="<?php echo HResponse::url('category/add'); ?>" method="POST" id="info-form">
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <?php $field = 'sort_num'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span8">
                                            <?php $field = 'identifier'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                    </div>
                                    <?php $field = 'parent_id'; require(HResponse::path() . '/fields/tree.tpl'); ?>
                                    <?php $field = 'parent_path'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'description'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
                                    <?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
                                    <?php $field = 'is_recommend'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <input type="hidden" name="author" value="<?php echo HSession::getAttribute('id', 'user');?>">
                                    <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); ?>
                                </form>
                            </div>
                        </div>
                        <div class="span8">
                            <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                                <?php require_once(HResponse::path() . '/common/list-tool-bar.tpl'); ?>
                                <?php require_once(HResponse::path() . '/fields/data-grid.tpl'); ?>
                            <!-- PAGE CONTENT ENDS HERE -->
                            </div><!--/row-->
                            </div><!--/#page-content-->
                        </div>
                    </div><!-- #main-content -->
                </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>      
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>    
	</body>
</html>
