                                <div class="row-fluid">
                                    <form id="search-form" action="<?php echo HResponse::url('' . $modelEnName . '/search'); ?>" method="get">
                                        <div class="span3">
                                            <div id="table_report_length" class="dataTables_length">
                                                    <label>每页显示:
                                                        <select size="1" name="perpage" id="perpage" aria-controls="table_report" data-cur="<?php echo HRequest::getParameter('perpage'); ?>">
                                                            <option value="10" selected="selected">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                        条
                                                </label>
                                            </div>
                                        </div>
                                        <div class="span9 txt-right f-right">
                                            <label>
                                                <select name="status" id="status" class="auto-select input-medium" data-cur="<?php echo HRequest::getParameter('status'); ?>">
                                                    <option value="">全部</option>
                                                    <?php foreach(GoodsPopo::$statusMap as $status) { ?>
                                                    <option value="<?php echo $status['id'];?>"><?php echo $status['name']; ?></option>
                                                    <?php }?>
                                                </select>
                                            </label>
                                            <label>搜索<?php echo $modelZhName;?>: 
                                                <input type="text" class="input-medium search-query" name="keywords" id="keywords" data-def="<?php echo !HRequest::getParameter('keywords') ? '关键字...' : HRequest::getParameter('keywords'); ?>">
                                                <button type="submit" class="btn btn-purple btn-small">搜索<i class="icon-search icon-on-right"></i></button>
                                            </label>
                                        </div>
                                    </form>
                                </div>
