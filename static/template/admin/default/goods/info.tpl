<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form" data-ajax="2">
                            <div class="row-fluid">
                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs" id="myTab">
                                            <li class="active">
                                                <a id="tab-step-baseinfo" data-toggle="tab" href="#tab-baseinfo">
                                                    <i class="green icon-info-sign bigger-110"></i>
                                                    基本信息
                                                </a>
                                            </li>
                                            <li >
                                                <a id="tab-step-album" data-toggle="tab" href="#tab-album">
                                                    封面相册
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-baseinfo" class="tab-pane in active">

                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    <?php $field = 'parent_id'; require(HResponse::path() . '/fields/tree.tpl'); ?>
                                    <?php $field = 'shop_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <div class="row-fluid">
                                        <div class="span6">
                                        <?php $field = 'code'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                        <div class="btn btn-small btn-success" id="btn-add">添加</div>
                                        </div>
                                    </div>
                                    <div class="row-fluid mt-10">
                                        <table id="suoti-box" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <th width="10%">编号</th>
                                            <th width="10%">颜色</th>
                                            <th width="10%">锁头</th>
                                            <th width="10%">锁体</th>
                                            <th width="10%">价格</th>
                                            <th width="40%">图片</th>
                                            <th width="10%">操作</th>
                                        </tr>
                                        <?php if ($record['list']) { ?>
                                        <?php foreach ($record['list'] as $key => $item) { ?>
                                        <tr id="item-<?php echo $key; ?>">
                                            <td class="text-center">
                                                <input type="text" name="code[]" value="<?php echo $item['code']; ?>"/>
                                            </td>
                                            <td class="text-center">
                                                <input type="text" readonly name="color[]" value="<?php echo $item['color']; ?>"/>
                                            </td>
                                            <td class="text-center">
                                                <input type="text" name="suotou[]" value="<?php echo $item['suotou']; ?>"/>
                                            </td>
                                            <td>
                                                <input type="text" name="suoti[]" value="<?php echo $item['suoti']; ?>" />
                                            </td>
                                            <td>
                                                <input type="text" name="price[]" value="<?php echo $item['price']; ?>"/>
                                            </td>
                                            <td>
                                                <div class="old-file-box">
                                                    <a  target="_blank" href="<?php echo HResponse::touri($item['image_path']); ?>" class="lightbox" id="sku-img-<?php echo $key;?>-lightbox">
                                                        <?php if($item['image_path']) { ?>
                                                        <img src="<?php echo HResponse::touri($item['image_path']); ?>" />
                                                        <?php }?>
                                                    </a>
                                                </div>
                                                <button type="button"
                                                        data-field="image_path"
                                                        data-type="image"
                                                        data-target="sku-img-<?php echo $key;?>"
                                                        class="btn btn-info btn-mini span12 btn-file btn-sku-img"
                                                ><i class="icon-file"></i></button>
                                                <input type="hidden" name="sku_image_path[]"
                                                       id="sku-img-<?php echo $key;?>"
                                                       value="<?php echo $item['image_path'];?>" />
                                                <script type="text/javascript">
                                                    <?php $timestamp    = time() . rand(1000, 9999); ?>
                                                    formData['sku-img-<?php echo $key; ?>']   = {
                                                        'timestamp' : '<?php echo $timestamp; ?>',
                                                        'token'     : '<?php echo md5($timestamp);?>',
                                                        'model'     : 'goods',
                                                        'field'     : 'image_path',
                                                        'nolinked'  : 1
                                                    };
                                                </script>
                                            </td>
                                            <td class="del-item">
                                               <a class="btn btn-danger btn-mini del-item" data-id="<?php echo $key; ?>">x</a>
                                            </td>
                                        </tr>
                                        <?php } } ?>
                                        </table>
                                    </div>
                                    <?php $field = 'first_word'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'brand_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'activity_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'parent_path'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <div class="row-fluid hide">
                                    <div class="span4">
                                        <?php $field = 'max_price'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    </div>
                                    <div class="span4">
                                    <?php $field = 'content'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    </div>
                                    <div class="span4">
                                        <?php $field = 'attr_data'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    </div>
                                    </div>
                                    <div class="row-fluid hide">
                                        <div class="span4">
                                            <?php $field = 'hash'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'adv_img'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'is_new'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                    </div>
                                    <div class="row-fluid hide">
                                        <div class="span4">
                                            <?php $field = 'is_show'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'buy_type'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'score'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                    </div>
                                                <div class="control-group">
                                                    <div class="span6">
                                                    </div>
                                                    <div class="span6 txt-right">
                                                        <a data-toggle="tab-step-attr" class="btn btn-success btn-tab-step" href="javascript:void(0);">下一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>

                                            <div id="tab-album" class="tab-pane">
                                            <?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
                                                <div class="control-group">
                                                    <div class="span6">                                                        
                                                        <a data-toggle="tab-step-attr" class="btn btn-warning btn-tab-step" href="javascript:void(0);">上一步</a>
                                                    </div>
                                                    <div class="span6 txt-right hide">
                                                        <a data-toggle="tab-step-other" class="btn btn-success btn-tab-step" href="javascript:void(0);">下一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div id="tab-other" class="tab-pane">
<?php $field = 'total_comments'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'total_great'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'total_save'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <div class="control-group">
                                                    <div class="span6">                                                        
                                                        <a data-toggle="tab-step-album" class="btn btn-warning btn-tab-step" href="javascript:void(0);">上一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                <!-- PAGE CONTENT ENDS HERE -->
                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'status'; require(HResponse::path() . '/fields/checkbox.tpl'); ?>
                                                <?php $field = 'mk_price'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'total_great'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'sort_num'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                                <?php $field = 'total_orders'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'total_number'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'total_visits'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
<?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/goods/goods-tpl.tpl'); ?>
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.nestable.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/goods-info.js?v=<?php echo $v;?>"></script>
	</body>
</html>
