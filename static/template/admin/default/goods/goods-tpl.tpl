<script type="javascript/template" id="suoti-item-tpl">
<tr id="item-{id}">
    <td class="text-center">
        <input type="text" name="code[]" value=""/>
    </td>
    <td class="text-center">
        <input type="text" readonly name="color[]"  id="color-{id}" />
    </td>
    <td class="text-center">
        <input type="text" name="suotou[]" id="suotou-{id}" />
    </td>
    <td>
        <input type="text" name="suoti[]" class="suoti-{id}" />
    </td>
    <td>
        <input type="text" name="price[]" class="price-{id}" />
    </td>
    <td>
    <div class="old-file-box">
        <a href="###" class="lightbox" id="sku-img-{id}-lightbox" target="_blank"></a>
    </div>
    <button type="button"
            data-field="image_path"
            data-type="image"
            data-target="sku-img-{id}"
            class="btn btn-info btn-mini span12 btn-file btn-sku-img"
    ><i class="icon-file"></i></button>
    <input type="hidden" name="sku_image_path[]" id="sku-img-{id}" value="" />
    </td>
    <td class="del-item">
        <a class="btn btn-danger btn-mini del-item" data-id="{id}">x</a>
    </td>

</tr>
</script>