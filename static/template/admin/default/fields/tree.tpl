        <div class="control-group" id="<?php echo $field; ?>-box">
			<label class="control-label" for="<?php echo $field; ?>">
                <?php echo $popo->getFieldName($field); ?>
                <?php 
                    $verify   = $popo->getFieldAttribute($field, 'verify');
                    if($verify && false === $verify['null']) {
                ?>
                <em class="red">*</em>
                <?php } ?>
            </label>
            <div class="controls">
                <select  name="<?php echo $field; ?>" 
                id="<?php echo $field; ?>" 
                data-cur="<?php echo !empty($record[$field]) ? $record[$field] : $popo->getFieldAttribute($field, 'default'); ?>" 
                class="auto-select span12">
                    <option value="0">请选择</option>
                    <?php 
                        HClass::import('hongjuzi.utils.HTree');
                        $parentField    = !$parentField ? $field : $parentField;
                        $hTree  = new HTree(
                            HResponse::getAttribute($field . '_list'),
                            'id',
                            $parentField,
                            'name',
                            'id',
                            '<option value="{id}">' .
                            '{name}' .
                            '</option>'
                        );
                        echo $hTree->getTree();
                    ?>
                </select>
                <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
            </div>
            <div class="clearfix"></div>
		</div>
        <script type="text/javascript"> selectList.push('#<?php echo $field; ?>');</script>
        <?php $parentField = null; ?>
