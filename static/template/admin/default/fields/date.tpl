                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <label class="control-label" for="<?php echo $field; ?>">
                                        <?php echo $popo->getFieldName($field); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <?php $time     = false === strpos($record[$field], '-') ? $record[$field] : strtotime($record[$field]);?>
                                        <div class="input-append date">
                                            <input class="span11 date-picker" id="<?php echo $field; ?>" type="text" 
                                            name="<?php echo $field; ?>" 
                                            value="<?php echo empty($time) ? date('Y-m-d') : date('Y-m-d', $time); ?>" 
                                            id="<?php echo $field; ?>"
                                            data-comment="<?php echo $popo->getFieldComment($field); ?>"
                                            placeholder="请输入<?php echo $popo->getFieldName($field); ?>" data-date-format="yyyy-mm-dd"/>
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                            <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <script type="text/javascript">
                                    dateList.push("#<?php echo $field;?>");
                                </script>
