                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <label class="control-label" for="<?php echo $field; ?>">
                                        <?php echo $popo->getFieldName($field); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <textarea id="<?php echo $field; ?>" class="span6" placeholder="添加<?php echo $field; ?>" name="<?php echo $field; ?>"><?php echo $record[$field]; ?></textarea>
                                        <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <script type="text/javascript">codeEditorList.push('<?php echo $field; ?>');</script>
