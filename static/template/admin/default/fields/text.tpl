                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <label class="control-label" for="<?php echo $field; ?>">
                                        <?php echo $popo->getFieldName($field); ?>
                                        <?php $def      = $popo->getFieldAttribute($field, 'default'); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="<?php echo $field; ?>"
                                        name="<?php echo $field; ?>" 
                                        autocomplete="off"
                                        class="span12 input-field-<?php echo $field; ?>"
                                        value="<?php echo $record[$field]; ?>"
                                        placeholder="请输入<?php echo $popo->getFieldName($field); ?><?php echo $def ? '，默认为：' . $def . '。' : '';?>" 
                                        data-def='<?php echo $def;?>'
                                        data-verify='<?php echo json_encode($popo->getFieldVerifyCfg($field));?>' />
                                        <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                               
