                                <?php $fileType     = str_replace('.', '', implode('|', $popo->getFieldAttribute($field, 'type'))); ?> 
                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <label class="control-label" for="<?php echo $field; ?>">
                                        <?php echo $popo->getFieldName($field); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <div class="span12">
                                            <button type="button"
                                                data-field="<?php echo $field; ?>"
                                                data-type="file" 
                                                class="btn btn-white btn-mini span12 btn-file"
                                            ><i class="icon-file"></i> 请选择文件</button>
                                            <input name="<?php echo $field; ?>" type="hidden" id="<?php echo $field; ?>" value="<?php echo $record[$field]; ?>"/>
                                            <div class="clearfix"></div>
                                            <div id="demo-<?php echo $field;?>" class="old-file-box">
                                                 <a id="demo-file-path-<?php echo $field;?>" href="<?php echo HResponse::touri($record[$field]); ?>" ><?php echo $record[$field];?></a>
                                                <input type="hidden" name="old_<?php echo $field;?>" value="<?php echo $record[$field]; ?>" />
                                                <a href="#<?php echo $field;?>" style="<?php echo $record['image_path'] ? '' : 'display:none';?>" class="btn btn-mini btn-danger btn-remove-file"><i class="icon-trash"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    <?php $timestamp    = time() . rand(1000, 9999); ?>
                                    formData['<?php echo $field; ?>']        = {
                                        'timestamp' : '<?php echo $timestamp; ?>',
                                        'token'     : '<?php echo md5('unique_salt' .  $timestamp);?>', 
                                        'model'     : modelEnName,
                                        'field'     : '<?php echo $field; ?>',
                                        'nolinked'  : 1
                                    };
                                </script>
