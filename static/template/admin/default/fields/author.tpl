                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <?php $author = HResponse::getAttribute('author')?>
                                    <label class="control-label" for="author">
                                        <?php echo $popo->getFieldName('author'); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="author" class="span12" placeholder="<?php echo $popo->getFieldName('author'); ?>" value="<?php echo $author; ?>" readonly="readonly" />
                                        <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
