                                <div class="control-group" id="<?php echo $field; ?>-box">
                                    <label class="control-label" for="<?php echo $field; ?>">
                                        <?php echo $popo->getFieldName($field); ?>
                                        <?php 
                                            $verify   = $popo->getFieldAttribute($field, 'verify');
                                            if($verify && false === $verify['null']) {
                                        ?>
                                        <em class="red">*</em>
                                        <?php } ?>
                                    </label>
                                    <div class="controls">
                                        <input data-cur="<?php echo !empty($record[$field]) ? $record[$field] : $popo->getFieldAttribute($field, 'default'); ?>"
                                        class="ace-switch ace-switch-4" type="checkbox" value="2"
                                        name="<?php echo $field; ?>" 
                                        verify-data='<?php echo json_encode($popo->getFieldVerifyCfg($field));?>' 
                                        data-def='<?php echo $popo->getFieldDefault($field);?>' />
                                        <span class="lbl"></span>
                                        <small class="help-info"><?php echo $popo->getFieldComment($field); ?></small>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
