<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
            <?php $rootInfo = HResponse::getAttribute('rootInfo');?>
			<div id="main-content" class="clearfix">
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-dashboard"></i> <a href="<?php echo HResponse::url('index'); ?>">后台桌面</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li>
                            <a href="<?php echo HResponse::url('apps'); ?>">更多</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li>
                            <?php echo $rootInfo['name'];?>
                        </li>
                    </ul><!--.breadcrumb-->
                    <div id="nav-search">
                        <span id="time-info">正在加载时钟...</span>
                    </div><!--#nav-search-->
                </div><!--#breadcrumbs-->
                <div id="page-content" class="clearfix">
                    <div class="page-header position-relative">
                        <h1>
                            <a href="javascript:history.go(-1);" class="pull-right btn btn-info btn-mini mt-5">
                                <i class="icon-arrow-left"></i> 返回
                            </a>
                            <?php echo $rootInfo['name'];?>
                        </h1>
                    </div><!--/page-header-->
                    <div class="row-fluid">
                        <?php 
                            $list   = HResponse::getAttribute('list');
                        ?>
                        <div class="apps-cat-box position-relative">
                            <?php foreach($list as $model) { ?>
                                <a href="<?php echo HResponse::url($model['identifier']);?>" class="item-box sub-app-box">
                                    <?php if($model['image_path']) { ?>
                                    <img src="<?php echo HResponse::touri($model['image_path']); ?>" alt="" class="apps-icon">
                                    <?php } ?>
                                    <h3 class="title"><?php echo $model['name'];?></h3>
                                </a>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div><!--/page-header-->
                    </div>
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
    </body>
</html>
