﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url('filedata/doimport'); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">

                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->
                                <div class="control-group">
                                    <label class="control-label" for="type">数据类型： </label>
                                    <div class="controls">
                                        <select  name="type" id="type" data-cur="<?php echo intval(HRequest::getParameter('id')); ?>" 
                                        class="auto-select span12">
                                            <option value="1">联大气象数据</option>
                                            <option value="2">联大污染数据</option>
                                            <option value="3">总站气象数据</option>
                                            <option value="4">总站污染数据</option>
                                        </select>
                                        <small class="help-info">共支持4种数据格式：联大气象、污染数据，总站气象、污染数据。</small>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <script type="text/javascript"> selectList.push("#type"); </script>
                                <div class="control-group">
                                    <label class="control-label" for="file">数据文件：</label>
                                    <div class="controls">
                                        <div class="span12">
                                            <input type="file" id="excel-file"
                                                name="excel_file"
                                                data-field="excel_file"
                                                data-type="|xls|" 
                                                class="file-field"
                                            />
                                        </div>
                                        <span class="help-inline">
                                            请选择需要导入的数据文件，只支持：xls格式。
                                            <em
                                            class="text-red">注意：excel导入的部分所有单元格记得格式换成“文本”，其它格式读取的数据不保证准确。</em>
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="file"><i class="icon-file"></i> 数据模板下载：</label>
                                    <div class="controls">
                                        <div class="span12">
                                            <a href="<?php echo HResponse::url(); ?>runtime/download/liandaqixiang-tpl.xls">联大气象数据模板.xls</a> | 
                                            <a href="<?php echo HResponse::url(); ?>runtime/download/liandawuran-tpl.xls">联大污染数据模板.xls</a> | 
                                            <a href="<?php echo HResponse::url(); ?>runtime/download/zongzhanqixiang-tpl.xls">总站气象数据模板.xls</a> | 
                                            <a href="<?php echo HResponse::url(); ?>runtime/download/zongzhanwuran-tpl.xls">总站污染数据模板.xls</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- PAGE CONTENT ENDS HERE -->

                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>数据提交</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body filedata-form-btns-box">
                                            <div class="control-group btn-form-box" data-spy="affix" data-offset-top="160" >
                                                <button type="submit" class="btn btn-success">开始导入</button>
                                                <button type="reset" class="btn">重置</button>
                                            </div>
                                            <div class="hr"></div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
	</body>
</html>
