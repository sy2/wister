</html>
<html><meta charset="utf-8" />
<table style="border:3px solid #25cb83;width:534px" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><table style="font-size:14px" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td>
          <img>
            <table cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td style="padding:20px 16px;color:#333"><table cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="height:28px;padding-left:14px"><h1>Hi, <?php echo $userInfo['name'];?>!</h1></td>
                    </tr>
                    <tr>
                      <td style="height:28px;padding-left:14px">您正在找回您 <b style="color:#f3750f"><a  style="color:#25cb83;" href="<?php echo $url;?>" target="_blank">蔬享乐园</a></b>的密码</td>
                    </tr>
                    <tr>
                      <td style="color:#666;height:28px;padding-top:24px;padding-left:14px">
                        请点击以下链接重设您的密码：<Br/>
                        <?php echo $pwdUrl;?>，请尽快登陆哦。
                    </tr>
                    <tr>

                      <td style="color:#FF7300;height:28px;padding-top:6px;padding-left:14px;font-size:12px">（该链接在24小时内有效，24小时需要重新生成）</td>
                    </tr>
                    <tr>
                      <td style="color:#999;padding:10px 0 20px 14px;font-size:12px">如果你错误地收到了此电子邮件，你无需执行任何操作！<wbr>此账号密码将不会更改。</td>
                    </tr>
                    <tr>
                      <td style="color:#999;font-size:12px;display:block;border-top:1px dotted #9f9f9f;padding-top:18px;padding-left:14px">我是系统自动发送的邮件，请不要直接回复哦。 <a  style="color:#25cb83;" href="http://hhhvd.dingding168.com">蔬享乐园</a>&nbsp;账户安全团队</td>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table></td>
  </tr>
</tbody></table>
</html>