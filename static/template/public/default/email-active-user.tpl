 <html><meta charset="utf-8" />
<table style="border:3px solid #25cb83;width:534px" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><table style="font-size:14px" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td>
          <img>
            <table cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td style="padding:20px 16px;color:#333"><table cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="height:28px;padding-left:14px">Hi, <?php echo $name;?>!</td>
                    </tr>
                    <tr>
                      <td style="height:28px;padding-left:14px">【蔬享乐园】用户账号激活</td>
                    </tr>
                    <tr>
                      <td style="color:#666;height:28px;padding-top:24px;padding-left:14px">请点击以下链接，激活用户吧</td>
                    </tr>
                    <tr>
                      <td style="padding:10px 0 0 14px"><a href="<?php echo $activeUrl}" style="color:#0082cb;word-break:break-all;word-wrap:break-word;display:inline-block;max-width:540px" target="_blank"><?php echo $activeUrl;?></a></td>
                    </tr>
                    <tr>
                      <td style="color:red;height:28px;padding-top:6px;padding-left:14px;font-size:12px">（该链接在24小时内有效，24小时后需要重新激活）</td>
                    </tr>
                    <tr>
                      <td style="color:#999;padding-top:50px;padding-left:14px;font-size:12px">如果以上链接无法访问，<wbr>请将该网址复制并粘贴至新的浏览器窗口中。</td>
                    </tr>如果直接点击无法打开，请复制链接地址，在新的浏览器窗口里打开。
                    <tr>
                      <td style="color:#999;padding:10px 0 20px 14px;font-size:12px">如果你错误地收到了此电子邮件，你无需执行任何操作！</td>
                    </tr>
                    <tr>
                      <td style="color:#999;font-size:12px;display:block;border-top:1px dotted #9f9f9f;padding-top:18px;padding-left:14px">我是系统自动发送的邮件，请不要直接回复哦。 <a href="http://www.schoolpi.com" target="_blank">蔬享乐园内客服团队</a>&nbsp;</td>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table></td>
  </tr>
</tbody></table>
</html>