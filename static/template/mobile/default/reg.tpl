<?php require(HResponse::path() . 'common/header.tpl'); ?>
<head>
<body>
<?php $isSingle = true; ?>
<?php require(HResponse::path() . 'common/top.tpl'); ?>
<div class="memberTitle"><img src="<?php echo HResponse::uri(); ?>images/member.png">会员注册</div>
<form class="memberForm">
    <li>
        <h3>会员号：</h3>
        <div class="memberFormInput"><input name="name" type="text" class="memberInput" placeholder="" />*</div>
    </li>
    <li>
        <h3>密码：</h3>
        <div class="memberFormInput"><input name="password" type="password" class="memberInput" placeholder="" />*</div>
    </li>
    <li>
        <h3>确认密码：</h3>
        <div class="memberFormInput"><input name="repassword" type="password" class="memberInput" placeholder="" />*</div>
    </li>
    <li>
        <h3>姓名：</h3>
        <div class="memberFormInput"><input name="true_name" type="text" class="memberInput" placeholder="" /></div>
    </li>
    <li>
        <h3>手机：</h3>
        <div class="memberFormInput"><input name="phone" type="text" id="mobilePhone" class="memberInput" placeholder="" />*</div>
    </li>
    <li>
        <h3>验证码：</h3>
        <div class="memberFormInput"><input type="text" class="memberInputShort" id="validateCode" name="code" maxlength="6" /><a id="num" mark="1">获取验证码</a></div>
    </li>
    <li>
        <h3>地区：</h3>
        <div class="memberFormInput"><input name="street" type="text" class="memberInputArea" id="picker" placeholder="点击选择省市区" readonly="readonly" unselectable="on" οnfοcus="this.blur()"></div>
    </li>
    <li>
        <h3>详细地址：</h3>
        <div class="memberFormInput"><input name="street_number" type="text" class="memberInput" placeholder="" /></div>
    </li>
    <li>
        <h3>&nbsp;</h3>
        <input name="openid" type="hidden" value="<?php echo HRequest::getParameter('openid'); ?>"/>
        <div id="reg-now" class="memberFormInput"><input type="button" class="memberRegLong" value="立即注册" /></div>
    </li>
</form>
<script src="<?php echo HResponse::uri(); ?>js/picker.min.js"></script>
<script src="<?php echo HResponse::uri(); ?>js/city.js"></script>
<script src="<?php echo HResponse::uri(); ?>js/index.js"></script>
<?php require(HResponse::path() . 'common/footer.tpl'); ?>
<script>
    //手机号码验证
    $("#mobilePhone").blur(function(){
        try {
            HHJsLib.isPhoneByDom('#mobilePhone', '手机号码');
        } catch (e) {
            alert(e.message);
        }
    });

    //发送短信验证码
    $("#num").click(function(){
        var mobilePhone = $("#mobilePhone").val();
        var url = queryUrl + 'public/sms/asendforregister';
        if(!mobilePhone){
            alert("请输入您的手机号码！");
            return ;
        }
        var mark = $("#num").attr("mark");
        if ("1" == mark) {
            settime(this);
            $.post(url, {phone: mobilePhone},function(response){
                if (response.rs === true) {
                    HHJsLib.info('短信验证码已发送，请查收！');
                } else {
                    HHjsLib.warn(response.message);
                }
            });
        }
    });

    //短信后倒计时
    var countdown=120;
    function settime(obj) {
        if (countdown == 0) {
            $(obj).attr("disabled",false);
            $(obj).attr("mark","1");
            $(obj).html("获取验证码");
            countdown = 120;
            return;
        } else {
            $(obj).attr("disabled", true);
            $(obj).attr("mark", "0");
            $(obj).html("重新发送(" + countdown + ")");
            countdown--;
        }
        setTimeout(function(){settime(obj)}, 1000)
    }
</script>
</body>
</html>
