<?php require(HResponse::path() . 'common/header.tpl'); ?>
<head>
<body>
<?php require(HResponse::path() . 'common/top.tpl'); ?>
<div class="inProList">
    <?php $cglist = HResponse::getAttribute('cglist'); ?>
    <?php foreach($cglist as $item) { ?>
    <li class="cate" data-id="<?php echo $item['id']; ?>" data-href="<?php echo HResponse::url('index/glist', 'pid=' . $item['id']); ?>"><a href="javascript:void(0);">
            <span><img src="<?php echo HResponse::touri($item['image_path']); ?>"></span>
            <h3><?php echo $item['name']; ?></h3>
    </a></li>
    <?php } ?>
</div>
<div class="quickNav">
    <?php $artlist = HResponse::getAttribute('artlist'); ?>
    <?php foreach($artlist as $item) { ?>
    <li class="art-item" data-id="<?php echo $item['id']; ?>" data-href="<?php echo HResponse::url('article', 'id=' . $item['id']); ?>"><a href="javascript:void();">
            <img src="<?php echo HResponse::touri($item['image_path']); ?>"><?php echo $item['name']; ?>
        </a></li>
   <?php } ?>
</div>
<?php require(HResponse::path() . 'common/footer.tpl'); ?>
</body>
</html>