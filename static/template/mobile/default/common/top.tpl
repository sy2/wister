<?php if ($isSingle) { ?>
    <div id="topBar">
    <div class="logo"><a href="<?php echo HResponse::url(); ?>">
            <img src="<?php echo HResponse::touri($siteCfg['image_path']); ?>"></a></div>
    </div>
<?php } else { ?>
<div id="topBar">
    <div class="logo"><a href="<?php echo HResponse::url(); ?>"><img src="<?php echo HResponse::touri($siteCfg['image_path']); ?>"></a></div>
    <div class="search">
        <form action="<?php echo HResponse::url('index/glist'); ?>" method="get" name="form">
            <input name="name" type="text" class="keywordStyle" placeholder="产品编号" value="<?php echo HRequest::getParameter('name'); ?>"/>
            <input type="submit" class="searchBtn" value="搜索" />
        </form>
    </div>
</div>
<?php if(!HSession::getAttribute('id', 'user')) { ?>
<div class="member">
    <input name="name" type="text" class="memberName" placeholder="会员号" />
    <input name="password" type="password" class="memberPassword" placeholder="密码" />
    <input type="submit" id="login-btn" class="memberLogin" value="登陆" />
    <input type="submit" class="memberReg" id="reg-btn" value="注册" />
</div>
<?php } else { ?>
    <div class="memberSuccess">
    您好，会员 <b><?php echo HSession::getAttribute('name', 'user'); ?></b>,欢迎您的光临！
</div>
<?php }?>
<?php } ?>
