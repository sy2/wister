<div class="bottomBar">
    <a href="tel:<?php echo $siteCfg['phone']; ?>">
        <img src="<?php echo HResponse::uri(); ?>images/phone.png">
        客户关爱热线：<?php echo $siteCfg['phone']; ?></a>
</div>
<script type="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.3.2.js"></script>
<script>
    var userAgent = navigator.userAgent;
    var flag = userAgent.indexOf('miniProgram') > - 1 ? true : false;

    $('.cate').on('click', function() {
        var id = $(this).attr('data-id');
        if (flag) {
            wx.miniProgram.navigateTo({url: '/pages/logs/logs?pid=' + id});
        } else {
            window.location.href = $(this).attr('data-href');
        }
    });

    $('#bottomBar').on('click', function() {
        wx.miniProgram.navigateTo({url: '/pages/index/index'})
    })

    $('#login-btn').on('click', function() {
        var name     = $('.memberName').val();
        var password = $('.memberPassword').val();
        var url = queryUrl + 'oauth/auser/alogin';
        $.getJSON(url, {name:name, password:password}, function(response) {
            if (true === response.rs) {
                window.location.reload();
            } else {
                alert(response.message);
            }
        })
    });

    $('#reg-btn').on('click', function() {
        if (flag) {
            wx.miniProgram.navigateTo({url: '/pages/logs/logs'});
        } else {
            window.location.href = queryUrl + 'mobile/index/reg';
        }
    });

    $('#reg-now').on('click', function() {
        var data = $('.memberForm').serialize();
        var url  = queryUrl + 'oauth/auser/aregister'
        $.getJSON(url, data, function(response) {
            alert(response.message);
        });
    });

    $('.art-item').on('click', function() {
        var id = $(this).attr('data-id')
        if (flag) {
            wx.miniProgram.navigateTo({url: '/pages/logs/logs?id=' + id});
        } else {
            window.location.href = $(this).attr('data-href');
        }
    });

    $('.view-price').on('click', function() {
        if (flag) {
            wx.miniProgram.navigateTo({url: '/pages/reg/reg'});
        } else {
            window.location.href = queryUrl + 'mobile/index/reg';
        }
    });

</script>

