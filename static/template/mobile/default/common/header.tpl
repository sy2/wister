<!DOCTYPE html>
<html>
<head>
    <?php $siteCfg = HResponse::getAttribute('siteCfg'); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $siteCfg['name']; ?></title>
    <meta name="Keywords" content="<?php echo $siteCfg['seo_keywords']; ?>" />
    <meta name="Description" content="<?php echo $siteCfg['seo_desc']; ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo HResponse::uri(); ?>css/style.css?v=">
    <script src="<?php echo HResponse::uri(); ?>js/jquery-1.8.3.min.js"></script>
    <script>
        queryUrl = '<?php echo HResponse::url(); ?>index.php/';
    </script>
    <script type='text/javascript' src="<?php echo HResponse::uri('cdn');?>hhjslib/hhjslib.min.js?v=<?php echo $v; ?>"></script>

