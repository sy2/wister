<?php $title = '富泽法兰'; ?>
<?php require(HResponse::path() . 'common/header.tpl'); ?>
<head>
<body>
<div class="logo"><a href="index.html"><img src="<?php echo HResponse::uri(); ?>images/logo.png"></a></div>
<div class="categoryList">
    <table class="tableTwo">
        <tbody>
            <tr>
                <th class="name" colspan="5">参数对比</th>
            </tr>
            <?php
                $list     = HResponse::getAttribute('list');
                $cateMap  = HResponse::getAttribute('cateMap');
                $dnpnList = array();
                $catList  = array();
                $priceList = array();
                $pgzList   = array();
                $wjList    = array();
                $wjfwList  = array();
                $njList    = array();
                $njfwList  = array();
                $hdList    = array();
                $hdfwList  = array();
                $zxjList   = array();
                $ksList    = array();
                $kjList    = array();
                $twjList   = array();
                $tgList    = array();
                $ids       = array();
                foreach($list as $item) {
                  $dnpnList[]  = $item['code'] . '/' . $item['shop_id'];
                  $catList[]   = $cateMap[$item['parent_id']]['name'];
                  $priceList[] = $item['mk_price'];
                  $pgzList[]   = $item['brand_id'];
                  $wjList[]    = $item['activity_id']; //外径
                  $wjfwList[]  = $item['score']; //外径范围
                  $njList[]    = $item['activity_id']; //内径
                  $njfwList[]  = $item['buy_type']; //内径范围
                  $hdList[]    = $item['max_price']; //厚度
                  $hdfwList[]  = $item['is_show']; //厚度范围
                  $zxjList[]   = $item['content']; //中心距
                  $ksList[]    = $item['attr_data']; //孔数
                  $kjList[]    = $item['hash']; //孔径
                  $twjList[]   = $item['adv_img']; //台外径
                  $tgList[]    = $item['is_new']; //台高
                  $ids[]       = $item['id']; 
               }
            ?>
            <?php if (count($list) < 2) { ?>
            <tr>
                <td colspan="3">至少需要2条数据对比</td>
            </tr>
            <?php } else { ?>
            <tr>
                <td>DN/PN</td>
                <?php foreach ($dnpnList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>分类</td>
                <?php foreach ($catList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>价格(元)</td>
                <?php foreach ($priceList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>配管子</td>
                <?php foreach ($pgzList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>外径(mm)</td>
                <?php foreach ($wjList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>外径范围(mm)</td>
                <?php foreach ($wjfwList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>内径(mm)</td>
                <?php foreach ($njList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>内径范围(mm)</td>
                <?php foreach ($njfwList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <!--
            <tr>
              <td>小内径(mm)</td>
              <td>10</td>
              <td>10</td>
              <td>10</td>
              <td>10</td>
            </tr>-->
            <tr>
              <td>厚度(mm)</td>
              <?php foreach ($hdList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>厚度范围(mm)</td>
             <?php foreach ($hdfwList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>中心距(mm)</td>
              <?php foreach ($zxjList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>孔数(个)</td>
             <?php foreach ($ksList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>孔径(mm)</td>
              <?php foreach ($kjList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>台外径(mm)</td>
              <?php foreach ($twjList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>台高(mm)</td>
              <?php foreach ($tgList as $item) { ?>
                <td><?php echo $item; ?></td>
                <?php } ?>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <?php foreach ($ids as $key => $id) { ?>
              <td><a class="del" data-index="<?php echo $key; ?>"  data-id="<?php echo $id; ?>"href="javascript:void(0);">删除</a></td>
              <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="FaLanDetail" class="falandetail">
    <div class="bg"></div>
    <iframe src="" scrolling="no"></iframe>
    <div class="close" onclick="HideFaLan()">X</div>
    <script>
        $(function(){
            $(".del").click(function(){
                 var index = parseInt($(this).attr('data-index'));
                 console.log(index);
                 console.log("table tr td:nth-child(" + (index + 2 ) + ")");
                 var id    = $(this).attr('data-id')
                 $.getJSON(
                    queryUrl + 'cms/index/delete',
                    {id: id},
                    function(response) {
                        window.location.reload();
                  })
            })
        })
    </script>
</div>
<div id="bottomBar"><li>小程序制作联系：138 1973 9111</li></div>
</body>
</html>
