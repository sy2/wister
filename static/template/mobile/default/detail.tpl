<?php require(HResponse::path() . 'common/header.tpl'); ?>
<head>
<body>
<?php require(HResponse::path() . 'common/top.tpl'); ?>
<?php $cateName = HResponse::getAttribute('cate_name'); ?>
<div class="listTitle"><?php echo $cateName; ?></div>
<div class="proView">
    <table class="table">
        <tbody>
        <tr>
            <th>产品图片</th>
            <th>编号/颜色</th>
            <th>锁头/锁体</th>
            <th>价格（元）</th>
        </tr>
        <?php $list = HResponse::getAttribute('list'); ?>
        <?php foreach($list as $item) { ?>
        <tr>
            <td><img src="<?php echo HResponse::touri($item['image_path']); ?>"></td>
            <td><?php echo $item['code'] . $item['color']; ?></td>
            <td><?php echo $item['suotou'] . $item['suoti']; ?></td>
            <?php if (HSession::getAttribute('area', 'user') == 2) { ?>
            <td><?php echo $item['price']; ?></td>
            <?php } else { ?>
            <td><span class="orgTxt view-price">查看价格</span></td>
            <?php } ?>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php require(HResponse::path() . 'common/footer.tpl'); ?>
</body>
</html>
