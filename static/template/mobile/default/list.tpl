<?php require(HResponse::path() . 'common/header.tpl'); ?>
<head>
<body>
<?php require(HResponse::path() . 'common/top.tpl'); ?>
<?php $cateInfo = HResponse::getAttribute('cate_record'); ?>
<div class="listTitle"><?php echo $cateInfo['name']; ?></div>
<div class="proList">
    <?php $list = HResponse::getAttribute('list'); ?>
    <?php foreach($list as $item) { ?>
    <li><a href="<?php echo HResponse::url('index/detail', 'id=' . $item['id']); ?>">
            <span><img src="<?php echo HResponse::touri($item['image_path']); ?>"></span>
            <h3><?php echo $item['name']; ?></h3>
        </a>
    </li>
    <?php } ?>
</div>
<?php require(HResponse::path() . 'common/footer.tpl'); ?>
</body>
</html>
