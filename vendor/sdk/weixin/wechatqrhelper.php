<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信验证帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatQrHelper extends Wechat
{

    /**
     * @var private static $_ticketUrl 请求地址 
     */
    private static $_ticketUrl = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={ACCESS_TOKEN}';

    /**
     * @var private static $_qrURl QR生成请求地址
     */
	private static $_qrURl     = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={TICKET}';

    /**
     * 得到临时二维码
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public static
     * @param $id 需要的参数
     * @param $expire 过期时间
     * @return String 链接地址
     */
    public function getTempQrUrl($id, $expire = 2592000)
    {
        $params  = array(
			"expire_seconds" => $expire, //最大值30天
            "action_name" => "QR_SCENE",
            "action_info" => array("scene" => array('scene_id' => $id))
        );

        return array('url' => $this->_requestQrUrl($params), 'scene_id' => $id, 'expire_seconds' => $expire);
    }

    /**
     * 得到永久二维码链接
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public static
     * @param $id 参数
     * @return String 链接地址
     */
    public function getStaticQrUrlById($id)
    {
        $params  = array(
			"action_name" => "QR_LIMIT_SCENE",
            "action_info" => array("scene" => array('scene_id' => $id))
        );

        return array('url' => $this->_requestQrUrl($params), 'scene_id' => $id);
    }

    /**
     * 通过str参数得到链接地址
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public static
     * @param $str str参数 64位长度
     * @return String 链接地址
     */
    public function getStaticQrUrlByStr($str)
    {
        $params  = array(
			"action_name" => "QR_LIMIT_SCENE",
            "action_info" => array("scene" => array('scene_str' => $str))
        );
        
        return array('url' => $this->_requestQrUrl($params), 'scene_str' => $str);
    }

    /**
     * 请求QR_URL
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private static
     * @param $params 参数
     * @return String 二维码地址
     */
    private function _requestQrUrl($params)
    {
        $json   = HRequest::post(
            str_replace('{ACCESS_TOKEN}', $this->_accessToken, self::$_ticketUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('二维码生成失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg'] . ':' . $json['msgid']);
        }

		return str_replace('{TICKET}', $json['ticket'], self::$_qrURl);
    }

}
