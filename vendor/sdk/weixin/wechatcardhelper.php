<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信卡券帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatCardHelper extends Wechat
{

    /**
     * @var private static $_createUrl 创建卡券URL
     */
    private static $_createUrl = 'https://api.weixin.qq.com/card/create?access_token={access_token}';

    /**
     * 创建卡券
     * 
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1451025056 }
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  {String} JSON字符串
     */
    public function create($params)
    {
        HLog::write(self::json_encode($params));
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_createUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('创建失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * @var private static $_deleteUrl  删除卡券URL
     */
    private static $_deleteUrl  = 'https://api.weixin.qq.com/card/delete?access_token={access_token}';

    /**
     * 删除卡券
     *
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1451025272}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function delete($cardId)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_deleteUrl),
            self::json_encode(array('card_id' => $cardId))
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('删除卡券失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    /**
     * @var private static $_detailUrl 查看卡券详情URL
     */
    private static $_detailUrl     = 'https://api.weixin.qq.com/card/get?access_token={access_token}';

    /**
     * 查看卡券详情
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function getDetail($cardId)
    {
        $params = array(
            'card_id' => $cardId
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_detailUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('查看卡券详情失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json['card'];
    }

    /**
     * @var private static $_updateUrl   更改卡券信息
     */
    private static $_updateUrl   = 'https://api.weixin.qq.com/card/update?access_token={access_token}';

    /**
     * 更改卡券信息
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function update($params)
    {
        HLog::write(self::json_encode($params));
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_updateUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('更改卡券信息失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    /**
     * @var private static $_modifyStockUrl   修改库存
     */
    private static $_modifyStockUrl   = 'https://api.weixin.qq.com/card/modifystock?access_token={access_token}';

    /**
     * 修改库存
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function modifystock($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_modifyStockUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('修改库存失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    private static $_getCardListUrl     = 'https://api.weixin.qq.com/card/batchget?access_token={access_token}';

    /**
     * [getCardList 得到卡券列表]
     * @param  integer $offset [开始行]
     * @param  [type]  $count  [显示数量]
     * @param  $status 状态类别 
     * “CARD_STATUS_NOT_VERIFY”,
     * 待审核；
     * “CARD_STATUS_VERIFY_FAIL”,
     * 审核失败；
     * “CARD_STATUS_VERIFY_OK”，
     * 通过审核；
     * “CARD_STATUS_DELETE”，
     * 卡券被商户删除；
     * “CARD_STATUS_DISPATCH”，
     * 在公众平台投放过的卡券
     * @return [type]          []
     */
    public function getCardList($offset = 0, $count = 50, $status = array("CARD_STATUS_VERIFY_OK", 'CARD_STATUS_DISPATCH')) 
    { 
        $params = array(
            "offset" => $offset,
            "count" => $count, 
            "status_list" => $status
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_getCardListUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('获取卡券列表失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    private static $_cardInfoUrl    = 'https://api.weixin.qq.com/datacube/getcardmembercarddetail?access_token={access_token}';

    /**
     * [getCardInfo 得到会员卡信息]
     * 
     * @param  [date] $beginDate [开始日期]
     * @param  [date] $startDate [结束日期]
     * @param  [string] $cardId    [会员卡编号]
     * @return [array]            [类别]
     */
    public function getCardInfo($beginDate, $endDate, $cardId)
    {
        $params = array(
            "begin_date" => $beginDate,
            "end_date" => $endDate, 
            "card_id" => $cardId
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_cardInfoUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('获取卡券信息失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * @var private static $_getCardCodeUrl 得到卡券code
     */
    private static $_getCardCodeUrl = 'https://api.weixin.qq.com/card/code/get?access_token={access_token}';

    /**
     * [$_cardStatusMap 卡券状态]
     * @var array
     */
    private static $_cardStatusMap  = array(
        'NORMAL' => '正常',
        'CONSUMED' => '已核销 ',
        'EXPIRE' => '已过期 ',
        'GIFTING' => '转赠中',
        'GIFT_TIMEOUT' => '转赠超时',
        'DELETE' => '已删除',
        'UNAVAILABLE' => '已失效'
    );

    /**
     * 创建卡券
     * 
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1451025056 }
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  {String} JSON字符串
     */
    public function getCardCodeStatus($code, $cardId, $checkConsume = false)
    {
        $params = array(
           "card_id" => $cardId,
           "code" => $code,
           "check_consume" => $checkConsume
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_getCardCodeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(false === $json['can_consume'] || (isset($json['errcode']) && $json['errcode'] > 0)) {
            $msg    = $json['user_card_status'] && isset(self::$_cardStatusMap[$json['user_card_status']]) 
                ? self::$_cardStatusMap[$json['user_card_status']] : '未被添加或被转赠领取';
            throw new HRequestException('您的会员卡' . $msg . '请确认！');
        }

        return $json;
    }

    private static $_qrCodeUrl     = 'https://api.weixin.qq.com/card/qrcode/create?access_token={access_token}';

    /**
     * 得到二维码
     * 
     * @return [type] [description]
     */
    public function getQrCode($cardId, $openid, $code = "")
    {
        $params     = array(
            "action_name" => "QR_CARD", 
            "expire_seconds" => 1800,
            "action_info" => array(
                "card" => array(
                    "card_id" => $cardId, 
                    "code" => $code,
                    "openid" => $openid,
                    "is_unique_code" => false ,
                    "outer_str" => "12b"
                )
            )
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_qrCodeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('获取卡券二维码失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }


    public function active($code, $cardId)
    {

    }

    /**
     * @var private static $_paycellUrl   设置快速买单
     */
    private static $_paycellUrl   = 'https://api.weixin.qq.com/card/paycell/set?access_token={access_token}';

    /**
     * 设置快速买单
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function paycell($cardId)
    {
        $params = array(
            'card_id' => $cardId,
            'is_open' => true
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_paycellUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('设置快速买单失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

}
