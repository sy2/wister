<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信客服帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatCustomHelper extends Wechat
{

    /**
     * @var private $_kfCfg 客服配置
     */
    private $_kfCfg;

    /**
     * 构造函数
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    public function __construct($appid, $secret)
    {
        parent::__construct($appid, $secret);
        $this->_kfCfg   = null;
    }

    /**
     * 设置客服参数
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $cfg 配置
     * @return 当前对象
     */
    public function setKeFuCfg($cfg)
    {
        $this->_kfCfg   = $cfg;

        return $this;
    }

    /**
     * @var private static $_sendUrl  发送信息链接
     */
    private static $_sendUrl = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={access_token}';

    /**
     * 发送文本
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId 接收用户openid
     * @param  $message 消息
     * @return 发送结果
     */
    public function sendText($openId, $message)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'text',
            'text' => array(
                'content' => $message
            )
        );
        $this->_send($data);
    }

    /**
     * 发送图片
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId OPENID
     * @param  $mediaId 媒体编号
     * @throws HRequestException 请求异常
     */
    public function sendImage($openId, $mediaId)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'image',
            'image' => array(
                'media_id' => $mediaId
            )
        );
        $this->_send($data);
    }

    /**
     * 发送微信图文
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId 用户ID
     * @param  $mediaId 素材编号
     * @throws HRequestException 请求异常
     */
    public function sendArticle($openId, $mediaId)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'mpnews',
            'mpnews' => array(
                'media_id' => $mediaId
            )
        );
        $this->_send($data);
    }

    /**
     * 发送语音
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId 用户编号
     * @param  $mediaId 素材编号
     * @throws HRequestException 请求异常
     */
    public function sendVoice($openId, $mediaId)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'voice',
            'voice' => array(
                'media_id' => $mediaId
            )
        );
        $this->_send($data);
    }

    /**
     * 发送视频
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId 用户编号
     * @param  $mediaId 素材编号
     * @throws HRequestException 请求异常
     */
    public function sendVideo($openId, $mediaId)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'video',
            'video' => array(
                'media_id' => $mediaId
            )
        );
        $this->_send($data);
    }

    /**
     * 发送图文
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $openId 用户id
     * @param  $newsList 新闻列表
     * {
     *   "title":"Happy Day",
     *   "description":"Is Really A Happy Day",
     *   "url":"URL",
     *   "picurl":"PIC_URL"
     * }
     * @throws HRequestException 请求异常
     */
    public function sendNews($openId, $newsList)
    {
        $data   = array(
            'touser' => $openId,
            'msgtype' => 'news',
            'news' => array(
                'articles' => $newsList
            )
        );
        $this->_send($data);
    }

    /**
     * 发送数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $data 数据
     * @throws HRequestException 请求异常
     */
    private function _send($data)
    {
        $url    = str_replace('{access_token}', $this->_accessToken, self::$_sendUrl); 
        $rs     = HRequest::post($url, self::json_encode($data));
        $json   = json_decode($rs, true);
        if(0 != $json['errcode']) {
            throw new HRequestException('发送失败！返回值：' . $json['errmsg']);
        }
    }

}
