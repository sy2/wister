<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信JsSDK帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatQrcodeHelper extends Wechat
{

    /**
     * @var 永久二维码 ticket
     */
    private static $_ticketUrl      = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s';

    /**
     * @var 永久二维码 图片
     */
    private static $_qrcodeUrl      = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s';
    
    /**
     * 获取永久二维码 ticket
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @return Array
     */
    public function getTicket($code)
    {
        $url        = sprintf(self::$_ticketUrl, $this->_accessToken);
        // $params     = array(
        //     'action_name' => 'QR_LIMIT_SCENE',
        //     'action_info' => array(
        //         'scene' => array('scene_id' => $code)
        //     )
        // );
        $params     = array(
            'action_name' => 'QR_LIMIT_STR_SCENE',
            'action_info' => array(
                'scene' => array('scene_str' => urlencode($code))
            )
        );
        $result     = HRequest::post($url, self::json_encode($params));
        $result     = json_decode($result, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('获取ticket失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg'] . ':' . $json['msgid']);
        }

        return $result;
    }
     
    /**
     * 获取永久二维码
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @return Array
     */
    public function getQrcode($ticket)
    {
        $url        = sprintf(self::$_qrcodeUrl, urlencode($ticket));
        $json       = HRequest::getContents($url);

        return $json;
    }

}
