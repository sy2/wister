<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信模板消息帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatTemplateMessageHelper extends Wechat
{
    /**
     * @var $private $_sendUrl 发送API
     */
    private static $_sendUrl   = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={ACCESS_TOKEN}';

    /**
     * 得到文本发送模板
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public static
     * @param $data 需要发送的数据
     * 格式：
     * array('to' => 'xxxx', 'from' => 'xxxx', 'content' => 'xxxx')
     * @return {String}
     */
    public function send($params)
    {
        $json   = HRequest::post(
            str_replace('{ACCESS_TOKEN}', $this->_accessToken, self::$_sendUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('发送模板消息失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg'] . ':' . $json['msgid']);
        }

        return $json;
    }

}
