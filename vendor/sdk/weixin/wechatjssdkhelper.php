<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信JsSDK帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatJsSDKHelper extends Wechat
{

    /**
     * @var private static $_jsapiTicketUrl JSAPI_TICKET信息获取URL
     */
    private static $_jsapiTicketUrl    = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi';

    /**
     * 获取用户基本信息
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @return Array
     */
    public function getJsApiTicket()
    {
        $url    = sprintf(self::$_jsapiTicketUrl, $this->_accessToken);
        $json   = json_decode(HRequest::getContents($url), true);
        if($json['errcode']) {
            throw new HVerifyException($json['errmsg']);
        }

        return $json;
    }

    private static $_wxCard     = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=wx_card';

    /**
     * 得到微信卡Ticket
     * 
     * @return [type] [description]
     */
    public function getApiTicketWxCard()
    {
        $url    = sprintf(self::$_wxCard, $this->_accessToken);
        $json   = json_decode(HRequest::getContents($url), true);
        if($json['errcode']) {
            throw new HVerifyException($json['errmsg']);
        }

        return $json;
    }

}
