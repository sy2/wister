<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信卡券门店帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatCardShopHelper extends Wechat
{

    /**
     * @var private static $_shopListUrl 查询门店列表URL
     */
    private static $_shopListUrl = 'https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token={access_token}';

    /**
     * 查询门店列表
     * 
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1444378120 }
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  {String} JSON字符串
     */
    public function getpoilist($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_shopListUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        
        if(0 < $json['errcode']) {
            throw new HRequestException('查询门店列表失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }
    }

}
