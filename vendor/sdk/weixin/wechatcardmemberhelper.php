<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechatcardhelper');

/**
 * 微信卡券 会员卡帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatCardMemberHelper extends WechatCardHelper
{

    /**
     * @var private static $_shopListUrl 拉取会员信息（积分查询）URL
     */
    private static $_userInfoUrl = 'https://api.weixin.qq.com/card/membercard/userinfo/get?access_token={access_token}';

    /**
     * 拉取会员信息（积分查询）
     * 
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1466494654_K9rNz }
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  {String} JSON字符串
     */
    public function getuserinfo($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_userInfoUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        
        if(0 < $json['errcode']) {
            throw new HRequestException('拉取会员信息失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }
    }

    /**
     * 创建会员卡
     */
    public function createmembercard($cardType, $data)
    {
        $params       = array(
            'card' => array(
                'card_type' => $cardType
            )
        );
        $params['card'][strtolower($cardType)]    = $data;
        $json     = $this->create($params);
        if(0 < $json['errcode']) {
            throw new HRequestException('添加会员卡失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    public static $payInfoData         = array(
        'swipe_card ' => array('is_swipe_card' => true) // 是否设置该会员卡支持拉出微信支付刷卡界面
    );

    public static $baseInfo           = array(
        'logo_url' =>  array(false, '卡LOGO'),  //卡券的商户logo，建议像素为300*300。   
        'code_type' => array(false, '展示类型'),           //   Code码展示类型， 
        'pay_info' => array(true, null),   //支付功能结构体，swipe_card结构
        'is_pay_and_qrcode' => array(true, null),   //   是否设置该会员卡中部的按钮同时支持微信支付刷卡和会员卡二维码
        'brand_name' => array(true, null), //商户名字,字数上限为12个汉字。
        'title' => array(false, '标题'), //会员卡标题，字数上限为9个汉字              
        'color' => array(false, '颜色'),   //   卡券颜色。     
        'notice' => array(false, "卡券使用提醒"),  // 使用提醒，字数上限为16个汉字。            
        'description' =>array(true, null),    //   使用说明。      
        'sku' => array(true, array(
            'quantity' => array(true, 100000000)
        )), //库存       
        'date_info' => array(true, array(
            'type' => array(true, 'DATE_TYPE_PERMANENT')
        )),  //使用日期，有效期的信息，有效期时间修改仅支持有效区间的扩大 
        'use_custom_code' => array(true, null), // 是否自定义Code码。填写true或false，默认为false
        'get_custom_code_mode' => array(true, null),
        'bind_openid' => array(true, null), // 是否指定用户领取，填写true或false。默认为false        
        'service_phone' => array(true, null),   //客服电话。
        'location_id_list' => array(true, null),   //  支持更新适用门店列表。
        'use_all_locations' =>array(true, null),    //  支持全部门店，填入后卡券门店跟随商户门店更新而更新               
        'center_title' => array(true, null),   //  会员卡中部的跳转按钮名称 建议用作使用用途                
        'center_sub_title' =>array(true, null),    // 会员卡中部按钮解释wording            
        'center_url' => array(true, null),   //   会员卡中部按钮对应跳转的url            
        'custom_url_name' =>array(true, null),    //  自定义跳转入口的名字。            
        'custom_url' => array(true, null),   //   自定义跳转的URL。            
        'custom_url_sub_title' => array(true, null),   // 显示在入口右侧的提示语。            
        'promotion_url_name' => array(true, null),   // 营销场景的自定义入口名称。            
        'promotion_url' => array(true, null),   //   入口跳转外链的地址链接。            
        'promotion_url_sub_title' => array(true, null),   // 显示在营销入口右侧的提示语。 
        'get_limit' => array(true, 1),   //  每人可领券的数量限制           
        'can_share' => array(true, true),   //   卡券原生领取页面是否可分享            
        'can_give_friend' => array(true, true),    //  卡券是否可转赠
        'need_push_on_view' => array(true, null), // 有效期类型，仅支持更改type为1的时间戳，不支持填入2           
    );

    public static $useCondition = array(
        'accept_category' => array(true, null), // 指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写适用于xxx
        'reject_category' => array(true, null), // 指定不可用的商品类目，仅用于代金券类型，填入后将在券面拼写不适用于xxxx
        'least_cost' => array(true, null), // 满减门槛字段，可用于兑换券和代金券 ，填入后将在全面拼写消费满xx元可用。
        'object_use_for' => array(true, null), // 购买xx可用类型门槛，仅用于兑换 ，填入后自动拼写购买xxx可用。
        'can_use_with_other_discount' => array(true, null), // 不可以与其他类型共享门槛 ，填写false时系统将在使用须知里 拼写“不可与其他优惠共享”， 填写true时系统将在使用须知里 拼写“可与其他优惠共享”， 默认为true
        "can_use_with_membercard" => array(true, null),
    );

    public static $abstract = array(
        'abstract' => array(true, null), // 封面摘要简介
        'icon_url_list' => array(true, null), // 建议图片尺寸像素850*350
    );

    public static $textImageListData = array(
        'image_url' => array(true, null), // 图片链接，必须调用上传图片接口
        'text' => array(true, null), // 图文描述
    );
    
    public static $timeLimitTypeMap = array(
        'MONDAY' => array('id=' => 'MONDAY', 'name' => '周一', 'date' => 1),
        'TUESDAY' => array('id=' => 'TUESDAY', 'name' => '周二', 'date' => 2), 
        'WEDNESDAY' => array('id=' => 'WEDNESDAY', 'name' => '周三', 'date' => 3),
        'THURSDAY' => array('id=' => 'THURSDAY', 'name' => '周四', 'date' => 4), 
        'FRIDAY' => array('id=' => 'FRIDAY', 'name' => '周五', 'date' => 5), 
        'SATURDAY' => array('id=' => 'SATURDAY', 'name' => '周六', 'date' => 6), 
        'SUNDAY' => array('id=' => 'SUNDAY', 'name' => '周日', 'date' => 0), 
    );

    public static $timeLimitData = array(
        'type' => array(true, null),
        'begin_hour' => array(true, null), // 当前type类型下的起始时间（小时） ，如当前结构体内填写了MONDAY， 此处填写了10，则此处表示周一 10:00可用
        'begin_minute' => array(true, null), // 当前type类型下的起始时间（分钟） ，如当前结构体内填写了MONDAY， begin_hour填写10，此处填写了59， 则此处表示周一 10:59可用
        'end_hour' => array(true, null), // 当前type类型下的结束时间（小时） ，如当前结构体内填写了MONDAY， 此处填写了20，则此处表示周一 10:00-20:00可用
        'end_minute' => array(true, null), // 当前type类型下的结束时间（分钟） ，如当前结构体内填写了MONDAY， begin_hour填写10，此处填写了59， 则此处表示周一 10:59-00:59可用
    );
    
    public static $advancedInfo    = array(
        'use_condition' => array(true, null),
        'abstract' => array(true, null),
        'time_limit' => array(true, null),
        'text_image_list' => array(true, null),
        'business_service' => array(true, null),
        'consume_share_card_list' => array(true, null),
        'share_friends' => array(true, null),
    );

    public static $codeTypeMap  = array(
        "CODE_TYPE_TEXT" => '文本',              
        "CODE_TYPE_BARCODE" => '一维码',
        "CODE_TYPE_QRCODE" => '二维码',
        "CODE_TYPE_ONLY_QRCODE" => '仅显示二维码',
        "CODE_TYPE_ONLY_BARCODE" => '仅显示一维码',
        "CODE_TYPE_NONE" => '不显示任何码型'
    );

    public static $dateInfoTypeMap = array(
        'DATE_TYPE_PERMANENT' => array('DATE_TYPE_PERMANENT' => '永久有效类型'),
        'DATE_TYPE_FIX_TIME_RANGE' => array('DATE_TYPE_FIX_TIME_RANGE' => '固定时长有效类型'),
        'DATE_TYPE_FIX_TERM' => array('DATE_TYPE_FIX_TERM' => '自领取后多少天内有效(领取后当天有效填写0)'),
    );

    public static $nameTypeMap  = array(
        'FIELD_NAME_TYPE_LEVEL' => array('id' => 'FIELD_NAME_TYPE_LEVEL', 'name' => '等级'),
        'FIELD_NAME_TYPE_COUPON' => array('id' => 'FIELD_NAME_TYPE_COUPON', 'name' => '优惠券 '),               
        'FIELD_NAME_TYPE_STAMP' => array('id' => 'FIELD_NAME_TYPE_STAMP', 'name' => '印花'),
        'FIELD_NAME_TYPE_DISCOUNT' => array('id' => 'FIELD_NAME_TYPE_DISCOUNT', 'name' => '折扣'),
        'FIELD_NAME_TYPE_ACHIEVEMEN' => array('id' => 'FIELD_NAME_TYPE_ACHIEVEMEN', 'name' => '成就'),
        'FIELD_NAME_TYPE_MILEAGE' => array('id' => 'FIELD_NAME_TYPE_MILEAGE', 'name' => '里程'),
        'FIELD_NAME_TYPE_SET_POINTS' => array('id' => 'FIELD_NAME_TYPE_SET_POINTS', 'name' => '集点'),
        'FIELD_NAME_TYPE_TIMS' => array('id' => 'FIELD_NAME_TYPE_TIMS', 'name' => '次数')
    );

    public static $customFieldData  = array(
        'name_type' =>   array(true, null), //   会员信息类目名称。
        'name' =>   array(true, null), // 会员信息类目自定义名称，当开发者变更这类类目信息的value值时不会触发系统模板消息通知用户    
        'url' =>   array(true, null) // 点击类目跳转外链url   
    );

    public static $customCellData  = array(
        'tips' =>   array(false, "提示语"), //   名称。6个汉字内。 
        'name' =>   array(false, "入口名称"), // 入口名称    
        'url' =>   array(false, "跳转链接") // 点击类目跳转外链url   
    );

    public static $bonusRules = array(
        'cost_money_unit' => array(true, null), //消费金额，以分为单位         
        'increase_bonus' => array(true, null), // 根据以上消费金额对应增加的积分           
        'max_increase_bonus' => array(true, null), //单次获取的积分上限            
        'init_increase_bonus' => array(true, null), //用户激活后获得的初始积分      
        'cost_bonus_unit' => array(true, null), //  每使用5积分。            
        'reduce_money' => array(true, null), //抵扣xx元，（这里以分为单位）            
        'least_money_to_use_bonus' => array(true, null), // 抵扣条件，满xx元（这里以分为单位）可用          
        'max_reduce_bonus' =>   array(true, null), //抵扣条件，单笔最多使用xx积分   
    );

    public static $memberCard       = array(
        'background_pic_url' =>   array(true, null), // 会员卡自定义卡面背景图 1000像素*600像素以下     
        'base_info' => array(true, array()), //自动补充
        'prerogative' =>   array(false, null), //特权说明。            
        'auto_activate' =>   array(true, null), //   详情见自动激活。    
        'wx_activate' =>   array(true, null), //  该选项与activate_url互斥。      
        'advanced_info' => array(true, array()), //高级信息      
        'supply_bonus' =>   array(false, "显示积分"), // 是否支持积分，仅支持从false变为true，默认为false
        'bonus_url' =>   array(true, null), //   积分信息类目跳转的url。
        'supply_balance' => array(false, "支持折扣"), //  该字段须开通储值功能后方可使用，详情见：获取特殊权限
        'balance_url' => array(true, null), // 余额信息类目跳转的url
        'custom_field1' =>   array(true, null), //自定义会员信息类目，会员卡激活后显示。            
        'custom_field2' =>  array(true, null), //定义会员信息类目，会员卡激活后显示。            
        'custom_field3' =>   array(true, null), // 自定义会员信息类目，会员卡激活后显示。            
        'bonus_cleared' =>   array(true, null), // 积分清零规则。            
        'bonus_rules' =>   array(true, ''), //   积分规则。            
        'balance_rules' =>   array(true, null), // 储值说明。            
        'activate_url' =>   array(true, null), //  激活链接
        'activate_app_brand_user_name' => array(true, null), //激活会原卡url对应的小程序user_name，仅可跳转该公众号绑定的小程序
        'activate_app_brand_pass' => array(true, null), //激活会原卡url对应的小程序path
        'custom_cell1' =>   array(true, null), //  自定义会员信息类目，会员卡激活后显示     
        'custom_cell2' =>   array(true, null), //  自定义会员信息类目，会员卡激活后显示         
        'bonus_rule' =>   array(true, null), //  积分规则结构体，用于微信买单功能                    
        'discount' =>   array(false, '折扣'), //折扣，该会员卡享受的折扣优惠 
    );

    public static $juanCard       = array(  
        'base_info' => array(true, array()), //自动补充              
        'advanced_info' => array(true, array()), //高级信息           
        'discount' =>   array(true, null), //折扣，该会员卡享受的折扣优惠
        'reduce_cost' => array(true, null),
        'least_cost' => array(true, null),
        'default_detail' => array(true, null)
    );

    /**
     * 修改会员卡
     */
    public function updatemembercard($cardId, $data, $cardType = 'member_card')
    {
        $params   = array(
            'card_id' => $cardId,
        );
        $params[strtolower($cardType)]    = $data;
        $json     = $this->update($params);
        if(0 < $json['errcode']) {
            throw new HRequestException('拉取会员信息失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    private static $_getTotalCodeUrl = 'http://api.weixin.qq.com/card/code/getdepositcount?access_token={access_token}';

    /**
     * 得到总的导入Code数量 
     * 
     * @return 总数量
     */
    public function getTotalCode($cardId)
    {
        $params   = array(
            'card_id' => $cardId,
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_getTotalCodeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('获取总导入卡号失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json['count'];
    }

    private static $_importCodeUrl = 'http://api.weixin.qq.com/card/code/deposit?access_token={access_token}';

    /**
     * 导入自定义卡号
     * 
     * @return [type] [description]
     */
    public function importCode($cardId, $data)
    {
        if(100 < count($data)) {
            throw new HVerifyException('一次最多只能导入100个！');
        }
        $params   = array(
            'card_id' => $cardId,
            'code' => $data
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_importCodeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('导入卡号失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    private static $_changeCodeUrl = 'https://api.weixin.qq.com/card/code/update?access_token={access_token}';

    /**
     * 修改卡号
     * 
     * @param  [type] $cardId  [当前卡号]
     * @param  [type] $code    [原卡号]
     * @param  [type] $newCode [新卡号]
     */
    public function changeCode($cardId, $code, $newCode)
    {
        $params   = array(
            'card_id' => $cardId,
            'code' => $code,
            'new_code' => $newCode
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_changeCodeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('修改卡号失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }
    }

    /**
     * 激活会员卡
     * @var string
     */
    private static $_activateUrl = 'https://api.weixin.qq.com/card/membercard/activate?access_token={access_token}';

    /**
     * 接口方式 激活会员卡
     */
    public function activate($code, $initBonus = 0, $cardId = null)
    {
        $params   = array(
            'membership_number' => $code,
            'code' => $code,
            'init_bonus' => $initBonus,
            'init_bonus_record' => '激活会员'
        );
        if($cardId) {
            $params['card_id']  = $cardId;
        }
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_activateUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('激活会员卡失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    /**
     * 设置卡券失效
     * @var string
     */
    private static $_unavailableUrl = 'https://api.weixin.qq.com/card/code/unavailable?access_token={access_token}';

    /**
     * 接口方式 设置卡券失效
     */
    public function unavailable($code)
    {
        $params   = array(
            'code' => $code,
            'reason' => '卡券失效'
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_unavailableUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('设置卡券失效失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    private static $_selfConsumeCellUrl     = 'https://api.weixin.qq.com/card/selfconsumecell/set?access_token={access_token}';

    /**
     * 打开自助核销卡券功能 
     * 
     * @param  [type] $cardId [卡券ID]
     * @return [type]         [description]
     */
    public function openSelfConsumeCell($cardId, $isOpen)
    {
        $params   = array(
            'card_id' => $cardId,
            'is_open' => $isOpen 
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_selfConsumeCellUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('设置失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return true;
    }

    private static $_consumeUrl = 'https://api.weixin.qq.com/card/code/consume?access_token={access_token}';

    /**
     * 核销卡券
     * 
     * @param  [type] $code   [description]
     * @param  [type] $cardId [description]
     * @return [type]         [description]
     */
    public function consumeCode($code, $cardId)
    {
        $params   = array(
            'card_id' => $cardId,
            'code' => $code 
        );
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_consumeUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('核销失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

}
