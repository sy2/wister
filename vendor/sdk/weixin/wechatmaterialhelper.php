<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.wechat');

/**
 * 微信素材管理帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatMaterialHelper extends Wechat
{

    /**
     * @var private static $_countUrl 总素材信息URL
     */
    private static $_countUrl = 'https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token={access_token}';

    /**
     * 得到素材总数
     * 
     * {@see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1444738733&token=&lang=zh_CN}
     *
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  {String} JSON字符串
     */
    public function getCount()
    {
        $json   = HRequest::getContents(str_replace('{access_token}', $this->_accessToken, self::$_countUrl));
        $json   = json_decode($json, true);
        
        if(0 > $json['errcode']) {
            throw new HRequestException('获取失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * @var private static $_listUrl  获取列表URL
     */
    private static $_listUrl  = 'https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token={access_token}';

    /**
     * 获取新闻列表
     *
     * {@see https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function getNewsList($total, $offset = 0)
    {
        $params = array(
            'type' => 'news',
            'offset' => $offset,
            'count' => $total,
        );

        return $this->_getList($params);
    }

    /**
     * 获取声音列表
     *
     * {@see https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function getVoiceList($total, $offset = 0)
    {
        $params = array(
            'type' => 'voice',
            'offset' => $offset,
            'count' => $total,
        );
        
        return $this->_getList($params);
    }

    /**
     * 获取视频列表
     *
     * {@see https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function getVideoList($total, $offset = 0)
    {
        $params = array(
            'type' => 'video',
            'offset' => $offset,
            'count' => $total,
        );

        return $this->_getList($params);
    }

    /**
     * 获取图片列表
     *
     * {@see https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function getImageList($total, $offset = 0)
    {
        $params = array(
            'type' => 'image',
            'offset' => $offset,
            'count' => $total,
        );
        
        return $this->_getList($params);
    }

    /**
     * 得到列表（内部使用）
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $params 变量
     * @return 返回数据
     */
    private function _getList($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_listUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('获取列表失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 上传图文素材里面的图片地址
     * @var string
     */
    private static $_uploadImageUrl  = 'https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token={access_token}';

    /**
     * 上传图文素材里面的 图片
     * @param $params
     */
    public function uploadImage($params)
    {
        $json   = HRequest::postFile(
            str_replace('{access_token}', $this->_accessToken, self::$_uploadImageUrl),
            $params,
            true
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('上传图文素材图片失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 上传封面图片地址
     * @var string
     */
    private static $_uploadCoverImageUrl = 'https://api.weixin.qq.com/cgi-bin/material/add_material?access_token={access_token}';

    /**
     * 上传封面图
     * @param $params
     * @return array|mixed|object|string
     */
    public function uploadCoverImage($params)
    {
        $json   = HRequest::postFile(
            str_replace('{access_token}', $this->_accessToken, self::$_uploadCoverImageUrl),
            $params,
            true
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('上传永久素材失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 上传图文消息地址
     * @var string
     */
    private static $_addNewsUrl = 'https://api.weixin.qq.com/cgi-bin/material/add_news?access_token={access_token}';

    /**
     * 上传图文消息
     * @param $params
     */
    public function addNews($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_addNewsUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('上传图文消息失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 修改图文消息地址
     * @var string
     */
    private static $_updateNewsUrl = 'https://api.weixin.qq.com/cgi-bin/material/update_news?access_token={access_token}';

    /**
     * 修改图文消息
     * @param $params
     */
    public function updateNews($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_updateNewsUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('修改图文消息失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 根据openid群发
     * @var string
     */
    private static $_sendByOpenidUrl = 'https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={access_token}';

    /**
     * 根据openid群发
     * @param $params
     */
    public function sendByOpenid($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_sendByOpenidUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('群发失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 删除永久素材地址
     * @var string
     */
    private static $_delMaterialUrl = 'https://api.weixin.qq.com/cgi-bin/material/del_material?access_token={access_token}';

    /**
     * 删除永久素材
     * @param $params
     * @throws HRequestException
     */
    public function delMaterial($params)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_delMaterialUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('删除素材失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * @var private static $_downloadFileApiUrl    下载文件接口
     */
    private static $_downloadFileApiUrl    = 'http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={access_token}&media_id={id}';

    /**
     * 下载得到素材URL
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $mediaId 素材编号
	 * @return attachment 
     */
    public function downloadMedaiId($mediaId)
    {
        $json   = HRequest::getContents(
            strtr(self::$_downloadFileApiUrl, 
            array('{access_token}' => $this->_accessToken, '{id}' => $mediaId))
        );
    }

    /**
     * @var 	private static $_getMedaiUrl    得到永久素材详情URL
     */
	private static $_getMediaUrl    = 'https://api.weixin.qq.com/cgi-bin/material/get_material?access_token={access_token}';

    /**
     * 得到永久素材
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $mediaId 素材编号
     * @return Array
     */
    public function getMedia($mediaId)
    {
		$params = array('media_id' => $mediaId);
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_getMediaUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);
        if(0 < $json['errcode']) {
            throw new HRequestException('获取素材失败！错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

}
