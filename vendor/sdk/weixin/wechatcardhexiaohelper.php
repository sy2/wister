<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('vendor.sdk.weixin.WechatCardHelper');

/**
 * 微信卡券核销帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class WechatCardHeXiaoHelper extends WechatCardHelper
{

    /**
     * [$_heXiaoUrl 核销URL]
     * @var string
     */
    private static $_heXiaoUrl     = 'https://api.weixin.qq.com/card/code/consume?access_token={access_token}';

    /**
     * 自定义方式核销卡券
     * 
     * @param $code 编号
     * @param [int] $cardId [会员卡编号]
     */
    public function byNormal($code)
    {
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_heXiaoUrl),
            self::json_encode(array('code' => $code))
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('核销失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 自定义方式核销卡券
     * 
     * @param $code 编号
     * @param [int] $cardId [会员卡编号]
     */
    public function byCustom($code, $cardId)
    {
        $params = array('code' => $code, 'card_id' => $cardId);
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_heXiaoUrl),
            self::json_encode($params)
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('核销失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json;
    }

    /**
     * 解码接口URL
     * @var string
     */
    private static $_decodeCodeUrl  = 'https://api.weixin.qq.com/card/code/decrypt?access_token={access_token}';

    /**
     * 解码Code
     * 
     * @param  [string] $code [编码]
     * @return [Array]       [解码结果]
     */
    public function decodeCode($code)
    {
        $code   = urldecode($code);
        $json   = HRequest::post(
            str_replace('{access_token}', $this->_accessToken, self::$_decodeCodeUrl),
            self::json_encode(array('encrypt_code' => $code))
        );
        $json   = json_decode($json, true);

        if(0 < $json['errcode']) {
            throw new HRequestException('解码失败，错误信息：' . $json['errcode'] . ':' . $json['errmsg']);
        }

        return $json['code'];
    }

}
