/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, container, callback) {
    if('undefined' === typeof(container)) {
      throw 'unveil container is not set!';
    }
    var $w = $(container),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = "data-original", //retina? ;//"data-src-retina" : "data-original",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-original");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
        alert(isView);
          
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.hasClass("hide")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();
        var isView  = eb >= wt - th && et <= wb + th;

        return isView;
      });

        try {
          loaded = inview.trigger("unveil");
          images = images.not(loaded);
        } catch(e) {
          alert(e);
        }
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);
