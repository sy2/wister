
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
var myGeo,
    map     = new BMap.Map("allmap");            // 创建Map实例

/**
 * 百度地图地址选择器
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package None
 * @since 1.0.0
 */
var HJZBMapSelector     = {
    opt: {
        'dom': '#area',
        'postion': '#area',
        'drag': '#drag',
        'full': null
    },
    init: function(opt) {
        this.initOpt(opt);
        this.bindMapBox();
        this.bindBtnShowMap();
        this.bindBtnHideMap();
        this.bindBtnSaveLoc();
        this.initAreaMap();
        this.bindDomChange();
    },
    initOpt: function(opt) {
        for(var ele in opt) {
            this.opt[ele]   = opt[ele];
        }
    },
    bindDomChange: function() {
        var self        = this;
        var localSearch = new BMap.LocalSearch(map);
        localSearch.enableAutoViewport(); //允许自动调节窗体大小
        $(this.opt.dom).bind('change', function() {
            var keyword = $(this).val();
        　　localSearch.setSearchCompleteCallback(function (searchResult) {
        　　　　var poi = searchResult.getPoi(0);
                self.setAddress(poi.point.lng, poi.point.lat);
        　　　　map.centerAndZoom(poi.point, 13);
        　　});
        　　localSearch.search(keyword);
        });
    },
    bindBtnShowMap: function() {
        var self    = this;
        $(self.opt.dom).click(function() {
            $(self.opt.drag).show();
            if($(self.opt.position).val()) {
                var pos     = $(self.opt.position).val().split(',');
                map.centerAndZoom($(self.opt.dom).val(), 15);
                self.setMarker(pos[0], pos[1]);
            } else {
                var myCity = new BMap.LocalCity();
                myCity.get(function(result) {
                    var cityName = result.name;
                    map.centerAndZoom(cityName, 13);   // 初始化地图,设置城市和地图级别。
                });
            }
            myGeo   = new BMap.Geocoder();  
        });
    },
    bindMapBox: function() {
        var self    = this;
        // 百度地图API功能
        map.enableScrollWheelZoom(true);
        map.addControl(new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));    // 右下比例尺
        map.setDefaultCursor("Crosshair");//鼠标样式
        map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT}));  //右上角，仅包含平移和缩放按钮
        var cityList = new BMapLib.CityList({
            container: 'container',
            map: map
        });
        map.addEventListener("click", function(e) {
            map.clearOverlays();
            var lng     = e.point.lng + 0.00005;
            var lat     = e.point.lat - 0.00005;
            self.setAddress(lng, lat);
            self.setMarker(lng, lat);
        });
    },
    setAddress: function(lng, lat) {
        var self    = this;
        myGeo.getLocation(new BMap.Point(lng, lat), function(rs){    
            if (!rs){
                return HHJsLib.warn('地址信息获取失败，请重新选择！');
            }
            var addComp = rs.addressComponents;
            $('#province').val(addComp.province);
            $('#city').val(addComp.city);
            $('#district').val(addComp.district);
            $('#street').val(addComp.street);
            $('#street_number').val(addComp.streetNumber);
            if(null != self.opt.full) {
                $(self.opt.full).val(
                    addComp.province
                    + addComp.city
                    + addComp.district
                    + addComp.street
                    + addComp.streetNumber
                );
            }
        }); 
        //获取经纬度
        $(this.opt.position).val(lng + "," + lat);
    },
    setMarker: function(lng, lat) {
        var marker  = new BMap.Marker(new BMap.Point(lng, lat), {icon: icon});  // 创建标注
        var icon    = new BMap.Icon(siteUri + "images/marker.gif", new BMap.Size(34, 36));
        var self    = this;
        marker.enableDragging(true);    // 设置标注可拖拽
        marker.addEventListener("dragging", function(ee) {
            $(self.opt.position).val(ee.point.lng + "," + ee.point.lat);
            self.setAddress(ee.point.lng, ee.point.lat);
        });
        map.addOverlay(marker);
    },
    bindBtnHideMap: function () {
        var self    = this;
        $('#btn-hide-map').hide();
        $('#btn-hide-map').click(function() {
            map.clearOverlays();
            $('#drag').hide();
            $('#area').val('');
            $('#province').val('');
            $('#city').val('');
            $('#district').val('');
            $('#street').val('');
            $('#street_number').val('');
            if(null != self.opt.full) {
                $(self.opt.full).val('');
            }
        });
    },
    bindBtnSaveLoc: function () {
        var self    = this;
        $('#btn-save-loc').click(function() {
            $(self.opt.drag).hide();
            self.initAreaMap();
        });
    },
    initAreaMap: function() {
        if(!$(this.opt.position).val()) {
            $(this.opt.dom + "-show-box").addClass('text-center').html('亲，您还没有选择地址信息～');
            return;
        }
        if(1 > $(this.opt.dom + "-show-box").length) {
            return;
        }
        $(this.opt.dom + "-show-box").html('');
        var map1    = new BMap.Map(this.opt.dom.substr(1) + "-show-box");
        var loc     = $(this.opt.position).val().split(',');
        var zoom    = map.getZoom();
        map1.centerAndZoom(new BMap.Point(loc[0], loc[1]), 12 > zoom || !zoom ? 13 : zoom);
        var marker1 = new BMap.Marker(new BMap.Point(loc[0], loc[1]));  // 创建标注
        map1.addOverlay(marker1);              // 将标注添加到地图中
    }
};
