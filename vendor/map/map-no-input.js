
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
var myGeo,
    map     = new BMap.Map("allmap");            // 创建Map实例

/**
 * 百度地图地址选择器
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package None
 * @since 1.0.0
 */
var HJZBMapSelector     = {
    opt: {
        'dom': '#area',
        'postion': '#area',
        'full': null,
        zoom: 15
    },
    init: function(opt) {
        this.initOpt(opt);
        this.bindMapBox();
        this.initAreaMap();
        this.getLocationByBrowser();
        this.bindDomChange();
    },
    bindDomChange: function() {
        var self        = this;
        var localSearch = new BMap.LocalSearch(map);
        localSearch.enableAutoViewport(); //允许自动调节窗体大小
        $(this.opt.dom).bind('blur change', function() {
            var keyword = $(this).val();
        　　localSearch.setSearchCompleteCallback(function (searchResult) {
        　　　　var poi = searchResult.getPoi(0);
                self.setAddress(poi.point.lng, poi.point.lat);
        　　　　map.centerAndZoom(poi.point, 13);
        　　});
        　　localSearch.search(keyword);
        });
    },
    initOpt: function(opt) {
        for(var ele in opt) {
            this.opt[ele]   = opt[ele];
        }
    },
    getLocationByCity: function() {
        var self    = this;
        var myCity  = new BMap.LocalCity();
        myCity.get(function(result) {
            var cityName = result.name;
            if(!$(self.opt.position).val()) {
                self.getLocationByBrowser(cityName);
            }
        });
    },
    bmapStatusMap: {
        'status-0': '检索成功',
        'status-1': '城市列表',
        'status-2': '位置结果未知',
        'status-3': '导航结果未知',
        'status-4': '非法密钥',
        'status-5': '非法请求',
        'status-6': '没有权限',
        'status-7': '服务不可用',
        'status-8': '超时'
    },
    getLocationByBrowser: function(cityName) {
        var self        = this;
        var geolocation = new BMap.Geolocation();
        geolocation.getCurrentPosition(function(r){
            var status  = this.getStatus();
            if(status == BMAP_STATUS_SUCCESS){
                var icon    = new BMap.Icon(siteUri + "images/marker.gif", new BMap.Size(34, 36));
                var mk      = new BMap.Marker(r.point, {icon: icon});
                map.addOverlay(mk);
                map.centerAndZoom(r.point, self.opt.zoom);   // 初始化地图,设置城市和地图级别。
                $(self.opt.position).val(r.point.lng + "," + r.point.lat);
                self.setAddress(r.point.lng, r.point.lat);
            } else {
                alert(self.bmapStatusMap['status' + status]);
                map.centerAndZoom(cityName, self.opt.zoom);   // 初始化地图,设置城市和地图级别。
            }
        },{enableHighAccuracy: true});
    },
    bindMapBox: function() {
        var self    = this;
        myGeo       = new BMap.Geocoder();
        // 百度地图API功能
        map.enableAutoResize();
        map.enableScrollWheelZoom(false);
        map.disableDoubleClickZoom(false);
        //map.addControl(new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));    // 右下比例尺
        map.setDefaultCursor("Crosshair");//鼠标样式
        map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT}));  //右上角，仅包含平移和缩放按钮
        map.addEventListener("click", function(e) {
            map.clearOverlays();
            var lng     = e.point.lng + 0.00005;
            var lat     = e.point.lat - 0.00015;
            var icon    = new BMap.Icon(siteUri + "images/marker.gif", new BMap.Size(34, 36));
            var marker  = new BMap.Marker(new BMap.Point(lng, lat), {icon: icon});  // 创建标注
            map.addOverlay(marker);
            marker.enableDragging(true);    // 设置标注可拖拽
            marker.addEventListener("dragging", function(ee) {
                $(self.opt.position).val(ee.point.lng + "," + ee.point.lat);
                self.setAddress(ee.point.lng, ee.point.lat);
            });
            //获取经纬度
            $(self.opt.position).val(lng + "," + lat);
            self.setAddress(lng, lat);
        });
        if($(this.opt.dom).val()) {
            map.centerAndZoom($(this.opt.dom).val(), 12);
        }
    },
    setAddress: function(lng, lat) {
        var self    = this;
        myGeo.getLocation(new BMap.Point(lng, lat), function(rs){    
            if (!rs){
                return HHJsLib.warn('地址信息获取失败，请重新选择！');
            }
            var addComp = rs.addressComponents;
            $('#province').val(addComp.province);
            $('#city').val(addComp.city);
            $('#district').val(addComp.district);
            $('#street').val(addComp.street);
            $('#street_number').val(addComp.streetNumber);
            if(null != self.opt.full) {
                $(self.opt.full).val(
                    addComp.province
                    + addComp.city
                    + addComp.district
                    + addComp.street
                    + addComp.streetNumber
                );
            }
        }); 
    },
    initAreaMap: function() {
        var position= $(this.opt.position).val();
        if(!position) {
            return;
        }
        var loc     = position.split(',');
        var icon    = new BMap.Icon(siteUri + "images/marker.gif", new BMap.Size(34, 36));
        map.centerAndZoom(new BMap.Point(loc[0], loc[1]), this.opt.zoom);
        var marker  = new BMap.Marker(new BMap.Point(loc[0], loc[1]), {icon: icon});  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
    }
};
