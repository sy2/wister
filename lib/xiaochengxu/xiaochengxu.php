<?php

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */


/**
 * 微信基本类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package WeiXin
 * @since 1.0.0
 */
class Xiaochengxu
{

    /**
     * @var protected $_appid 应用编号
     */
    protected $_appid;

    /**
     * @var protected $_secret 密钥
     */
    protected $_secret;

    /**
     * @var protected $_token 操作口令
     */
    protected $_accessToken;

    /**
     * @var 会话编号
     */
    protected $_sessionKey;

    /**
     * @var protected static $_urlMap  地址映射
     */
    protected static $_urlMap  = array(
        'session_key' => 'https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={code}&grant_type=authorization_code',
        'access_token' => 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={secret}'
    );

    /**
     * 构造函数
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param  $appid APPId
     * @param  $secret 密钥
     */
    public function __construct($appid, $secret)
    {
        $this->_appid   = $appid;
        $this->_secret  = $secret;
        $this->_asscessToken    = null;
    }

    /**
     * 得到入口令
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function sessionKey($code)
    {
        $url        = strtr(
            self::$_urlMap['session_key'],
            array('{appid}' => $this->_appid, '{secret}' => $this->_secret, '{code}' => $code)
        );
        $json       = HRequest::post($url, array());
        $json       = json_decode($json, true);
        if(isset($json['errcode']) && 0 < $json['errcode']) {
            throw new HApiException($json['errcode'] . ':' . $json['errmsg']);
        }
        $this->_sessionKey = $json['session_key'];

        return $json;
    }

    /**
     * 得到入口令
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function requestAccessToken($identifier = 'xcx-access-token')
    {
        $staticCfg  = HClass::quickLoadModel('staticcfg');
        $record     = null;
//        $record     = $staticCfg->getRecordByIdentifier($identifier);
//        if($record) {
//            $json       = json_decode(HString::decodeHtml($record['content']), true);
//            if($json['end_time'] > $_SERVER['REQUEST_TIME']) {
//                $this->_accessToken     = $json['access_token'];
//                return $this;
//            }
//        }
        $json       = HRequest::getContents(strtr(
            self::$_urlMap['access_token'], 
            array('{appid}' => $this->_appid, '{secret}' => $this->_secret)
        ));
        $json       = json_decode($json, true);
        if(isset($json['errcode']) && 0 < $json['errcode']) {
            throw new HRequestException($json['errmsg']);
        }
        $json['end_time']   = time() + $json['expires_in'] / 2;
        $data       = array(
            'name' => '小程序口令ACCESS-TOKEN',
            'identifier' => $identifier,
            'content' => json_encode($json),
            'author' => 0
        );
        if($record) {
            $staticCfg->editByWhere($data, '`id` = ' . $record['id']);
        } else {
            $staticCfg->add($data);
        }
        $this->_accessToken    = $json['access_token'];

        return $this;
    }

    /**
     * 得到已经获得的
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @return {string}
     */
    public function getAccessToken()
    {
        return $this->_accessToken;
    }

    /**
     * 微信api不支持中文转义的json结构
     *
     * @param array $arr
     */
    public static function json_encode($arr) 
    {
        if (count($arr) == 0) return "[]";
        $parts = array ();
        $is_list = false;
        //Find out if the given array is a numerical array
        $keys = array_keys ( $arr );
        $max_length = count ( $arr ) - 1;
        if (($keys [0] === 0) && ($keys [$max_length] === $max_length )) { //See if the first key is 0 and last key is length - 1
            $is_list = true;
            for($i = 0; $i < count ( $keys ); $i ++) { //See if each key correspondes to its position
                if ($i != $keys [$i]) { //A key fails at position check.
                    $is_list = false; //It is an associative array.
                    break;
                }
            }
        }
        foreach ( $arr as $key => $value ) {
            if (is_array ( $value )) { //Custom handling for arrays
                if ($is_list)
                    $parts [] = self::json_encode ( $value ); /* :RECURSION: */
                else
                    $parts [] = '"' . $key . '":' . self::json_encode ( $value ); /* :RECURSION: */
            } else {
                $str = '';
                if (! $is_list)
                    $str = '"' . $key . '":';
                //Custom handling for multiple data types
                if (!is_string ( $value ) && is_numeric ( $value ) && $value<2000000000)
                    $str .= $value; //Numbers
                elseif ($value === false)
                    $str .= 'false'; //The booleans
                elseif ($value === true)
                    $str .= 'true';
                else
                    $str .= '"' . addslashes ( $value ) . '"'; //All other things
                // :TODO: Is there any more datatype we should be in the lookout for? (Object?)
                $parts [] = $str;
            }
        }
        $json = implode ( ',', $parts );
        if ($is_list)
            return '[' . $json . ']'; //Return numerical JSON
        return '{' . $json . '}'; //Return associative JSON
    }

    /**
     *  作用：产生随机字符串，不长于32位
     */
    public function createNoncestr( $length = 32 ) 
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";  
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {  
            $str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);  
        }  
        return $str;
    }

    /**
     *  作用：格式化参数，签名过程需要使用
     */
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v)
        {
            if($urlencode)
            {
               $v = urlencode($v);
            }
            //$buff .= strtolower($k) . "=" . $v . "&";
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar;
        if (strlen($buff) > 0) 
        {
            $reqPar = substr($buff, 0, strlen($buff)-1);
        }
        return $reqPar;
    }
    
    /**
     *  作用：生成签名
     */
    public function getSign($Obj)
    {
        foreach ($Obj as $k => $v)
        {
            $Parameters[$k] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //echo '【string1】'.$String.'</br>';
        //签名步骤三：sha1加密
        $result_ = sha1($String);
        //echo "【result】 ".$result_."</br>";
        return $result_;
    }
    
    /**
     *  作用：array转xml
     */
    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key=>$val)
        {
             if (is_numeric($val))
             {
                $xml.="<".$key.">".$val."</".$key.">"; 

             }
             else
                $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";  
        }
        $xml.="</xml>";
        return $xml; 
    }
    
    /**
     *  作用：将xml转为array
     */
    public function xmlToArray($xml)
    {       
        //将XML转为array        
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);      
        return $array_data;
    }

    /**
     *  作用：以post方式提交xml到对应的接口url
     */
    public function postXmlCurl($xml,$url,$second=30)
    {       
        //初始化curl        
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        curl_close($ch);
        //返回结果
        if($data)
        {
            //curl_close($ch);
            return $data;
        }
        else 
        { 
            $error = curl_errno($ch);
            echo "curl出错，错误码:$error"."<br>"; 
            echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }

    /**
     *  作用：使用证书，以post方式提交xml到对应的接口url
     */
    function postXmlSSLCurl($xml,$url,$second=30)
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        //设置header
        curl_setopt($ch,CURLOPT_HEADER,FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        //设置证书
        //使用证书：cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT, WxPayConf_pub::SSLCERT_PATH);
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY, WxPayConf_pub::SSLKEY_PATH);
        curl_setopt($ch, CURLOPT_CAINFO, WxPayConf_pub::SSLCA_PATH);
        //post提交方式
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xml);
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        }
        else { 
            $error = curl_errno($ch);
            echo "curl出错，错误码:$error"."<br>"; 
            echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData( $encryptedData, $iv)
    {
        $wxDecodeData   = new WXBizDataCrypt($this->_appid, $this->_sessionKey);
        $rs             = $wxDecodeData->decryptData($encryptedData, $iv);

        return $rs;
    }

    /**
     * 创建小程序二维码
     *
     * @param $path 地址
     * @param $query 编号
     * @param int $width 宽度
     * @throws HRequestException 请求异常
     */
    public function createwxaqrcode($path, $query, $width=430)
    {
        $postData = '{"path":"' . $path . '?' . $query .  '","width":' . $width . '}';
        $this->requestAccessToken();
        // $url      = 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=' .
        // $this->getAccessToken();
        //$url      = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $this->getAccessToken();
	$url 	= 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=' . $this->getAccessToken();
        $result   = $this->postXmlCurl($postData, $url);
        header("Content-Type:image/png");
        echo $result;
    }

    /**
     * 创建无限小程序二维码
     *
     * @param $path 地址
     * @param $scene 编号
     * @param int $width 宽度
     * @throws HRequestException 请求异常
     */
    public function createUnlimitWXQRcode($path, $scene, $width=430)
    {
        $postData = '{"path":"' . $path . '", "width":' . $width . ', "scene": "' . $scene . '"}';
        $this->requestAccessToken();
        $url      = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $this->getAccessToken();
        $result   = $this->postXmlCurl($postData, $url);
        header("Content-Type:image/png");
        echo $result;
    }

}

/**
 * error code 说明.
 * <ul>

 *    <li>-41001: encodingAesKey 非法</li>
 *    <li>-41003: aes 解密失败</li>
 *    <li>-41004: 解密后得到的buffer非法</li>
 *    <li>-41005: base64加密失败</li>
 *    <li>-41016: base64解密失败</li>
 * </ul>
 */
class ErrorCode
{
    public static $OK = 0;
    public static $IllegalAesKey = -41001;
    public static $IllegalIv = -41002;
    public static $IllegalBuffer = -41003;
    public static $DecodeBase64Error = -41004;
}


class WXBizDataCrypt
{
    private $appid;
    private $sessionKey;

    /**
     * 构造函数
     * @param $sessionKey string 用户在小程序登录后获取的会话密钥
     * @param $appid string 小程序的appid
     */
    public function __construct( $appid, $sessionKey)
    {
        $this->sessionKey = $sessionKey;
        $this->appid = $appid;
    }

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData( $encryptedData, $iv)
    {
        if (strlen($this->sessionKey) != 24) {
            return ErrorCode::$IllegalAesKey;
        }
        $aesKey     = base64_decode($this->sessionKey);
        if (strlen($iv) != 24) {
            return ErrorCode::$IllegalIv;
        }
        $aesIV      = base64_decode($iv);
        $aesCipher  = base64_decode($encryptedData);
        $result     = openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
        $dataObj    = json_decode( $result );
        if( $dataObj  == NULL ) {
            return ErrorCode::$IllegalBuffer;
        }
        if( $dataObj->watermark->appid != $this->appid ) {
            return ErrorCode::$IllegalBuffer;
        }

        return $dataObj;
    }

}

