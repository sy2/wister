<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('lib.xiaochengxu.xiaochengxu');

/**
 * 微信模板消息帮助类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package weixin
 * @since 1.0.0
 */
class QrHelper extends Xiaochengxu
{

    private $_autoColor;

    private $_lineColor;

    private $_isHyaline;

    private $_sence;

    public function __construct($appid, $secret)
    {
        parent::__construct($appid, $secret);
        $this->_autoColor   = false;
        $this->_isHyaline   = false;
        $this->_sence       = '';
        $this->_lineColor   = array('r' => 0, 'g' => 0, 'b' => 0);
    }

    private static $_noTimeLimitUrl     = 'https://api.weixin.qq.com/wxa/getwxacode?access_token={ACCESS_TOKEN}';

    public function setAutoColor($isAuto)
    {
        $this->_autoColor   = $isAuto;
    }

    public function setLineColor($r, $g, $b)
    {
        $this->_lineColor   = array('r' => $r, 'g' => $g, 'b' => $b);
    }

    public function setIsHyaline($isHyaline)
    {
        $this->_isHyaline   = $isHyaline;
    }

    public function setSence($sence)
    {
        $this->_sence   = $sence;
    }

    /**
     * 得到永久小程序码
     *
     * @param $filePath 保存路径
     * @param $path 参数路径
     * @param int $width 宽度，默认430
     */
    public function getNoTimeLimit($filePath, $path, $width = 430)
    {
        $data   = array(
            'path' => $path,
            'width' => $width,
            'auto_color' => $this->_autoColor,
            'line_color' => $this->_lineColor,
            'is_hyaline' => $this->_isHyaline
        );
        $json   = HRequest::postJson(
            str_replace('{ACCESS_TOKEN}', $this->_accessToken, self::$_noTimeLimitUrl),
            self::json_encode($data)
        );
        $rs     = json_decode($json, true);
        if($rs && 0 < $rs['errcode']) {
            throw new HRequestException($rs['errmsg']);
        }
        file_put_contents($filePath, $json);

        return true;
    }

    /**
     * @var string 生成临时小程序码
     */
    private static $_tempUrl    = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={ACCESS_TOKEN}';

    /**
     * 得到临时小程序码
     *
     * @param $filePath 保存路径
     * @param $page 页面路径
     * @param int $width 宽度默认,430
     */
    public function getTemp($filePath, $page, $width = 430)
    {
        $data   = array(
            'page' => $page,
            'width' => $width,
            'auto_color' => $this->_autoColor,
            'line_color' => $this->_lineColor,
            'is_hyaline' => $this->_isHyaline,
            'sence' => $this->_sence
        );
        $json   = HRequest::postJson(
            str_replace('{ACCESS_TOKEN}', $this->_accessToken, self::$_tempUrl),
            self::json_encode($data)
        );
        ;
        $rs     = json_decode($json, true);
        if($rs && 0 < $rs['errcode']) {
            throw new HRequestException($rs['errmsg']);
        }
        file_put_contents($filePath, $json);
        return true;
    }

    private static $_wxaqrLimitUrl     = 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token={ACCESS_TOKEN}';

    public function getwxQr($filePath, $page, $width = 430, $type)
    {
        $data   = array(
            'path' => $page,
            'width' => $width,
            'auto_color' => $this->_autoColor,
            'line_color' => $this->_lineColor,
            'is_hyaline' => $this->_isHyaline
        );
        $json   = HRequest::postJson(
            str_replace('{ACCESS_TOKEN}', $this->_accessToken, self::$_wxaqrLimitUrl),
            self::json_encode($data)
        );
        $rs     = json_decode($json, true);
        if($rs && 0 < $rs['errcode']) {
            throw new HRequestException($rs['errmsg']);
        }
        if( $type ) {
            header("Content-Type:image/png");
            echo $json;exit;
        }

        file_put_contents($filePath, $json);

        return true;
    }

}
