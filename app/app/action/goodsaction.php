<?php

/**
 * @version         $Id$
 * @create          2012-4-8 8:48:15 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight       Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 * HongJuZi Framework
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.app.action.appaction, config.popo.goodspopo, model.goodsmodel');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author          xjiujiu <xjiujiu@foxmail.com>
 * @package         app.admin.action
 * @since           1.0.0
 */
class GoodsAction extends AppAction
{

    /**
     * 构造函数
     *
     * 初始化类里的变量
     *
     * @access public
     */
    private $_goodsCategory;
    private $_information;
    private $_article;
    public function __construct()
    {
        $this->_popo  = new GoodsPopo();
        $this->_model = new GoodsModel($this->_popo);
        $this->_goodsCategory = HClass::quickLoadModel('goodscategory');
        $this->_information   = HClass::quickLoadModel('information');
        $this->_article       = HClass::quickLoadModel('article');
        $this->_model->setMustWhere('status', '`status` = 2');
    }

    /**
     * 获取分类
     */
    public function cate()
    {
        $cglist = $this->_goodsCategory->getAllRowsByFields(
            '`id`,`name`',
            '`status` = 2 AND `is_recommend` = 2');
        $information = $this->_information->getRecordByFields('`image_path`', '1 = 1');
        $list        = $this->_getGoodsListAndTitle($cglist);
        $artList     = $this->_article->getAllRowsByFields('`id`,`name`', '`status` = 2');
        HResponse::json(
            array(
                'rs'        => true,
                'cate_list' => $cglist,
                'logo'      => HResponse::touri($information['image_path']),
                'list'      => $list,
                'art_list'  => $artList,
            ));
    }

    /**
     * 分类商品
     */
    public function index() {
        $parentId = intval(HRequest::getParameter('id'));
        list($list, $name) = $this->_getGoodsListAndTitle($parentId);
        $rtn    = array(
            'rs'    => true,
            'list'  => $list,
            'title' => $name
        );
        HResponse::json($rtn);
    }

    /**
     * 获取商品信息
     */
    private function _getGoodsListAndTitle($catList) {
        $list     = $this->_model->getAllRowsByFields(
            '`id`,`name`,`image_path`,`parent_id`',
            HSqlHelper::whereInByListMap('parent_id', 'id', $catList)
        );

        foreach ($list as &$item) {
            $item['image_path'] = HResponse::touri($item['image_path']);
        }

        $cgList = array();
        foreach ($catList as $val) {
            $temp = array();
            foreach ($list as $gitem) {
                if($val['id'] == $gitem['parent_id']) {
                    $temp[] = $gitem;
                }
            }
            $cgList[] = array('title' => $val['name'], 'slist' => $temp, 'id' => $val['id']);
        }

        return $cgList;

    }


    /**
     * 获取商品详情
     */
    public function detail()
    {
        $id     = HRequest::getParameter('id');
        $record = $this->_model->getRecordById($id);
        $cate   = HClass::quickLoadModel('goodscategory');
        $grec   = $cate->getRecordById($record['parent_id']);
        $user   = HClass::quickLoadModel('user');
        $userRec= $user->getRecordByWhere('`parent_id` = 1');
        $data   = array(
            'content'  => HString::decodeHtml($record['content']),
            'title'    => $grec['name'] . '---' . $record['name'],
            'sq_phone' => $userRec['phone'],
            'sh_phone' => $userRec['weixin'],
        );
        $rtn    = array(
            'rs'     => true,
            'data'   => $data,
        );
        HResponse::json($rtn);
    }
}

?>
