<?php

defined('_HEXEC') or die('Restricted access!');
HClass::import('config.popo.goodscategorypopo, app.admin.action.AdminAction, model.goodscategorymodel');


class GoodscategoryAction extends AdminAction
{

    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new GoodscategoryPopo();
        $this->_model       = new GoodscategoryModel($this->_popo);
    }

    protected function _assignAllParentList()
    {
        return '';
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('is_recommend', 'name', GoodscategoryPopo::$isRecommendMap);
        HResponse::registerFormatMap('status', 'name', GoodscategoryPopo::$statusMap);
    }
}

?>
