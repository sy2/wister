<?php

/**
 * @version			$Id$
 * @create 			2018-06-26 11:06:33 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.jobpopo, app.admin.action.AdminAction, model.jobmodel');

/**
 * 求职的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class JobAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new JobPopo();
        $this->_model       = new JobModel($this->_popo);
    }

    public function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        HResponse::setAttribute('tags_list',Jobpopo::$tagsMap);
        HResponse::setAttribute('status_list',Jobpopo::$statusMap);
    }

    public function _otherJobsBeforeEdit()
    { 
        parent::_otherJobsBeforeEdit(); 
        HRequest::setParameter('id', HRequest::getParameter('id') ?: 0 );
    }

    public function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status','name',JobPopo::$statusMap);
    }

}

?>
