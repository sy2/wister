<?php

/**
 * @version			$Id$
 * @create 			2016-05-23 11:05:04 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.goodspopo, app.admin.action.adminaction, model.goodsmodel');

/**
 * 商品管理的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class GoodsAction extends AdminAction
{
    private $_staticCfg;
    public function __construct()
    {
        parent::__construct();
        $this->_popo        = new GoodsPopo();
        $this->_model       = new GoodsModel($this->_popo);
        $this->_popo->setFieldAttribute('is_new', 'is_show', false);
        $this->_popo->setFieldAttribute('code', 'is_show', false);
        $this->_staticCfg   = HClass::quickLoadModel('staticcfg');
    }

    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        HResponse::setAttribute('status_list', GoodsPopo::$statusMap);
        $this->_assignProductColor();
        $this->_assignProductListByColor();
    }

    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        $this->_formatSuotiData();
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', GoodsPopo::$statusMap);
        HResponse::registerFormatMap('is_show', 'name', GoodsPopo::$isShowMap);
    }

    private function _assignProductColor()
    {
        $record = $this->_staticCfg->getRecordByIdentifier('product-color');
        $list   = explode("\r\n", $record['content']);
        $codeList = array();
        foreach ($list as $key => $val) {
            $codeList[$val] = array('id' => $val, 'name' => $val);
        }
        HResponse::setAttribute('code_list', $codeList);
    }

    private function _formatSuotiData()
    {
        $color  = HRequest::getParameter('color');
        $suotou = HRequest::getParameter('suotou');
        $suoti  = HRequest::getParameter('suoti');
        $price  = HRequest::getParameter('price');
        $code   = HRequest::getParameter('code');
        $images = HRequest::getParameter('sku_image_path');
        $data  = array();
        foreach ($color as $key => $val) {
            $data[] = array(
                'color'  => $val,
                'suotou' => $suotou[$key],
                'suotou' => $suotou[$key],
                'suoti'  => $suoti[$key],
                'price'  => $price[$key],
                'code'   => $code[$key],
                'image_path'  => $images[$key],
            );
        }
        HRequest::setParameter('code', 'no');
        HRequest::setParameter('extend', json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    private function _assignProductListByColor()
    {
        $record = HResponse::getAttribute('record');
        $json   = HString::decodeHtml($record['extend']);
        $record['list'] = json_decode($json, true);
        HResponse::setAttribute('record', $record);
    }
}

?>
