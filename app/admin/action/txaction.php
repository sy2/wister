<?php

/**
 * @version			$Id$
 * @create 			2018-07-11 10:07:41 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.recompopo, app.admin.action.AdminAction, model.recommodel');

/**
 * 提现的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class TxAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_goods;
    private $_user;
    private $_userson;
    private $_userInfo;
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new RecomPopo();
        $this->_model       = new RecomModel($this->_popo);
        $this->_popo->modelZhName = '用户结佣';
        $this->_model->setMustWhere('status','status IN (3,4,5)');
        $this->_goods       = HClass::quickLoadModel('goods');
        $this->_user        = HClass::quickLoadModel('user');
        $this->_userson     = HClass::quickLoadModel('userson');
        $this->_userInfo    = HClass::quickLoadModel('userinfo');
        $this->_listTpl     = 'tx/list';
    }

    protected function _combineWhere()
    {
        $where = parent::_combineWhere();
        if(HRequest::getParameter('status')){
            $where  .= ' AND  `status` = ' . HRequest::getParameter('status');
        }
        return $where;
    }


    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', RecomPopo::$statusMap);
        $this->_assignLoupanList();
        $this->_assignParentList();
    }

    private function _assignLoupanList()
    {
        $list    = $this->_goods->getAllRowsByFields('id,name', HSqlHelper::whereInByListMap('id', 'roomid', HResponse::getAttribute('list')));
        $listMap = HArray::turnItemValueAsKey($list,'id');
        HResponse::registerFormatMap('roomid', 'name', $listMap);
    }

    private function _assignParentList()
    {
        $list    = $this->_user->getAllRowsByFields('id,true_name', HSqlHelper::whereInByListMap('id', 'parent_id', HResponse::getAttribute('list')));
        $userMap = HArray::turnItemValueAsKey($list, 'id');
        HResponse::registerFormatMap('parent_id', 'true_name', $userMap);
    }

    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignUserInfo();
        $this->_assignSuperUserInfo();
    }

    private function _assignUserInfo()
    {
        $record      = HResponse::getAttribute('record');
        $curUserId   = $record['parent_id'];
        $curUserInfo = $this->_userInfo->getRecordByWhere('`parent_id` = ' . $curUserId);
        $attrs       = json_decode($record['attrs'], true);
        $curUserInfo['tuijianMoney'] = $attrs['ext_1'];
        HResponse::setAttribute('curUserInfo', $curUserInfo);
    }

    private function _assignSuperUserInfo()
    {
        $record      = HResponse::getAttribute('record');
        $usInfo   = $this->_userson->getRecordByFields('parent_id', '`son_id` = ' . $record['parent_id']);
        if( !$usInfo ) {
            HResponse::setAttribute('superUserInfo', []);
        } else {
            $superUserId   = $usInfo['parent_id'];
            $superUserInfo = $this->_userInfo->getRecordByWhere('`parent_id` = ' . $superUserId);
            $attrs         = json_decode($record['attrs'], true);
            $superUserInfo['tuijianMoney'] = $attrs['tui_rate'];
            HResponse::setAttribute('superUserInfo', $superUserInfo);
        }

    }

    public function edit()
    {
        $id   = HRequest::getParameter('id');
        $info = $this->_model->getRecordByFields(`status`, '`id` = ' . $id);

        if( !in_array($info['status'], array(3,4) ) ) {
            throw new HVerifyException('不在认购签约状态，不能操作');
        }

        if( $info['status'] == 5) {
            throw new HVerifyException('已经设置过结佣了');
        }

        $rs = $this->_model->editByWhere( array('status'=> 5) , '`id` = ' . $id);
        if( $rs ) {
            throw new HVerifyException('设置成功');
//            HResponse::succeed('设置成功', $this->_getReferenceUrl(1));
        } else {
            HResponse::error('设置失败');
        }
    }

}

?>
