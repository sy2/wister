<?php

/**
 * @version			$Id$
 * @create 			2015-11-22 14:11:54 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.noticepopo, app.admin.action.AdminAction, model.noticemodel');

/**
 * 消息的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class NoticeAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new NoticePopo();
        $this->_model       = new NoticeModel($this->_popo);
        $this->_popo->setFieldAttribute('create_time', 'is_show', true);
        $this->_popo->setFieldAttribute('author', 'is_show', true);
        if('root' != HSession::getAttribute('actor', 'user')) {
            $this->_model->setMustWhere('is_my', '`parent_id` = ' . HSession::getAttribute('id', 'user'));
        }
    }

    /**
     * 搜索方法 
     * 
     * @access public
     */
    public function search()
    {
        $this->_search($this->_combineWhere());

        $this->_render('notice/list');
    }

    protected function _otherJobsBeforeEdit()
    {
        HRequest::setParameter('shop_id', 1);
    }
    /**
     * 搜索方法 
     * 
     * @access public
     */
    public function index()
    {
        $this->_search($this->_combineWhere());

        $this->_render('notice/list');
    }

    /**
     * 列表后驱方法
     * @return [type] [description]
     */
    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', NoticePopo::$statusMap);
        HResponse::registerFormatMap('type', 'name', NoticePopo::$typeMap);
        HResponse::setAttribute('parent_id_list', '');
        $this->_updateNoticeListStatus();
    }

    /**
     * 加载作者Map
     */
    private function _assignAuthorMap()
    {
        $list   = HResponse::getAttribute('list');
        if(!$list) {
            return;
        }
        $user   = HClass::quickLoadModel('user');
        $authorList     = $user->getAllRowsByFields(
            '`id`, `name`',
            HSqlHelper::whereInByListMap('id', 'parent_id', $list)
        );
        HResponse::setAttribute('authorMap', HArray::turnItemValueAsKey($authorList, 'id'));
    }

    /**
     * 更新消息状态
     * @return [type] [description]
     */
    private function _updateNoticeListStatus()
    {
        $list   = HResponse::getAttribute('list');
        if(!$list) {
            return;
        }
        if(1 > $this->_model->editByWhere(array('status' => 2), HSqlHelper::whereInByListMap('id', 'id', $list))) {
            //throw new HRequestException('更新失败');
        }
        $totalNotice = $this->_model->getTotalRecords('`parent_id` < 2 AND `status` = 1');
        HSession::setAttribute('total_notice', $totalNotice, 'user');
    }

    /**
     * 详情后驱方法
     * @return [type] [description]
     */
    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        HResponse::setAttribute('status_list', NoticePopo::$statusMap);
        HResponse::setAttribute('type_list', NoticePopo::$typeMap);
    }

    /**
     * 得到当前模块的所有父类 
     * 
     * 根据当前popo类里的parentTable来判断是否有父类 
     * 
     * @access protected
     */
    protected function _assignAllParentList()
    {
        if('root' != HSession::getAttribute('actor', 'user')) {
            HResponse::setAttribute('parent_id_list', null);
            return;
        }
        if($this->_catIdentifier) {
            HResponse::setAttribute(
                'parent_id_list', 
                $this->_category->getSubCategoryByIdentifier($this->_catIdentifier, true)
            );
            return;
        }
        HResponse::setAttribute(
            'parent_id_list', 
            $this->_getRelationModelList($this->_popo->get('parent'), 'parent_id', '*')
        );
    }

}

?>
