<?php

/**
 * @version			$Id$
 * @create 			2015-10-24 16:10:38 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.wxworkflowpopo, app.admin.action.AdminAction, model.wxworkflowmodel');

/**
 * 微信事务的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class WxworkflowAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new WxworkflowPopo();
        $this->_model       = new WxworkflowModel($this->_popo);
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('type', 'name', WxworkflowPopo::$typeMap);
        HResponse::registerFormatMap('status', 'name', WxworkflowPopo::$statusMap);
    }

    protected function _otherJobsAfterAddView()
    {
        parent::_otherJobsAfterAddView();
        HResponse::setAttribute('type_list', WxworkflowPopo::$typeMap);
        HResponse::setAttribute('status_list', WxworkflowPopo::$statusMap);
    }

    protected function _otherJobsAfterEditView()
    {
        parent::_otherJobsAfterEditView();
        HResponse::setAttribute('type_list', WxworkflowPopo::$typeMap);
        HResponse::setAttribute('status_list', WxworkflowPopo::$statusMap);
    }

    /**
     * 数据验证
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    protected function _verifyDataByPopoCfg()
    {
        HRequest::setParameter('end_time', strtotime(HRequest::getParameter('end_time')));
        parent::_verifyDataByPopoCfg();
    }

}

?>
