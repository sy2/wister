<?php

/**
 * @version			$Id$
 * @create 			2018-07-07 17:07:16 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.recompopo, app.admin.action.AdminAction, model.recommodel');

/**
 * 推荐客户的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class RecomAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_user;
    private $_goods;
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new RecomPopo();
        $this->_model       = new RecomModel($this->_popo);
        $this->_user        = HClass::quickLoadModel('user');
        $this->_goods       = HClass::quickLoadModel('goods');
        $this->_listTpl     = 'recom/list';
    }
    
    public function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        HRequest::setParameter('roomid', HRequest::getParameter('roomid') ?: 0);
    }

    public function _otherJobsAfterEdit()
    {
        parent::_otherJobsAfterEdit();
        if( HRequest::getParameter('status') == 5) {
            $this->_sendMoneyToUser();
        }
    }

    private function _sendMoneyToUser()
    {
        $roomId     = HRequest::getParameter('roomid');
        if( $roomId ) {
            return ;
        }
        $goodsInfo  = $this->_goods->getRecordById($roomId);
        $this->_user->incFieldByWhere('money', '`id` = ' . HRequest::getParameter('parent_id'), $goodsInfo['ext_1']);
    }

    public function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        $list  = HResponse::getAttribute('list');

        $goods = HClass::quickLoadModel('goods');
        $this->_assignUserMap();
        $list  = $goods->getAllRows(HSqlHelper::whereInByListMap('id', 'roomid', $list));
        HResponse::registerFormatMap('status', 'name', RecomPopo::$statusMap);
        HResponse::registerFormatMap('roomid', 'name', HArray::turnItemValueAsKey($list, 'id'));
    }

    private function _assignUserMap() 
    {
        $list     = HResponse::getAttribute('list');

        $user     = HClass::quickLoadModel('userinfo');
        $userList = $user->getAllRowsByFields('`parent_id`,`true_name`', HSqlHelper::whereInByListMap('parent_id', 'parent_id', $list) );
        HResponse::registerFormatMap('parent_id', 'true_name', HArray::turnItemValueAsKey($userList, 'parent_id'));
        $userjj   = $this->_user->getListByFields('id,name',' phone != \'null \'',0,100);

        HResponse::setAttribute('jjRen',$userjj);
        HResponse::registerFormatMap('parent_id', 'true_name', HArray::turnItemValueAsKey($userList, 'parent_id'));
    }

    protected function _otherJobsAfterInfo() 
    {
        parent::_otherJobsAfterInfo();
        $this->_assignUserInfoList();
        $this->_assignLouPanInfoList();
        $this->_assignHuXingInfoList();
        HResponse::setAttribute('status_list', RecomPopo::$statusMap);
    }

    private function _assignHuXingInfoList()
    {
        $record = HResponse::getAttribute('record');
        $info   = $this->_goods->getRecordByFields('`extend`', '`id` = ' . $record['roomid']);
        $data   = json_decode(HString::decodeHtml($info['extend']), true);
        foreach ($data as &$item) {
            $item['id'] = $item['name'];
        }
        HResponse::setAttribute('loupan_list', $data);
    }

    private function _assignUserInfoList()
    {
        $userList = $this->_user->getAllRowsByFields('id,name');
        HResponse::setAttribute('parent_id_list', HArray::turnItemValueAsKey($userList,'id') );
    }


    private function _assignLouPanInfoList()
    {
        $loupanList = $this->_goods->getAllRowsByFields('id,name');
        HResponse::setAttribute('roomid_list', HArray::turnItemValueAsKey($loupanList, 'id'));
    }

    public function ahuxing()
    {
        $roomId = HRequest::getParameter('roomid');
        if( !$roomId ) {
            throw new HVerifyException('id不存在');
        }
        $info   = $this->_goods->getRecordByFields('`extend`', '`id` = ' . $roomId);
        $data   = json_decode(HString::decodeHtml($info['extend']), true);
        HResponse::json( array('rs' => true, 'data' => $data));
    }
}

?>
