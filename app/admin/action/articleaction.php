<?php

/**
 * @version			$Id$
 * @create 			2013-06-18 10:06:22 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.articlepopo, app.admin.action.AdminAction, model.articlemodel');

/**
 * 文章的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class ArticleAction extends AdminAction
{

    public function __construct() 
    {
        parent::__construct();
        $this->_catIdentifier   = 'cat-article';
        $this->_popo            = new ArticlePopo();
        $this->_popo->setFieldAttribute('tags', 'is_show', false);
        $this->_popo->setFieldAttribute('description', 'is_show', false);
        $this->_popo->setFieldAttribute('author', 'is_show', false);
        $this->_model           = new ArticleModel($this->_popo);
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::setAttribute('parent_id_list', '');
        HResponse::registerFormatMap('status', 'name', ArticlePopo::$statusMap);
    }

    protected function _assignAllParentList()
    {
        $list   = $this->_category->getSubCategoryByIdentifier($this->_catIdentifier, true);
        HResponse::setAttribute('parent_id_list', $list);
    }
    
    protected function _otherJobsBeforeEdit(){
        HRequest::setParameter('shop_id', 1);
        HRequest::setParameter('create_time', date('Y-m-d H:i:s'));
    }

}

?>
