<?php

/**
 * @version			$Id$
 * @create 			2018-07-20 11:07:03 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.cityareapopo, app.admin.action.AdminAction, model.cityareamodel');

/**
 * 楼盘城市的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class CityareaAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new CityareaPopo();
        $this->_model       = new CityareaModel($this->_popo);
    }

    public function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('is_open', 'name', CityareaPopo::$isOpenMap);
    }

}

?>
