<?php

/**
 * @version			$Id$
 * @create 			2013-06-17 01:06:41 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.categorypopo, app.admin.action.AdminAction, model.categorymodel'); 

/**
 * 信息分类的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class GonggaoAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_gonggaoId;

    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new CategoryPopo();
        $this->_model       = new CategoryModel($this->_popo);
        $this->_popo->modelEnName = 'gonggao';
        $record             = $this->_model->getRecordByIdentifier('mobile-index-adv');
        $this->_gonggaoId     = $record['id'];
        $this->_model->setMustWhere('gonggao', '`parent_id` = ' . $this->_gonggaoId);
        $this->_listTpl = 'gonggao/list';
        $this->_popo->setFieldAttribute('parent_id', 'is_show', false);
    }


    public function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo(); // TODO: Change the autogenerated stub
    }


   protected function _otherJobsBeforeEdit()
   {
       parent::_otherJobsBeforeEdit(); // TODO: Change the autogenerated stub
       HRequest::setParameter('parent_id', $this->_gonggaoId);
   }

   public function addview()
   {
      $this->_addview();
      $this->_otherJobsAfterInfo();
      $this->_render('gonggao/info');
   }

    public function editview()
    {
        $this->_editview();
        $this->_otherJobsAfterInfo();
        $this->_render('gonggao/info');
    }

    public function add()
    {
        HRequest::setParameter('parent_id', $this->_gonggaoId);
        if (HRequest::getParameter('id')) {
            $this->_edit();
        } else {
            $this->_add();
        }
        HResponse::json(array('rs' => true, 'message' => '设置成功'));
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::setAttribute('parent_id_list', '');
    }

}

?>
