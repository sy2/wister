<?php

/**
 * @version         $Id$
 * @create          2012-4-8 8:48:15 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight       Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 * HongJuZi Framework
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.mobile.action.mobileaction, config.popo.goodspopo, model.goodsmodel');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author          xjiujiu <xjiujiu@foxmail.com>
 * @package         app.admin.action
 * @since           1.0.0
 */
class IndexAction extends MobileAction
{

    /**
     * 构造函数
     *
     * 初始化类里的变量
     *
     * @access public
     */
    private $_goodsCategory;
    private $_information;
    private $_article;
    public function __construct()
    {
        parent::__construct();
        $this->_popo  = new GoodsPopo();
        $this->_model = new GoodsModel($this->_popo);
        $this->_goodsCategory = HClass::quickLoadModel('goodscategory');
        $this->_information   = HClass::quickLoadModel('information');
        $this->_article       = HClass::quickLoadModel('article');
        $this->_model->setMustWhere('status', '`status` = 2');
    }

    /**
     * 首页
     */
    public function index() {
        $this->_assignCateList();
        $this->_assignArticleList();
        $this->_render('index');
    }

    private function _assignCateList()
    {
        $list = $this->_goodsCategory->getAllRowsByFields(
            '`id`,`name`,`image_path`',
            '`status` = 2 AND `is_recommend` = 2');
        HResponse::setAttribute('cglist', $list);
    }

    private function _assignArticleList()
    {
        $list  = $this->_article->getAllRowsByFields('`id`,`name`,`image_path`');
        HResponse::setAttribute('artlist', $list);
    }


    /**
     * 获取商品详情
     */
    public function detail()
    {
        $id       = HRequest::getParameter('id');
        $goods    = HClass::quickLoadModel('goods');
        $record    = $goods->getRecordByFields(
            '`id`,`image_path`,`name`,`parent_id`,`extend`',
            '`id` = ' . $id);
        $list     = json_decode(HString::decodeHtml($record['extend']), true);
        $cate     = HClass::quickLoadModel('goodscategory');
        $cateInfo = $cate->getRecordById($record['parent_id']);
        HResponse::setAttribute('list', $list);
        HResponse::setAttribute('cate_name', $cateInfo['name']);
        $this->_render('detail');
    }

    public function glist()
    {
        $this->_assignGoodsList();
        $this->_assingCateInfo();
        $this->_render('list');
    }

    private function _assignGoodsList()
    {
        $goods  = HClass::quickLoadModel('goods');
        $pid    = HRequest::getParameter('pid');
        $name   = HRequest::getParameter('name');
        if ($pid) {
            $where = '`parent_id` = ' . $pid;
        }
        if ($name) {
            $where = '`name` LIKE \'%' . $name . '%\'';
        }

        $list   = $goods->getAllRowsByFieldsAndOrder(
            '`id`,`name`,`code`,`mk_price`,`parent_id`,`image_path`',
            $where,
            '`sort_num` ASC'
        );
        HResponse::setAttribute('list', $list);
    }

    private function _assingCateInfo()
    {
        $cat    = HClass::quickLoadModel('goodscategory');
        $list   = HResponse::getAttribute('list');
        $pId    = $list[0]['parent_id'] ?: 0;
        $record = $cat->getRecordByFields('`id`,`name`,`image_path`', '`id` = ' . $pId);
        HResponse::setAttribute('cate_record', $record);
    }

    public function reg()
    {
        $this->_render('reg');
    }

}

?>
