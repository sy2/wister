<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.base.action.frontaction');

/**
 * 用户认证基础类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.oauth.action
 * @since 1.0.0
 */
class Mobileaction extends FrontAction
{
    public function __construct()
    {
        $this->_assignSiteCfgInfo();
        $this->_checkFromXcxNickName();
    }

    private function _assignSiteCfgInfo()
    {
        $information = HClass::quickLoadModel('information');
        $record      = $information->getRecordByWhere('1 = 1');
        $user        = HClass::quickLoadModel('user');
        $userInfo    = $user->getRecordByFields('`phone`', '`id` = 1');
        $record      = array_merge($record, $userInfo);
        HResponse::setAttribute('siteCfg', $record);
    }

    private function _checkFromXcxNickName()
    {
        $nickname = HRequest::getParameter('nickname');
        if ($nickname && !HSession::getAttribute('id', 'user')) {
            HResponse::redirect(HResponse::url('auser/aregisterforxcx', 'name=' . $nickname, 'oauth'));
        }
    }
}

?>
