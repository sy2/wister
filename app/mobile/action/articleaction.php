<?php

/**
 * @version         $Id$
 * @create          2012-4-8 8:48:15 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight       Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 * HongJuZi Framework
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.mobile.action.mobileaction, config.popo.articlepopo, model.articlemodel');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author          xjiujiu <xjiujiu@foxmail.com>
 * @package         app.admin.action
 * @since           1.0.0
 */
class ArticleAction extends MobileAction
{

    /**
     * 构造函数 
     * 
     * 初始化类里的变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo    = new ArticlePopo();
        $this->_model   = new ArticleModel($this->_popo);
        $this->_model->setMustWhere('status', '`status` = 2');
    }

    public function index()
    {
        $id     = intval(HRequest::getParameter('id'));
        $record = $this->_model->getRecordByFields(
            '`content`,`name`',
            '`id` = ' . $id);
        $record['content'] = HString::decodeHtml($record['content']);
        HResponse::setAttribute('record', $record);
        $this->_render('about');
    }
}

?>
