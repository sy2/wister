<?php 

/**
 * @version			$Id$
 * @create 			2012-4-25 21:50:27 By xjiujiu
 * @description     HongJuZi Framework
 * @copyright 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.base.action.baseaction');

/**
 * 前台应用的Action基类部分
 * 
 * 提取前台应用对应action的公用方法 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.front.action
 * @since 			1.0.0
 */
class FrontAction extends BaseAction 
{

    //指定页面标题
    protected $_title;

    /**
     * 初始化数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->_title   = null;
        $this->_assignWebSite();
        $this->_switchLangById(HSession::getAttribute('lang_id', 'siteCfg'));
    }

    /**
     * 模块主页显示方法 
     * 
     * @access protected
     * @param int $perpage 每页加载的记录条数，默认为10
     */
    protected function _list($where = '', $perpage = 10)
    {
        $this->_assignModelList($where, $perpage);
        $this->_commAssign();
        $this->_otherJobs();

        HResponse::setAttribute('title', !$this->_title ? $this->_popo->modelZhName : $this->_title);
        HResponse::setAttribute('typeName', !$this->_title ? $this->_popo->modelZhName : $this->_title);
    }

    /**
     * 格式化QQ表情图
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public static function formatQQFaceContent($content)
    {
        if(!preg_match_all('/\[表情_(\d+?)\]/is', $content, $face)) {
            return $content;
        }
        $replaceMap     = array();
        foreach($face[0] as $key => $item) {
            $id         = $face[1][$key];
            $url        = '<img src="'
            . HResponse::uri('cdn') . 'jquery/plugins/qqFace/arclist/' . $id . '.gif" />';
            $replaceMap[$item] = $url;
        }

        return strtr($content, $replaceMap);
    }

    /**
     * 加载其它任务
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _otherJobs() { }
    
    /**
     * 得到所有的父类型 
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _assignParentList()
    {
        $parent    = HClass::quickLoadModel($this->_popo->get('parent'));
        HResponse::setAttribute('parentList', $parent->getAllRows());
    }

    /**
     * 加载默认网站信息
     *
     * 如果这个网站还没有指定默认的网站信息，刚加载默认。
     * 如果这个用户是以Lang的参加加载，刚加载对应的语言
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    protected function _assignWebsite()
    {
        if(!HSession::getAttribute('id', 'siteCfg') && !HRequest::getParameter('lang')) {
            $this->_assignDefWebsite();
            return;
        }
        $langId     = HRequest::getParameter('lang') 
            ? HRequest::getParameter('lang') : HSession::getAttribute('lang_id', 'siteCfg');
        $record     = $this->_getWebSite(
            '`lang_id` = ' . $langId . ' AND `is_open` = 2'
        );
        if(!$record) {
            $this->_assignDefWebsite();
            HResponse::info('此语言版本的网站还没有开放，正在为您导航到正常语言版本！', HResponse::url());
        }
        HSession::setAttributeByDomain($record, 'siteCfg');
    }

    /**
     * 加载默认网站
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _assignDefWebsite()
    {
        $record     = $this->_getWebSite('1 = 1');
        if(!$record) {
            die('网站没有开启，请同管理员联系！');
            exit;
        }
        HSession::setAttributeByDomain($record, 'siteCfg');
    }

    /**
     * 添加交易明细
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param  $orderInfo 交易信息
     */
    protected function _addOrderLog($orderInfo, $rate)
    {
        $orderLog   = HClass::quickLoadModel('orderlog');
        $record     = $orderLog->getRecordByWhere('`order_id` = ' . $orderInfo['id']);
        if($record) {
            return $record['amount'];
        }
        $rate       = floatval($rate) / 100;
        $fee        = $rate * $orderInfo['amount'];
        $fee        = 0.01 > $fee ? 0.01 : $fee;
        $amount     = $orderInfo['amount'] - $fee;
        $data       = array(
            'amount' => $amount,
            'src_amount' => $orderInfo['amount'],
            'order_id' => $orderInfo['id'],
            'rate' => $rate,  //设置的统一费率
            'parent_id' => $orderInfo['shop_id'], //没有shop_id
            'buyer_id' => $orderInfo['parent_id'],
            'author' => HSession::getAttribute('id', 'user')
        );
        if(1 > $orderLog->add($data)) {
            throw new HVerifyException('添加收支明细失败，请确认！');
        }
        $data['amount']     = -1 * $orderInfo['amount'];
        $data['parent_id']  = $orderInfo['parent_id'];
        $orderLog->add($data);
        //添加到系统收入
        if(2 != $orderInfo['type']) {
            $data['amount']     = $fee;
            $data['parent_id']  = 0;
            $orderLog->add($data);
        }

        return $amount;
    }

}

?>
