<?php 

/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

/**
 * 应用的最上层基础类
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.base.action
 * @since 1.0.0
 */
class BaseAction extends HAction
{

    /**
     * {@inheritDoc}
     */
    public function __construct()
    {
        $userId = HRequest::getParameter('userid');
        if ($userId) {
            HSession::setAttribute('userid', $userId, 'user');
        }
        if(!file_exists(ROOT_DIR . '/config/install.lock')) {
            HResponse::info('Wooc 程序还没有安装，请先安装！', HResponse::url('install'));
        }
    }


    /**
     * 设置页面的SEO信息 
     * 
     * @access protected
     * @param string $seoKeyWords 网站的SEO Keywords部分
     * @param string $seoDesc 网站SEO DESC 部分
     */
    protected function _assignSeoInfo()
    {
        $siteCfg           = HSession::getAttributeByDomain('siteCfg');
        HResponse::setAttribute('title', $siteCfg['name']);
        HSession::setAttributeByDomain($siteCfg, 'siteCfg');
    }

    /**
     * 加载语言类型列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _assignLangList()
    {
        $langList   = $this->_getLangList();
        HResponse::setAttribute('lang_id_list', $langList);
        HResponse::setAttribute('lang_id_map', HArray::turnItemValueAsKey($langList, 'id'));
    }

    /**
     * 得到语言列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _getLangList($where = '1 = 1')
    {
        $lang   = HClass::quickLoadModel('lang');

        return $lang->getAllRows($where);
    }

    /**
     * 转换语言
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _switchLang()
    {
        if(!HSession::getAttribute('id', 'lang')) {
            $lang   = HClass::quickLoadModel('lang');
            $record = $lang->getRecordByWhere('`is_default` = 2');
            HSession::setAttributeByDomain($record, 'lang');
            return;
        }
        $id   = HRequest::getParameter('lang');
        if(!$id) {
            return;
        }
        if($id == HSession::getAttribute('id', 'lang')) {
            return;
        }
        $this->_switchLangById($id);
    }

    /**
     * 通过id切换指定语言
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param $id 语言编号
     */
    protected function _switchLangById($id)
    {
        $lang   = HClass::quickLoadModel('lang');
        $record = $lang->getRecordById($id);
        if(!$record) {
            throw new HVerifyException('语言不支持，请确认！');
        }
        HSession::setAttributeByDomain($record, 'lang');
        //加载语言字典
        HTranslate::loadDictByApp(HResponse::getAttribute('HONGJUZI_APP'), $record['identifier']);
    }

    /**
     * 得到网站信息
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _getWebSite($where = '1 = 1')
    {
        $information    = HClass::quickLoadModel('information');

        return $information->getRecordByWhere($where);
    }

    /**
     * 加载信息作者信息 
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _assignAuthorInfo()
    {
        $record = HResponse::getAttribute('record');
        $user   = HClass::quickLoadModel('user');
        HResponse::setAttribute('author', $user->getRecordById($record['author']));
    }

    /**
     * 加载相册
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    protected function _assignAlbum($record = null)
    {
        $record     = !$record ? HResponse::getAttribute('record') : $record;
        $linkedData = HClass::quickLoadModel('linkeddata');
        $linkList   = $linkedData->setRelItemModel($this->_popo->modelEnName, 'resource')
            ->getAllRowsByOrder('`rel_id` = \'' . $record['hash'] . '\'', '`extend` ASC');
        if(empty($linkList)) { return ; } 
        $resource   = HClass::quickLoadModel('resource');
        $resourceList   = $resource->getAllRows(HSqlHelper::whereInByListMap('id', 'item_id', $linkList));
        HResponse::setAttribute('album', $linkList);
        HResponse::setAttribute(
            'resourceMap', 
            HArray::turnItemValueAsKey($resourceList, 'id')
        );
    }

    /**
     * 检测机器机验证码
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @throws HVerifyException 校验异常
     */
    protected function _checkVCode($need = false)
    {
        if(false === $need && !HSession::getAttribute('ask_vcode')) {
            return;
        }
        $this->_checkVCodeMust();
    }

    /**
     * 检测机器机验证码
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @throws HVerifyException 校验异常
     */
    protected function _checkVCodeMust()
    {
        $vcode  = HVerify::isEmptyByVal(HRequest::getParameter('vcode'), '图片验证码');
        if(strtolower($vcode) != HSession::getAttribute('vcode')) {
            throw new HVerifyException('图片验证码不正确，请确认！');
        }
    }

    /**
     * 加工密码
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param $password 密码
     * @param  $salt 因子
     * @return String
     */
    protected function _encodePassword($password, $salt)
    {
        $password   = md5($password);

        return md5(md5($password) . $salt);
    }

    /**
     * 验证手机码是否可用
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param $code 校验码
     * @param  $phone 手机
     * @throws HVerifyException 验证异常
     */
    protected function _checkCodeIsOk($code, $phone)
    {
        $linkedData  = HClass::quickLoadModel('linkeddata');
        $linkedData->setRelItemModel('user', 'code');
        $where          = '`item_id` = \'' . $code . '\' AND `extend` = 1'
            . ' AND `rel_id` = ' . $phone;
        $record         = $linkedData->getRecordByWhere($where);
        if(!$record) {
            throw new HVerifyException('手机校验码不正确，请确认！');
        }
    }
    
    /**
     * 公用的赋值方法 
     * 
     * @access protected
     */
    protected function _commAssign()
    {
        HResponse::setAttribute('title', $this->_title);
        $this->_assignSeoInfo();
        HResponse::setAttribute('modelEnName', $this->_popo->modelEnName);
        HResponse::setAttribute('modelZhName', $this->_popo->modelZhName);
    }
    
    /**
     * 得到用户头像
     * @param  [type]  $userInfo [description]
     * @param  integer $size     [description]
     * @return [type]            [description]
     */
    public static function getAvatar($uid = null, $size = 1)
    {
        if(empty($uid) || $uid <= 0){
            return HResponse::uri() . 'images/default-avatar.png';
        }
        $avatarPath     = HObject::GC('RES_DIR') . '/user/' . HFile::getAvatar($uid, $size);
        if(file_exists(ROOT_DIR . $avatarPath)) {
            return HResponse::url() . $avatarPath;
        }
        HLog::write($avatarPath);
        //用户自己的头像如第三方图片
        if($uid == HSession::getAttribute('id', 'user') && HSession::getAttribute('image_path', 'user')) {
            return HResponse::touri(HSession::getAttribute('image_path', 'user'));
        }

        return HResponse::uri() . 'img/avatar.png';
    }

    /**
     * 加截省列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _getCityList($where = '1 = 1')
    {
        $city   = HClass::quickLoadModel('city');
        $where  .= ' AND `name` NOT IN (\'县\', \'区、乡（镇）\', \'市\', \'市辖区\', \'省直辖行政单位\')';

        return $city->getAllRows($where);
    }

}


?>
