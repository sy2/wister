<?php

/**
 * @version $Id$
 * @create 2013/10/13 12:51:32 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.public.action.publicaction');
HClass::import('app.oauth.action.auserAction');

/**
 * 公用控制父类
 *
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.public.action
 * @since 1.0.0
 */
class PublicadminAction extends PublicAction
{

    /**
     * {@inheritDoc}
     */
    public function beforeAction()
    {
        AuserAction::isLogined();
        HSession::setAttribute('time', (time() + 7200), 'user');
    }

}
